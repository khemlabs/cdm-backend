include source/.env
export

all: build-prod init-db clean-prod

pre-build:
	echo "Copying env files and database to context"
	cp database/cdm_database.sql source/mysql/init_db.sql
	echo "Creating docker network: cdm_network ..."
	docker network create cdm_network
	echo "Please connect your nginx proxy to cdm_network"

build-prod:
	cp source/.env ./
	docker-compose -f source/docker-compose.yml up -d --build

build-prod-bind:
	cp source/.env ./
	docker-compose -f source/docker-compose.yml -f source/bind.yml up -d --build

build-dev:
	cp source/.env ./
	docker-compose -f source/docker-compose.yml -f source/dev.yml up -d --build

build-bind:
	cp source/.env ./
	docker-compose -f source/docker-compose.yml -f source/dev.yml -f source/bind.yml up -d --build

init:
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:schema:update --dump-sql"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:schema:update --force"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:fixtures:load --append"


init-db:
	mysql -u $$DBUSER -p$$DBPASS $$DBNAME < database/cdm_database.sql

init-db-dev:
	docker-compose -f source/docker-compose.yml -f source/dev.yml  exec mysql sh -c "mysql -u \$$MYSQL_USER -p\$$MYSQL_PASSWORD \$$MYSQL_DATABASE < init_db.sql"

force-update:
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:schema:update --force"  

up:
	docker-compose -f source/docker-compose.yml up -d

down:
	docker-compose -f source/docker-compose.yml down -v --remove-orphans

logs:
	docker-compose -f source/docker-compose.yml logs -f

status:
	docker-compose -f source/docker-compose.yml status

role:
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console fos:user:promote $(ADMIN) ROLE_ADMIN"

clear-cache:
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console cache:clear --env=prod --no-debug"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:cache:clear-metadata"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:cache:clear-result"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console doctrine:cache:clear-query"
	docker-compose -f source/docker-compose.yml exec app sh -c "php app/console cache:warmup --env=prod --no-debug"
	# docker-compose -f source/docker-compose.yml exec app sh -c "chown -R :www-data ./"
	# docker-compose -f source/docker-compose.yml exec app sh -c "chmod -R g+rw ./"

force-clear-cache:
	docker-compose -f source/docker-compose.yml exec app sh -c "rm -rf app/cache/*"

clean-prod:
	rm source/mysql/init_db.sql
	rm .env

clean-dev:
	rm source/mysql/init_db.sql
	docker-compose -f source/docker-compose.yml exec app sh -c "rm -rf web/bundles/ app/bootstrap.php.cache app/cache/ app/config/parameters.yml app/logs/ app/phpunit.xml build/ vendor/ bin/"
	rm .env

clean-net:
	docker network remove cdm_network

clean-all: clean-prod down clean-net	
