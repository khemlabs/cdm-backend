UPDATE
======

1.1
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Actualizar librer�as con composer
	$ php composer.phar update

+ Copiar el dump de la base de datos
	$ cd /var/www/html/edu-28-crucedemundos/database/
	$ mysql -u root -p cdm < cdm_database.sql

+ Actualizar las tablas de la base de datos para la librer�a SithousAntiSpam
	$ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --dump-sql
	$ php app/console doctrine:schema:update --force

+ Agregar las reglas del filtro anti spam para la creaci�n de usuarios
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: user_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 86400
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de di�logos
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: dialog_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 1
	Max Calls that can happin in MaxTime: 3

+ Agregar las reglas del filtro anti spam para la persistencia de niveles
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: level_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 5
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de selfies
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: selfie_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 2
	Max Calls that can happin in MaxTime: 1

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

1.2
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Actualizar librer�as con composer
	$ php composer.phar update

+ Copiar el dump de la base de datos
	$ cd /var/www/html/edu-28-crucedemundos/database/
	$ mysql -u root -p cdm < cdm_database.sql

+ Actualizar las tablas de la base de datos para la librer�a SithousAntiSpam
	$ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --dump-sql
	$ php app/console doctrine:schema:update --force

+ Agregar las reglas del filtro anti spam para la creaci�n de usuarios
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: user_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 86400
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de di�logos
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: dialog_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 1
	Max Calls that can happin in MaxTime: 3

+ Agregar las reglas del filtro anti spam para la persistencia de niveles
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: level_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 5
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de selfies
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: selfie_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 2
	Max Calls that can happin in MaxTime: 1

+ Modificar security_listeners.xml para habilitar Captcha en Login:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/config/security_listeners.xml

all� cambiar esta l�nea:
<parameter key="security.authentication.listener.form.class">Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener</parameter>

por esta:
<parameter key="security.authentication.listener.form.class">AppBundle\Listener\UserFormListener</parameter>

+ Evitar que se muestre error 403 en navegador:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

all� modificar las l�neas 66 a 75:

        return new Response($this->twig->render(
            (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException),
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        
cambiar por :

	if($code==403){
		$code=404;		
	}

	$response=new Response($this->twig->render(
            (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException),
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        ), 200, array('Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html'));

	$response->setStatusCode(code);

        return 	$response;

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

1.3
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Arreglar error al evitar que se muestre error 403 en navegador:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

modificar la l�nea 
	$response->setStatusCode(code);
por la l�nea
	$response->setStatusCode($code);

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm	

1.4
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Sobreescribir archivo para arreglar error al evitar que se muestre error 403 en navegador:

	$ cp -f /var/www/html/edu-28-crucedemundos/overwrites/ExceptionController.php /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

1.5
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Actualizar las tablas de la base de datos para medir tiempo de di�logos
	$ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --force

+ Editar el filtro antispam "user_protection"
	$ php app/console sithous:antispam:delete
	Enter the SithousAntiSpamType ID to delete: user_protection
	Successfully removed SithousAntiSpamType "user_protection"

	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: user_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 30
	Max Calls that can happin in MaxTime: 1

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

1.6
-----

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

1.7
-----

+ Clonar o Actualizar el repositorio:
	- Clonar
	$ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
	- Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
	$ git pull

+ Seguir el nuevo proceso de instalaci�n con docker en el manual 5.1 Configuraci�n Aplicaci�n con Docker (PHP + NGINX)

1.8
-----

+ Clonar o Actualizar el repositorio: - Clonar
  $ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
	- Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
  \$ git pull

+ Seguir el nuevo proceso de instalación con docker en el manual 5 Configuración Aplicación con Docker (PHP + NGINX)

1.9
-----

+ Clonar o Actualizar el repositorio:

  - Clonar
    $ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
  - Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
    $ git pull

+ Actualizar las tablas de la base de datos para medir tiempo de diálogos
  - Sin Docker
    $ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --force 
  - Con Docker
    make force-update
    make clear-cache

1.10
-----

+ Clonar o Actualizar el repositorio:

  - Clonar
    $ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
  - Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
    $ git pull

+ Nuevas configuraciones adiccionales de seguridad segun Assetment de la ASI
 - Editar el archivo de virtualhost de apache:
   $ vim /etc/http/conf.d/crucedemundos.conf
 - Agregar la siguiente linea en VirtualHost
   Redirect 404 /.htaccess
 - Editar la linea de directory Options por:
   Options -Indexes FollowSymLinks MultiViews
 - Editar el archivo php.ini y agregar la siguiente linea:
   expose_php=Off
   
+ Borrar la cache del repositorio:
 - Sin Docker:
   $ cd /var/www/html/edu-28-crucedemundos/source/cdm
   $ rm -rf app/cache/*
   $ php app/console cache:clear
   $ php app/console cache:warmup
 - Con Docker:
   $ cd /var/www/html/edu-28-crucedemundos/
   $ make clear-cache
