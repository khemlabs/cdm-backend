CHANGELOG para 1.2
===================

 * Correcci�n incidencia JIRA APPCRUCEMU-20 "Posibilidad de feedback"
 * Correcci�n incidencia JIRA APPCRUCEMU-14. Limite caracteres en login
 * Correcci�n incidencia JIRA APPCRUCEMU-16. Agregado de Captcha en Login
 * Correcci�n incidencia JIRA APPCRUCEMU-21. Se elimino carga externa de Font Awesome.
 * Correcci�n incidencia JIRA APPCRUCEMU-22. Se agrego en menu bot�n ADMIN para Role Admin
 * Correcci�n Vulnerabilidad-0001. Se agrego timeout de sesion en 10 minutos.
 * Correcci�n Vulnerabilidad-0006. Se agrega un control de ROLE_ADMIN al controlador para agregar cursos.
 * Correcci�n Vulnerabilidad-0007. Se agrega validaci�n de los datos a la carga de cursos.
 * Correcci�n Vulnerabilidad-0004. No se muestra el c�digo de error 403
