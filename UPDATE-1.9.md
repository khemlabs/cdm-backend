UPDATE para 1.9
===================

+ Clonar o Actualizar el repositorio:

  - Clonar
    $ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
  - Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
    $ git pull

+ Actualizar las tablas de la base de datos para medir tiempo de diálogos
  - Sin Docker
    $ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --force 
  - Con Docker
    make force-update
    make clear-cache
