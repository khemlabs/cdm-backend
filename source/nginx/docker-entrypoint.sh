#!/bin/bash
set -eux

echo "Linking public path..."
if [ -h /var/www/cmd ] 
 then
  echo "Path already linked."
 else
  ln -s /source /var/www/cmd
fi

exec "$@"