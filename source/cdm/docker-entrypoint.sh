#!/bin/bash
set -eux

#composer install
php app/console cache:clear --env=$ENV_MODE
#chown -R www-data:www-data ./
#chmod -R 775 web/

exec "$@"
