<?php
// src/AppBundle/Form/ResettingType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gregwar\CaptchaBundle\Type\CaptchaType;

class ResettingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    //$builder->add('captcha', CaptchaType::class); // That's all !
	    // If you're using php<5.5, you can use instead:
	    $builder->add('captcha', 'Gregwar\CaptchaBundle\Type\CaptchaType');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ResettingFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_resetting';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
