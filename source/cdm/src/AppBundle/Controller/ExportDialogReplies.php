<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExportDialogReplies extends Controller
{
    /**
     * @Route("/alumnos/export/dialog_replies")
     */
    public function indexAction(Request $request)
    {

        $dialogRepository = $this->getDoctrine()
            ->getRepository('AppBundle:DialogReplies');

        $levels = $dialogRepository->findAll();
        $rows = array();

        $data = array(
            "ID",
            "USER_ID",
            "COMPUTER_ID",
            "CHARACTER_NAME",
            "LEVEL_ID",
            "GAME_ID",
            "DIALOG_ID",
            "DIALOG_INDEX",
            "DIALOG_MOOD",
            "ANSWER_ID",
            "DIALOG_TIME",
            "TIMESTAMP");

        $rows[] = implode('|', $data);

        foreach ($levels as $level) {
            $data = array(
                str_replace('\n', ' ', $level->getId()),
                str_replace('\n', ' ', $level->getUserId()),
                str_replace('\n', ' ', $level->getComputerId()),
                str_replace('\n', ' ', $level->getCharacterName()),
                str_replace('\n', ' ', $level->getLevelId()),
                str_replace('\n', ' ', $level->getGameId()),
                str_replace('\n', ' ', $level->getDialogId()),
                str_replace('\n', ' ', $level->getDialogIndex()),
                str_replace('\n', ' ', $level->getDialogMood()),
                str_replace('\n', ' ', $level->getAnswerId()),
                str_replace('\n', ' ', $level->getDialogTime()),
                str_replace('\n', ' ', $level->getTimeString()));

            $rows[] = implode('|', $data);
        }

        $content = implode("\n", $rows);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="dialog_replies.csv"');

        return $response;

    }
}
