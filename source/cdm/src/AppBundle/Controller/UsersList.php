<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\Users;
use Doctrine\ORM\Query\ResultSetMapping;

class UsersList extends Controller
{
	/**
	 * @Route("/alumnos")
	 */
	public function indexAction(Request $request)
	{
		$uid =$request->query->get('user_id');

		$users=array();

		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:Users');
		$users = $usersRepository->findBy(array('user_id'=> $uid));

		return $this->render('cdm/alumnos.html.twig',array('users' => $users, 
			'user_id' => $uid,
		));
	}
}

