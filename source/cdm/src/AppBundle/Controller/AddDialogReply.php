<?php
// src/AppBundle/Controller/AddDialogReply.php
namespace AppBundle\Controller;

use AppBundle\Entity\DialogReplies;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddDialogReply extends Controller
{
    /**
     * @Route("/reply/add")
     */
    public function createAction(Request $request)
    {
        $spamCheck = $this->get('sithous.antispam');
        if (!$spamCheck->setType('dialog_protection')->verify()) {
            return new JsonResponse(array(
                'result' => 'error',
                'message' => $spamCheck->getErrorMessage(),
            ));
        }

        $usersRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Users');

        $uid = $request->query->get('user_id');
        $cid = $request->query->get('computer_id');
        $gid = $request->query->get('game_id');

        $check = $usersRepository->findOneBy(array('user_id' => $uid, 'computer_id' => $cid));
        if ($check != null) {

            $passTest = (string) $uid . (string) $cid . "mondongo";
            $encoder = new MessageDigestPasswordEncoder('md5', false, 0);
            $hash = $encoder->encodePassword($passTest, "");

            if ($hash === $request->query->get('hash')) {

                $replies = new DialogReplies();
                $replies->setUserId($uid);
                $replies->setComputerId($cid);
                $replies->setGameId($gid);
                $replies->setCharacterName($request->query->get('character_name'));
                $replies->setDialogId($request->query->get('dialog_id'));
                $replies->setLevelId($request->query->get('level_id'));
                $replies->setDialogIndex($request->query->get('dialog_index'));
                $replies->setDialogMood($request->query->get('dialog_mood'));
                $replies->setAnswerId($request->query->get('answer_id'));
                $replies->setDialogTime($request->query->get('dialog_time'));
                $replies->setTimestamp(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();

                // tells Doctrine you want to (eventually) save the Product (no queries yet)
                $em->persist($replies);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();

                return new Response('Saved new reply with id ' . $replies->getId());
            } else {
                return new Response('HASH MISMATCH');
            }
        } else {
            return new Response('USER/COMPUTER NOT FOUND');
        }
    }
}
