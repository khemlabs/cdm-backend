<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailTest extends Controller
{
	/**
	 * @Route("/mail_test")
	 */
	public function indexAction()
	{
		$name = "Juan";
		$message = \Swift_Message::newInstance()
			->setSubject('Hello Email')
			->setFrom('juanpamato@yahoo.com.ar')
			->setTo('juanpamato@gmail.com')
			->setBody(
				$this->renderView(
					// app/Resources/views/Emails/registration.html.twig
					'Emails/registration.html.twig',
					array('name' => $name)
				),
				'text/html'
			)
			/*
			 * If you also want to include a plaintext version of the message
			 ->addPart(
				 $this->renderView(
					 'Emails/registration.txt.twig',
					 array('name' => $name)
				 ),
				 'text/plain'
			 )
			 */
			;
		$this->get('mailer')->send($message);

		return new Response('OK');
	}
}
