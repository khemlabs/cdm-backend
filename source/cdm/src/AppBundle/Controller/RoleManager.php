<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\DataUser;

class RoleManager extends Controller
{
	/**
	 * @Route("/admin/roles")
	 */
	public function createAction(Request $request)
	{

		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:DataUser');
		$em = $this->getDoctrine()->getManager();

		$users = $request->query->all();
		$claves = array_keys($users);
		$users = array_values($users);
		
		//print($users[0]);
		for ($x = 0; $x < sizeof($users); $x++) {
		//foreach ($users as $user){
			$userData = $usersRepository->findOneBy(array('username'=> urldecode($claves[$x])));
			//print($users[$x]);
			if($users[$x]=='ROLE_USER'){
				$userData->removeRole('ROLE_ADMIN');
				$userData->removeRole('ROLE_STAFF');
				$userData->addRole($users[$x]);
				$em->persist($userData);
			}else if($users[$x]=='ROLE_ADMIN'){
				$userData->removeRole('ROLE_STAFF');
				$userData->addRole($users[$x]);
				$em->persist($userData);
			}else if($users[$x]=='ROLE_STAFF'){
				$userData->removeRole('ROLE_ADMIN');
				$userData->addRole($users[$x]);
				$em->persist($userData);
			}else if($users[$x]=='DELETE'){
				$em->remove($userData);
			}
			
			

	    		// tells Doctrine you want to (eventually) save the Product (no queries yet)
	    		
			// actually executes the queries (i.e. the INSERT query)
		}
	//	print($request->query->all()["jp"]);
	/*	$userData = $usersRepository->findOneBy(array('username'=> 'jpa'));
		print($userData->getUsername());
		$userData->addRole("ROLE_ADMIN");
		// tells Doctrine you want to (eventually) save the Product (no queries yet)
		$em->persist($userData);*/
	    	$em->flush();

		//return new Response($request->query->get('jp'));
		//
		print("Cambio de roles de usuario guardado");

		$allusers = $usersRepository->findAll();		

		$cursosRepository = $this->getDoctrine()
			->getRepository('AppBundle:Cursos');
		
		$cursos = $cursosRepository->findAll();		
		
		return $this->render('cdm/admin.html.twig',array('users' => $allusers, 'cursos' => $cursos));	
	
	}
}

