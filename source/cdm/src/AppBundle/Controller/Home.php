<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Home extends Controller
{
	/**
	 * @Route("/")
	 */
	public function indexAction(Request $request)
	{
		return $this->render('cdm/index.html.twig');
	}
}

