<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExportDialogs extends Controller
{
	/**
	 * @Route("/alumnos/export/dialogs")
	 */
	public function indexAction(Request $request)
	{
		
		$dialogRepository = $this->getDoctrine()
			->getRepository('AppBundle:Dialog');

		$levels = $dialogRepository->findAll();
		$rows = array();

		$data = array(
			"ID",
			"CHARACTER_NAME",
			"LEVEL_ID",
    			"DIALOG_TYPE",
			"DIALOG_INDEX",
			"DIALOG_MOOD",
			"DIALOG_PROMPT",
    			"ANSWER_ID",
    			"ANSWER_TEXT",
			"TIPO_INDICADOR",
			"SUBTIPO_INDICADOR");

			$rows[] = implode('|', $data);

   		foreach ($levels as $level) {
			$data = array(
				str_replace('\n', ' ', $level->getId()),
				str_replace('\n', ' ', $level->getCharacterName()),
				str_replace('\n', ' ', $level->getLevelId()),
    				str_replace('\n', ' ', $level->getDialogType()),
				str_replace('\n', ' ', $level->getDialogIndex()),
				str_replace('\n', ' ', $level->getDialogMood()),
    				str_replace('\n', ' ', $level->getDialogPrompt()),
    				str_replace('\n', ' ', $level->getAnswerId()),
    				str_replace('\n', ' ', $level->getAnswerText()),
				str_replace('\n', ' ', $level->getTipoIndicador()),
				str_replace('\n', ' ', $level->getSubtipoIndicador()),
	    			str_replace('\n', ' ', $level->getValorIndicador()));

			$rows[] = implode('|', $data);
	    	}

		$content = implode("\n", $rows);
		$response = new Response($content);
    		$response->headers->set('Content-Type', 'text/csv');
		$response->headers->set('Content-Disposition', 'attachment; filename="dialogs.csv"');

    		return $response;
		
	}
}

