<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\level;

class Determinacion extends Controller
{
	/**
	 * @Route("/alumnos/determinacion")
	 */
	public function indexAction(Request $request)
	{
		
		$levelsRepository = $this->getDoctrine()
			->getRepository('AppBundle:Level');

		$levelDataRepository = $this->getDoctrine()
			->getRepository('AppBundle:LevelData');

		$users = $request->query->get('users');

		$u = array();
		$u = explode('_user_id=',$users);
		$computer_id = $u[0];
		$user_id = $u[1];

		$levelN = $request->query->get('level');
		
		$em = $this->getDoctrine()->getManager();
		$qbLevels = $em->createQueryBuilder();
		$array = $qbLevels->select('l.level_id')
			->distinct()
			->from('AppBundle:Level', 'l')
			->where('l.computer_id = :cid')
			->andWhere('l.user_id = :uid')
			->setParameters(array('cid' => $computer_id ,'uid' => $user_id))
			->getQuery()->getArrayResult();

		$levelOp = array();
		for ($x = 0; $x < sizeof($array); $x++) {
			foreach($array[$x] as $b){
				array_push($levelOp,$b);
			}
		}

		$levelCant=0;
		$totalPerc=0;

		for ($x = 0; $x < sizeof($levelOp); $x++) {
			$ld = $levelDataRepository->findOneBy(array('level_id'=> $x));
			if($ld!=null){
				$cant = sizeof(explode(";",$ld->getMissions()));
				$levels = $levelsRepository->findBy(array('level_id'=> $x, 'computer_id'=> $computer_id, 'user_id'=>$user_id));
				$levelPerc=0;
				foreach ($levels as $level){
					$cantTrue=0;
					$missions = explode(";",$level->getMissions());
					foreach($missions as $m){
						if($m=="True")
							$cantTrue++;
					}
					if($cantTrue>0) {
						$l=100*$cantTrue/$cant;
						if($l>$levelPerc)
							$levelPerc = $l;
					}
				}
				if($levelPerc>0){
					$totalPerc+=$levelPerc;
					$levelCant++;
				}
			}
		}
		
		if($levelCant!=0){
			$totalPerc = round($totalPerc/$levelCant,2);
		}


		//$levelsImp = $levelsRepository->findBy(array('level_id'=> 5, 'computer_id'=> $users, 'give_up'=> 0 ));
		//
		$intentos = sizeof($levelsRepository->findBy(array('level_id'=> 5, 'computer_id'=> $computer_id, 'user_id' => $user_id, 'give_up'=> 0 )));

		return $this->render('cdm/determinacion.html.twig',array('user' => $users, 'userName' => $user_id, 'percent' => $totalPerc, 'intentos' => $intentos ));
	}
}
