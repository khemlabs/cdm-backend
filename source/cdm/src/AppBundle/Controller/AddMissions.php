<?php
// src/AppBundle/Controller/AddMissions.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Missions;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddMissions extends Controller
{
	/**
	 * @Route("/missions/add")
	 */
	public function createAction(Request $request)
	{

		$passTest = "mondongo";
		$encoder = new MessageDigestPasswordEncoder('md5', false, 0);
		$hash = $encoder->encodePassword($passTest,"");

		if($hash === $request->query->get('hash')){

			$missions = new Missions();
			$missions ->setLevelId($request->query->get('level_id'));
			$missions ->setMissions($request->query->get('missions'));
			$missions ->setCreateTime(new \DateTime("now"));

			$em = $this->getDoctrine()->getManager();

			// tells Doctrine you want to (eventually) save the Product (no queries yet)
			$em->persist($missions);

			// actually executes the queries (i.e. the INSERT query)
			$em->flush();

			return new Response('Saved new mission with id '.$missions->getId());
		}else{
			return new Response('FORBIDDEN ACCESS');
		}
	}
}
