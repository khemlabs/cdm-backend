<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\DataUser;
use AppBundle\Entity\Cursos;

class GetCursos extends Controller
{
	/**
	 * @Route("/cursos")
	 */
	public function createAction(Request $request)
	{		
		$em = $this->getDoctrine()->getManager();
   		$query = $em->createQuery(
        		'SELECT c
		        FROM AppBundle:Cursos c'
		);
		$categorias = $query->getArrayResult();

   		$response = new Response(json_encode($categorias));
		$response->headers->set('Content-Type', 'application/json');

    		return $response;
	
	}
}

