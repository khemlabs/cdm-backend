<?php
// src/AppBundle/Controller/AddLevelData.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\LevelData;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddLevelData extends Controller
{
	/**
	 * arrobaRoute("/levelData/add")
	 */
	public function createAction(Request $request)
	{

		$passTest = "mondongo";
		$encoder = new MessageDigestPasswordEncoder('md5', false, 0);
		$hash = $encoder->encodePassword($passTest,"");

		if($hash === $request->query->get('hash')){
			$levelData = new LevelData();
			$levelData ->setLevelId($request->query->get('level_id'));
			$levelData ->setMissions($request->query->get('missions'));
			$levelData ->setPortal($request->query->get('portal'));
			$levelData ->setFire($request->query->get('fire'));
			$levelData ->setPollution($request->query->get('pollution'));
			$levelData ->setPortalCharge($request->query->get('portalCharge'));
			$levelData ->setFireCharge($request->query->get('fireCharge'));
			$levelData ->setPollutionCharge($request->query->get('pollutionCharge'));
			$levelData ->setResourcesCharge($request->query->get('resourcesCharge'));
			$levelData ->setCreateTime(new \DateTime("now"));

			$em = $this->getDoctrine()->getManager();

			// tells Doctrine you want to (eventually) save the Product (no queries yet)
			$em->persist($levelData);

			// actually executes the queries (i.e. the INSERT query)
			$em->flush();

			return new Response('Saved new mission with id '.$levelData->getId());
		}else{
			return new Response('FORBIDDEN ACCESS');
		}
	}
}

