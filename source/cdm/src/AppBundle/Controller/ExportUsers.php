<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExportUsers extends Controller
{
	/**
	 * @Route("/alumnos/export/users")
	 */
	public function indexAction(Request $request)
	{
		
		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:Users');

		$levels = $usersRepository->findAll();
		$rows = array();

		$data = array(
			"ID",
			"USER_ID",
    			"COMPUTER_ID",
			"USER_NAME",
			"CUE",
			"GRADO",
			"DIVISION",
			"TURNO",
    			"TIMESTAMP");

			$rows[] = implode('|', $data);

   		foreach ($levels as $level) {
			$data = array(
				str_replace('\n', ' ', $level->getId()),
				str_replace('\n', ' ', $level->getUserId()),
    				str_replace('\n', ' ', $level->getComputerId()),
				str_replace('\n', ' ', $level->getUserName()),
				str_replace('\n', ' ', $level->getCue()),
				str_replace('\n', ' ', $level->getGrado()),
				str_replace('\n', ' ', $level->getDivision()),
				str_replace('\n', ' ', $level->getTurno()),
	    			str_replace('\n', ' ', $level->getTimeString()));

			$rows[] = implode('|', $data);
	    	}

		$content = implode("\n", $rows);
		$response = new Response($content);
    		$response->headers->set('Content-Type', 'text/csv');
		$response->headers->set('Content-Disposition', 'attachment; filename="users.csv"');

    		return $response;
		
	}
}

