<?php
// src/AppBundle/Controller/AddDialog.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Dialog;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddDialog extends Controller
{
    /**
     * arrobaRoute("/dialog/add")
     */
    public function createAction(Request $request)
{

	$passTest = "mondongo";
	$encoder = new MessageDigestPasswordEncoder('md5', false, 0);
	$hash = $encoder->encodePassword($passTest,"");

	if($hash === $request->query->get('hash')){

    		$dialog = new Dialog();
		$dialog->setCharacterName($request->query->get('character_name'));
    		$dialog->setLevelId($request->query->get('level_id'));
    		$dialog->setDialogType($request->query->get('dialog_type'));
    		$dialog->setDialogIndex($request->query->get('dialog_index'));
    		$dialog->setDialogMood($request->query->get('dialog_mood'));
    		$dialog->setDialogPrompt($request->query->get('dialog_prompt'));
    		$dialog->setAnswerId($request->query->get('answer_id'));
    		$dialog->setAnswerText($request->query->get('answer_text'));
    		$dialog->setTipoIndicador($request->query->get('tipo_indicador'));
    		$dialog->setSubtipoIndicador($request->query->get('subtipo_indicador'));
    		$dialog->setValorIndicador($request->query->get('valor_indicador'));
    		$dialog->setCreateTime(new \DateTime("now"));

		$em = $this->getDoctrine()->getManager();

	    // tells Doctrine you want to (eventually) save the Product (no queries yet)
	    $em->persist($dialog);

	    // actually executes the queries (i.e. the INSERT query)
	    $em->flush();

	    return new Response('Dialog add with id '.$dialog->getId());
	}else{
		return new Response('FORBIDDEN ACCESS');
	}
}
}
