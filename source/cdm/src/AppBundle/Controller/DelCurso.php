<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\DataUser;
use AppBundle\Entity\Cursos;

class DelCurso extends Controller
{
	/**
	 * @Route("/admin/cursos/delete")
	 */
	public function createAction(Request $request)
	{

		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:DataUser');

		$cursosRepository = $this->getDoctrine()
			->getRepository('AppBundle:Cursos');

		$id = $request->query->get('id');


		$curso = $cursosRepository->findOneBy(array('id'=>$id));


		$em = $this->getDoctrine()->getManager();

		$em->remove($curso);
		$em->flush();              
	
		//
		print("Curso eliminado");

		$allusers = $usersRepository->findAll();

		$cursos = $cursosRepository->findAll();

		return $this->render('cdm/admin.html.twig',array('users' => $allusers, 'cursos' => $cursos));
	
	}
}

