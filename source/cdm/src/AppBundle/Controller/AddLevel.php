<?php
// src/AppBundle/Controller/AddLevel.php
namespace AppBundle\Controller;

use AppBundle\Entity\Level;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddLevel extends Controller
{
    /**
     * @Route("/level/add")
     */
    public function createAction(Request $request)
    {
        $spamCheck = $this->get('sithous.antispam');
        if (!$spamCheck->setType('level_protection')->verify()) {
            return new JsonResponse(array(
                'result' => 'error',
                'message' => $spamCheck->getErrorMessage(),
            ));
        }

        $usersRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Users');

        $uid = $request->query->get('user_id');
        $cid = $request->query->get('computer_id');

        $check = $usersRepository->findOneBy(array('user_id' => $uid, 'computer_id' => $cid));
        if ($check != null) {

            $passTest = (string) $uid . (string) $cid . "mondongo";
            $encoder = new MessageDigestPasswordEncoder('md5', false, 0);
            $hash = $encoder->encodePassword($passTest, "");

            if ($hash === $request->query->get('hash')) {
                $level = new Level();
                $level->setUserId($request->query->get('user_id'));
                $level->setComputerId($request->query->get('computer_id'));
                $level->setLevelId($request->query->get('level_id'));
                $level->setGameId($request->query->get('game_id'));
                $level->setToolsSelected($request->query->get('tools_selected'));
                $level->setToolsEndCharge($request->query->get('tools_end'));
                $level->setMissions($request->query->get('missions'));
                $level->setPortalDone($request->query->get('portal_done'));
                $level->setFireDone($request->query->get('fire_done'));
                $level->setPollutionDone($request->query->get('pollution_done'));
                $level->setMapChecks($request->query->get('map_checks'));
                $level->setLevelTime($request->query->get('level_time'));
                $level->setGameTime($request->query->get('game_time'));
                $level->setPhoneMapTime($request->query->get('phone_map_time'));
                $level->setFirstMapTime($request->query->get('first_map_time'));
                $level->setToolsMapTime($request->query->get('tools_map_time'));
                $level->setMissionTime($request->query->get('mission_time'));
                $level->setToolsTime($request->query->get('tools_time'));
                $level->setMapTrail($request->query->get('map_trail'));
                $level->setMapDeadEnds($request->query->get('map_dead_ends'));
                $level->setRtBegin($request->query->get('rt_begin'));
                $level->setRtAfterTools($request->query->get('rt_after_tools'));
                $level->setRtEnd($request->query->get('rt_end'));
                $level->setLevelEnd($request->query->get('end_level'));
                $level->setPortalCharge($request->query->get('portal_charge'));
                $level->setFireCharge($request->query->get('fire_charge'));
                $level->setPollutionCharge($request->query->get('pollution_charge'));
                $level->setRtCharge($request->query->get('rt_charge'));
                $level->setTimestamp(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();

                // tells Doctrine you want to (eventually) save the Product (no queries yet)
                $em->persist($level);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();

                return new Response('Saved new level with id ' . $level->getId());
            } else {
                return new Response('FORBIDDEN ACCESS');
            }
        } else {
            return new Response('FORBIDDEN ACCESS');
        }
    }
}
