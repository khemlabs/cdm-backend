<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExportSelfies extends Controller
{
    /**
     * @Route("/alumnos/export/selfies")
     */
    public function indexAction(Request $request)
    {

        $selfieRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Selfie');

        $levels = $selfieRepository->findAll();
        $rows = array();

        $data = array(
            "ID",
            "USER_ID",
            "COMPUTER_ID",
            "LEVEL_ID",
            "GAME_ID",
            "EMOJI_NAME",
            "ESTADO",
            "TIMESTAMP");

        $rows[] = implode('|', $data);

        foreach ($levels as $level) {
            $data = array(
                str_replace('\n', ' ', $level->getId()),
                str_replace('\n', ' ', $level->getUserId()),
                str_replace('\n', ' ', $level->getComputerId()),
                str_replace('\n', ' ', $level->getLevelId()),
                str_replace('\n', ' ', $level->getGameId()),
                str_replace('\n', ' ', $level->getEmojiName()),
                str_replace('\n', ' ', $level->getEstado()),
                str_replace('\n', ' ', $level->getTimeString()));

            $rows[] = implode('|', $data);
        }

        $content = implode("\n", $rows);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="selfies.csv"');

        return $response;

    }
}
