<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\level;

class Autocontrol extends Controller
{
	/**
	 * @Route("/alumnos/autocontrol")
	 */
	public function indexAction(Request $request)
	{
		
		$levelsRepository = $this->getDoctrine()
			->getRepository('AppBundle:Level');

		$levelDataRepository = $this->getDoctrine()
			->getRepository('AppBundle:LevelData');

		$selfieRepository = $this->getDoctrine()
		    ->getRepository('AppBundle:Selfie');

		$users = $request->query->get('users');

		$u = array();
		$u = explode('_user_id=',$users);
		$computer_id = $u[0];
		$user_id = $u[1];

		$levelN = $request->query->get('level');
		
		$em = $this->getDoctrine()->getManager();
		$qbLevels = $em->createQueryBuilder();

		$array = $qbLevels->select('l.level_id')
			->distinct()
			->from('AppBundle:Level', 'l')
			->where('l.computer_id = :cid')
			->andWhere('l.user_id = :uid')
			->setParameters(array('cid' => $computer_id ,'uid' => $user_id))
			->getQuery()->getArrayResult();

		$levelOp = array();
		for ($x = 0; $x < sizeof($array); $x++) {
			foreach($array[$x] as $b){
				array_push($levelOp,$b);
			}
		}

		$missions=array();
		$portals=array();
		$fires=array();
		$pollutions=array();


		$levels = $levelsRepository->findBy(array('level_id'=> $levelN, 'computer_id'=> $computer_id, 'user_id'=> $user_id));

		foreach ($levels as $level){
			$ld = $levelDataRepository->findOneBy(array('level_id'=> $level->getLevelId()));
			array_push($missions,$ld->getMissions());
			array_push($portals,$ld->getPortal());
			array_push($fires,$ld->getFire());
			array_push($pollutions,$ld->getPollution());
		}

		$selfies = $selfieRepository->findBy(array('level_id'=> $levelN, 'computer_id'=> $computer_id, 'user_id'=> $user_id));

		$cantRejugado = sizeof($levels);

		return $this->render('cdm/autocontrol.html.twig',array('user' => $users, 'userName' => $user_id, 'levelOp' => $levelOp, 'levelSel' => $levelN, 'levels' => $levels,'missions' => $missions,'portals' => $portals, 'fires' => $fires, 'pollutions' => $pollutions, 'selfies' => $selfies, 'rejugadas' => $cantRejugado));
	}
}
