<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Dialog;

class Empatia extends Controller
{
	/**
	 * @Route("/alumnos/empatia")
	 */
	public function indexAction(Request $request)
	{

		$replyRepository = $this->getDoctrine()
			->getRepository('AppBundle:DialogReplies');

		$dialogsRepository = $this->getDoctrine()
		    ->getRepository('AppBundle:Dialog');

		
		$user = $request->query->get('users');

		$u = array();
		$u = explode('_user_id=',$user);
		$computer_id = $u[0];
		$user_id = $u[1];
		
		$replies = $replyRepository->findBy(array('computer_id'=> $computer_id, 'user_id' => $user_id));

		$dialogs=array();
		$r=array();

		foreach ($replies as $reply){
			$d = $dialogsRepository->findOneBy(array('level_id'=> $reply->getLevelId(),
				'character_name'=> $reply->getCharacterName(),
				'dialog_index'=> $reply->getDialogIndex(),
				'dialog_mood'=> $reply->getDialogMood(),
				'answer_id'=> $reply->getAnswerId(),
				'tipo_indicador'=>'EMPATIA'
			));
			if($d){
				array_push($dialogs,$d);
				array_push($r,$reply);
			}
		}
		
		return $this->render('cdm/empatia.html.twig',array('username' => $user_id, 'replies' => $r,'dialogs' => $dialogs));
	}
}

