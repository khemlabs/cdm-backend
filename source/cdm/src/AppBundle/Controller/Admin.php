<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\DataUser;

class Admin extends Controller
{
	/**
	 * @Route("/admin")
	 */
	public function indexAction(Request $request)
	{

		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:DataUser');
		
		$users = $usersRepository->findAll();


		$cursosRepository = $this->getDoctrine()
			->getRepository('AppBundle:Cursos');
		
		$cursos = $cursosRepository->findAll();		
		
		return $this->render('cdm/admin.html.twig',array('users' => $users, 'cursos' => $cursos));
	}
}

