<?php
// src/AppBundle/Controller/CreateUser.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Users;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\HttpFoundation\JsonResponse;

class CreateUser extends Controller
{
    /**
     * @Route("/users/update")
     */
    public function createAction(Request $request){

	$spamCheck = $this->get('sithous.antispam');
        if(!$spamCheck->setType('user_protection')->verify()){
         	return new JsonResponse(array(
        	'result'  => 'error',
               	'message' => $spamCheck->getErrorMessage()
           	));
				}

        $passTest = "mondongo";
	$encoder = new MessageDigestPasswordEncoder('md5', false, 0);
	$hash = $encoder->encodePassword($passTest,"");

	$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:Users');

	$uid = $request->query->get('userid');

	$result = $usersRepository->findBy(array( 'user_id' => $uid ));

	if(sizeof($result)!=0 && $hash === $request->query->get('hash')){
        
		$users = $result[0];
		$users->setUserId($request->query->get('userid'));
    		$users->setComputerId($request->query->get('computerid'));
    		$users->setUserName($request->query->get('username'));
    		$users->setCue($request->query->get('cue'));
    		$users->setGrado($request->query->get('grado'));
    		$users->setDivision($request->query->get('division'));
    		$users->setTurno($request->query->get('turno'));
		$users->setCreateTime(new \DateTime("now"));

   		$em = $this->getDoctrine()->getManager();

		// tells Doctrine you want to (eventually) save the Product (no queries yet)
				$em->merge($users);

		// actually executes the queries (i.e. the INSERT query)
		$em->flush();

		return new Response('Saved new user with id '.$users->getId());
	}else{
		return new Response('FORBIDDEN ACCESS');
	}
   }

   /**
     * @Route("/users/create/all")
     */
    public function createAll(Request $request){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		$string = '';
		$max = strlen($characters) - 1;
		$randomStringLength = 6;
		$maxUsers = 2000;

		for($j=0;$j<$maxUsers;$j++){
			for($i=0;$i < $randomStringLength;$i++){
				$string .= $characters[random_int(0,$max)];
			}
			$usersRepository = $this->getDoctrine()
									->getRepository('AppBundle:Users');
			$result = $usersRepository->findBy(array('user_id' => $string ));
		
			if(sizeof($result)<1 ){
				$users = new Users();
				$users->setUserId($string);
					$users->setComputerId('');
					$users->setUserName('');
					$users->setCue('');
					$users->setGrado('');
					$users->setDivision('');
					$users->setTurno('');
				$users->setCreateTime(new \DateTime("now"));
	
				$em = $this->getDoctrine()->getManager();
	
					$em->persist($users);
			
				$em->flush();
			}

			$string = '';
		}

		$response = new Response();
        $response->setContent('All users were created!');
        $response->setStatusCode(Response::HTTP_OK);
		return $response;
	}
}
