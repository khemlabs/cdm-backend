<?php
// src/AppBundle/Controller/Session.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class Session extends Controller
{
    /**
     * @Route("/session/start")
     */
    public function startAction(Request $request)
    {

        $passTest = "mondongo";
        $encoder = new MessageDigestPasswordEncoder('md5', false, 0);
        $hash = $encoder->encodePassword($passTest, "");

        $usersRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Users');
        $levelData = $this->getDoctrine()
            ->getRepository('AppBundle:Level');

        $uid = $request->query->get('userid');
        $cid = $request->query->get('computerid');
        $gid = $request->query->get('gameid');

        $check = $usersRepository->findBy(array('computer_id' => $cid, 'user_id' => $uid));
        $session = $levelData->findBy(array('game_id' => $gid));

        //$check = array();

        if (sizeof($check) < 1 && $hash === $request->query->get('hash')) {

            if ($session < 1) {
                return new Response('New Session Started' . $users->getId());
            }
            return new Response('Session Already Started' . $users->getId());

        } else {
            return new Response('USER NOT FOUND');
        }
    }
}
