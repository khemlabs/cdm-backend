<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\DataUser;
use AppBundle\Entity\Cursos;

class AddCurso extends Controller
{
	/**
	 * @Route("/admin/cursos/add")
	 */
	public function createAction(Request $request)
	{
		if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
		$usersRepository = $this->getDoctrine()
			->getRepository('AppBundle:DataUser');

		$cue = $request->query->get('cue');
		$nombre = $request->query->get('nombre');
		$grado = $request->query->get('grado');
		$division = $request->query->get('division');
		$turno = $request->query->get('turno');

		if(strlen($cue)<11&&!preg_match('/[^0-9p{]/', $cue) &&
		strlen($nombre)<99 &&
		strlen($grado)<3&&!preg_match('/[^0-9]/', $grado) &&
		strlen($division)<3&&!preg_match('/[^0-9]/', $division) &&
		strlen($turno)<17&&!preg_match('/[^A-Za-z������������]/', utf8_decode($turno))
		){

		$cursos = new Cursos();
		$cursos->setCue($cue);
    		$cursos->setNombre($nombre);
	    	$cursos->setGrado($grado);
    		$cursos->setDivision($division);
    		$cursos->setTurno($turno);    	
	    	$cursos->setCreateTime(new \DateTime("now"));

		$em = $this->getDoctrine()->getManager();

		// tells Doctrine you want to (eventually) save the Product (no queries yet)
		$em->persist($cursos);

		// actually executes the queries (i.e. the INSERT query)
		$em->flush();
	
		//
			print("Curso agregado");

		}else{
			print("Curso no agregado, datos inv&aacute;lidos");
		}
		$allusers = $usersRepository->findAll();


		$cursosRepository = $this->getDoctrine()
			->getRepository('AppBundle:Cursos');
		
		$cursos = $cursosRepository->findAll();		
		
		
		return $this->render('cdm/admin.html.twig',array('users' => $allusers, 'cursos' => $cursos));
		
		}	
	}

	function prepare($pattern){
		$replacements =array('�'=>'&Aacute;', '�'=>'&aacute;', '�'=>'&Eacute;', '�'=>'&eacute;', '�'=>'&Iacute;', '�'=>'&iacute;', '�'=>'&Ntilde;', '�'=>'&ntilde;', '�'=>'&Oacute;', '�'=>'&oacute;', '�'=>'&Uacute;', '�'=>'&uacute;');
		return str_replace(array_keys($replacements), $replacements, $pattern);  
	}
}

