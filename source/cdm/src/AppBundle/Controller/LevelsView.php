<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\level;

class LevelsView extends Controller
{
	/**
	 * @Route("/levels")
	 */
	public function indexAction(Request $request)
	{

		$levelsRepository = $this->getDoctrine()
			->getRepository('AppBundle:Level');

		$missionsRepository = $this->getDoctrine()
		    ->getRepository('AppBundle:Missions');

		
		$levels = $levelsRepository->findAll();
		$missions = $missionsRepository->findAll();

		$missions=array();

		foreach ($levels as $level){
			$m = $missionsRepository->findOneBy(array('level_id'=> $level->getLevelId()));
			array_push($missions,$m->getMissions());
		}
		
		return $this->render('test/levels_view.html.twig',array('levels' => $levels,'missions' => $missions));
	}
}
