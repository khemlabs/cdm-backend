<?php
// src/AppBundle/Controller/AddSelfie.php
namespace AppBundle\Controller;

use AppBundle\Entity\Selfie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AddSelfie extends Controller
{
    /**
     * @Route("/selfie/add")
     */
    public function createAction(Request $request)
    {

        $spamCheck = $this->get('sithous.antispam');
        if (!$spamCheck->setType('selfie_protection')->verify()) {
            return new JsonResponse(array(
                'result' => 'error',
                'message' => $spamCheck->getErrorMessage(),
            ));
        }

        $usersRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Users');

        $uid = $request->query->get('user_id');
        $cid = $request->query->get('computer_id');

        $check = $usersRepository->findOneBy(array('user_id' => $uid, 'computer_id' => $cid));
        if ($check != null) {

            $passTest = (string) $uid . (string) $cid . "mondongo";
            $encoder = new MessageDigestPasswordEncoder('md5', false, 0);
            $hash = $encoder->encodePassword($passTest, "");

            if ($hash === $request->query->get('hash')) {

                $selfie = new Selfie();
                $selfie->setUserId($request->query->get('user_id'));
                $selfie->setComputerId($request->query->get('computer_id'));
                $selfie->setLevelId($request->query->get('level_id'));
                $selfie->setGameId($request->query->get('game_id'));
                $selfie->setEmojiName($request->query->get('emoji'));
                $selfie->setEstado($request->query->get('estado'));
                $selfie->setCreateTime(new \DateTime("now"));

                $em = $this->getDoctrine()->getManager();

                // tells Doctrine you want to (eventually) save the Product (no queries yet)
                $em->persist($selfie);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();

                return new Response('Selfie add with id ' . $selfie->getId());
            } else {
                return new Response('FORBIDDEN ACCESS');
            }
        }
    }
}
