<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Metrics;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetMetrics extends Controller
{

    private function reduceDialogsByLevels($levels, $dialog)
    {
        $level_id = $dialog->getLevelId();

        //Chequeamos si el dialogo pertenece a los niveles actuales
        if ($level_id > 0 && $level_id < 8) {
            //Chequeamos la existencia de las keys para evitar el notice undefined index
            if (!array_key_exists($level_id, $levels)) {
                $levels[$level_id] = [
                    "matafuegos" => "null",
                    "restaurador" => "null",
                    "aromatizador" => "null",
                    "mision_1" => "null",
                    "mision_2" => "null",
                    "mision_3" => "null",
                    "level_times" => "null",
                    "map_checks" => "null",
                    "level_time" => "null",
                    "game_time" => "null",
                    "first_map_time" => "null",
                    "mission_time" => "null",
                    "tools_time" => "null",
                    "map_trail" => "null",
                    "rt_begin" => "null",
                    "rt_after_tools" => "null",
                    "rt_end" => "null",
                    "level_timestamp" => "null",
                    "level_end" => "null",
                    "portal_done" => "null",
                    "fire_done" => "null",
                    "pollution_done" => "null",
                    "map_dead_ends_vf" => "null",
                    "map_dead_ends_detail" => "null",
                    "avatar_endcharge" => "null",
                    "emmanuel_endcharge" => "null",
                    "agus_endcharge" => "null",
                    "portal_charge" => "null",
                    "fire_charge" => "null",
                    "pollution_charge" => "null",
                    "rt_charge" => "null",
                ];
                $levels[$level_id]["dialogs"] = [];
            }
            $dialog_id = $dialog->getId();
            $levels[$level_id]["dialogs"][$dialog_id] = [
                "dialog_id" => $dialog_id,
                "dialog_index" => "null",
                "dialog_timestamp" => "null",
                "answer_id" => "null",
                "dialog_time" => "null",
            ];
        }

        return $levels;
    }

    /**
     * @Route("alumnos/export/metrics")
     */
    public function obtainMetrics(Request $request)
    {
        //Metricas de los juegos de los alumnos.
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $metricsRepository = $connection->executeQuery(
            'SELECT * 
							FROM metrics m
							ORDER BY m.user_id,m.level_id,m.dialog_id ASC'
        )->fetchAll();
        //Array con todos los usuarios distintos que tienen metricas
        $userRepository = $connection->executeQuery(
            'SELECT DISTINCT(user_id)
							FROM metrics m
							ORDER BY m.user_id ASC'
        )->fetchAll();
				
        //Coleción de todas las posibles preguntas y respuestas.
        $dialogsRepository = $this
            ->getDoctrine()->getRepository('AppBundle:Dialog')
            ->findBy(array(), array('level_id' => 'ASC', "id" => 'ASC'));

        //Se agrupa el array de dialogos a un array que tiene todos los dialogos por nivel
        //Collecion de respuestas por nivel y por dialogo.
        $dialogByLevel = array_reduce($dialogsRepository, array($this, 'reduceDialogsByLevels'), array());

        //Se agrupa a todo los usuarios con un array con todos los niveles y los posibles dialogos.
        $dialogsByUserAndLevel = array();
        foreach ($userRepository as $user) {
            $dialogsByUserAndLevel[$user["user_id"]] = [
                "user_id" => "null",
                "game_id" => "null",
                "games_played" => "null",
                "create_time" => "null",
            ];
            $dialogsByUserAndLevel[$user["user_id"]]["levels"] = $dialogByLevel;
        }
        //Titulos de cada columna
        //Datos del alumno
        $headers = [
            "'USER_ID'",
            "'GAME_ID'",
            "'GAMES_PLAYED'",
            "'CREATE_TIME'",
        ];

        //Datos de cada nivel
        for ($i = 1; $i < 8; $i++) {
            $headers = array_merge($headers, [
                "'MATAFUEGOS_L" . $i ."'",
                "'RESTAURADOR_L" . $i."'",
                "'AROMATIZADOR_L" . $i."'",
                "'MISION_PRINCIPAL_L" . $i."'",
                "'MISION_SECUNDARIA1_L" . $i."'",
                "'MISION_SECUNDARIA2_L" . $i."'",
                "'REPLAY_LEVEL_L" . $i."'",
                "'MAP_CHECKS_L" . $i."'",
                "'LEVEL_TIME_L" . $i."'",
                "'GAME_TIME_L" . $i."'",
                "'FIRST_MAP_TIME_L" . $i."'",
                "'MISSION_TIME_L'" . $i."'",
                "'TOOLS_TIME_L" . $i."'",
                "'MAP_TRAIL_L" . $i."'",
                "'RT_BEGIN_L" . $i."'",
                "'RT_AFTER_TOOLS_L" . $i."'",
                "'RT_END_L" . $i."'",
                "'TIMESTAMP_L" . $i."'",
                "'FIN_NIVEL_L" . $i."'",
                "'PORTAL_DONE_L" . $i."'",
                "'FIRE_DONE_L" . $i."'",
                "'POLLUTION_DONE_L" . $i."'",
                "'MAP_DEAD_ENDS_VF_L" . $i."'",
                "'MAP_DEAD_ENDS_DETALLE_L" . $i."'",
                "'AVATAR_ENDCHARGE_L" . $i ."'",
                "'EMMANUEL_ENDCHARGE_L" . $i."'",
                "'AGUS_ENDCHARGE_L" . $i."'",
                "'PORTAL_CHARGE_L" . $i."'",
                "'FIRE_CHARGE_L" . $i."'",
                "'POLLUTION_CHARGE_L" . $i."'",
                "'RT_CHARGE_L" . $i."'",
            ]);

            //Maxima cantidad de dialogos por nivel
            foreach ($dialogByLevel[$i]["dialogs"] as $levelDialog) {
                $headers = array_merge($headers, [
                    "'DIALOG_ID_L" . $i . "D" . $levelDialog["dialog_id"]."'",
                    "'DIALOG_INDEX_L" . $i . "D" . $levelDialog["dialog_id"]."'",
                    "'DIALOG_TIMESTAMP_L" . $i . "D" . $levelDialog["dialog_id"]."'",
                    "'DIALOG_VALOR_RESPUESTA_L" . $i . "D" . $levelDialog["dialog_id"]."'",
                    "'DIALOG_TIME_L" . $i . "D" . $levelDialog["dialog_id"]."'",
                ]);
            }
        };
        $headers = implode(",", $headers) . "\n";

        $csvMetrics = array_reduce($metricsRepository, function ($csv, $metric) {
            if (array_key_exists($metric["user_id"], $csv)) {
                // $csv[$metric["user_id"]] = array();
                $csv[$metric["user_id"]]["user_id"] = $metric["user_id"];
                $csv[$metric["user_id"]]["game_id"] = $metric["game_id"];
                $csv[$metric["user_id"]]["games_played"] = $metric["games_played"];
                $csv[$metric["user_id"]]["create_time"] = $metric["create_time"];
                // $csv[$metric["user_id"]]["levels"] = array();
                // }

                if (array_key_exists($metric["level_id"], $csv[$metric["user_id"]]["levels"])) {
                    // Se ecuentran todas las apariciones de las herramientas para mostrar el uso de cada una
                    preg_match_all('/:(Restaurador_(?P<restaurador>\d+)|Matafuegos_(?P<matafuegos>\d+)|Aromatizador_(?P<aromatizador>\d+))/', $metric["tools_selected"], $tools);

                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["matafuegos"] = '"'.implode("", $tools["matafuegos"]).'"';
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["restaurador"] ='"'.implode("", $tools["restaurador"]).'"';
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["aromatizador"] = '"'.implode("", $tools["aromatizador"]).'"';

                    // Se separan las misiones por cada una
                    $mission = explode(";", $metric["missions"]);
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["mision_1"] = array_key_exists(0, $mission) ? $mission[0] : 'null';
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["mision_2"] = array_key_exists(1, $mission) ? $mission[1] : 'null';
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["mision_3"] = array_key_exists(2, $mission) ? $mission[2] : 'null';

                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["level_times"] = $metric["level_times"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["map_checks"] = $metric["map_checks"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["level_time"] = $metric["level_time"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["game_time"] = $metric["game_time"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["first_map_time"] = $metric["first_map_time"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["mission_time"] = $metric["mission_time"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["tools_time"] = $metric["tools_time"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["map_trail"] = $metric["map_trail"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["rt_begin"] = $metric["rt_begin"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["rt_after_tools"] = $metric["rt_after_tools"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["rt_end"] = $metric["rt_end"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["level_timestamp"] = $metric["level_timestamp"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["level_end"] = $metric["level_end"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["portal_done"] = $metric["portal_done"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["fire_done"] = $metric["fire_done"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["pollution_done"] = $metric["pollution_done"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["map_dead_ends_vf"] = $metric["map_dead_ends"]; //TODO: Falta una expresión regular que separe y cuente por cuantos callejones sin salida transito
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["map_dead_ends_detail"] = $metric["map_dead_ends"]; //TODO: Ellos quieren una columna por callejon sin salida, hay que ver cuan complicado es hacerlo, ya que para agragarlo como columna hay que saber de antemano cuantos hay por nivel y hoy en dia solo se puede saber parseando el "JSON" de la tabla level_data
                    preg_match_all('/(Avatar:(?P<avatar>\d+)|Emmanuel:(?P<emmanuel>\d+)|Agus:(?P<agus>\d+))/', $metric["tools_end_charge"], $tools_charge);

                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["avatar_endcharge"] = implode($tools_charge["avatar"]);
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["emmanuel_endcharge"] = implode($tools_charge["emmanuel"]);
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["agus_endcharge"] = implode($tools_charge["agus"]);
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["portal_charge"] = $metric["portal_charge"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["fire_charge"] = $metric["fire_charge"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["pollution_charge"] = $metric["pollution_charge"];
                    $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["rt_charge"] = $metric["rt_charge"];
                    // $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"] = array();
                    // }

                    // if (!array_key_exists($metric["dialog_id"], $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"])) {
                    // $csv[$metric["user_id"]][$metric["level_id"]][$metric["dialog_id"]] = array();
                    // }
                    // Chequeo que la key del dialogo exista en la estructura axuliar creada previamente, para no adicionar columnas inexistentes.

                    if (array_key_exists($metric["dialog_id"], $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"])) {
                        $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"][$metric["dialog_id"]]["dialog_id"] = $metric["dialog_id"];
                        $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"][$metric["dialog_id"]]["dialog_index"] = $metric["dialog_index"];
                        $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"][$metric["dialog_id"]]["dialog_timestamp"] = $metric["dialog_timestamp"];
                        $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"][$metric["dialog_id"]]["answer_id"] = $metric["answer_id"];
                        $csv[$metric["user_id"]]["levels"][$metric["level_id"]]["dialogs"][$metric["dialog_id"]]["dialog_time"] = $metric["dialog_time"];
                    }
                }
            }
            return $csv;
        }, $dialogsByUserAndLevel);

        // var_dump($csvMetrics);

        $csv = array_reduce($csvMetrics, function ($csv, $user) {

            $levels = array_reduce($user["levels"], function ($levels, $level) {

                $dialogs = array_reduce($level["dialogs"], function ($dialogs, $dialog) {
                    $d = implode("','", $dialog);
                    $dialogs = $dialogs ? $dialogs . "','" . $d : $d;
                    return $dialogs;
                }, "");
								$lvl = array_filter($level, function ($el) {return !is_array($el);});
								
                $l = implode("','", $lvl) . "','" . $dialogs;
                $newLevels = $levels ? $levels . "','" . $l : $l;
                return $newLevels;
            }, "");
						$row = array_filter($user, function ($el) {return !is_array($el);});
						
            $u = "'". implode("','", $row) . "','" . $levels . "'\n";
            $csv = $csv . $u;
            return $csv;
        }, "");

				//print($csv);
				$csv = $headers . $csv;
				$csv = str_replace('"',"@", $csv);
				$csv = str_replace("'",'"', $csv);
				$csv = str_replace("@","'", $csv);


        // $content = $headers . $csv;
        $response = new Response($csv);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="metrics.csv"');

        return $response;
    }
}
