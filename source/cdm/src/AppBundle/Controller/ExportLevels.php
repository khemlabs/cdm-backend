<?php

namespace AppBundle\Controller;

use appbundle\entity\level;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExportLevels extends Controller
{
    /**
     * @Route("/alumnos/export/levels")
     */
    public function indexAction(Request $request)
    {

        $levelsRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Level');

        $levels = $levelsRepository->findAll();
        $rows = array();

        $data = array(
            "ID",
            "USER_ID",
            "COMPUTER_ID",
            "LEVEL_ID",
            "GAME_ID",
            "TOOLS_SELECTED",
            "MISSIONS",
            "MAP_CHECKS",
            "LEVEL_TIME",
            "GAME_TIME",
            "PHONE_MAP_TIME",
            "FIRST_MAP_TIME",
            "TOOLS_MAP_TIME",
            "MISSION_TIME",
            "TOOLS_TIME",
            "MAP_TRAIL",
            "RT_BEGIN",
            "RT_END",
            "RT_AFTER_TOOLS",
            "MISSION_TIME",
            "TIME_STRING",
            "LEVEL_END",
            "PORTAL_DONE",
            "FIRE_DONE",
            "POLLUTION_DONE",
            "MAP_DEAD_ENDS",
            "TOOLS_END_CHARGE",
            "PORTAL_CHARGE",
            "FIRE_CHARGE",
            "POLLUTION_CHARGE",
            "RT_CHARGE");

        $rows[] = implode('|', $data);

        foreach ($levels as $level) {
            $data = array(
                $level->getId(),
                $level->getUserId(),
                $level->getComputerId(),
                $level->getLevelId(),
                $level->getGameId(),
                $level->getToolsSelected(),
                $level->getMissions(),
                $level->getMapChecks(),
                $level->getLevelTime(),
                $level->getGameTime(),
                $level->getPhoneMapTime(),
                $level->getFirstMapTime(),
                $level->getToolsMapTime(),
                $level->getMissionTime(),
                $level->getToolsTime(),
                $level->getMapTrail(),
                $level->getRtBegin(),
                $level->getRtEnd(),
                //$level->getTimestamp(),
                $level->getRtAfterTools(),
                $level->getMissionTime(),
                $level->getTimeString(),
                $level->getLevelEnd(),
                $level->getPortalDone(),
                $level->getFireDone(),
                $level->getPollutionDone(),
                $level->getMapDeadEnds(),
                $level->getToolsEndCharge(),
                $level->getPortalCharge(),
                $level->getFireCharge(),
                $level->getPollutionCharge(),
                $level->getRtCharge());

            $rows[] = implode('|', $data);
        }

        $content = implode("\n", $rows);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="levels.csv"');

        return $response;

    }
}
