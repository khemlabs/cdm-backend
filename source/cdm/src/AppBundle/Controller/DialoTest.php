<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Dialog;

class DialoTest extends Controller
{
	/**
	 * @Route("/dialog_test")
	 */
	public function indexAction(Request $request)
	{

		$replyRepository = $this->getDoctrine()
			->getRepository('AppBundle:DialogReplies');

		$dialogsRepository = $this->getDoctrine()
		    ->getRepository('AppBundle:Dialog');

		
		$replies = $replyRepository->findAll();

		$dialogs=array();

		foreach ($replies as $reply){
			$d = $dialogsRepository->findOneBy(array('level_id'=> $reply->getLevelId(),
				'character_name'=> $reply->getCharacterName(),
				'dialog_index'=> $reply->getDialogIndex(),
				'dialog_mood'=> $reply->getDialogMood(),
				'dialog_mood'=> $reply->getDialogMood(),
				'answer_id'=> $reply->getAnswerId(),
			));
			if($d)
			array_push($dialogs,$d);
			else
			array_push($dialogs,new Dialog());
		}
		
		return $this->render('test/test_dialog.html.twig',array('replies' => $replies,'dialogs' => $dialogs));
	}
}

