<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use appbundle\entity\level;

class ExportLevelsData extends Controller
{
	/**
	 * @Route("/alumnos/export/levels_data")
	 */
	public function indexAction(Request $request)
	{
		
		$levelsRepository = $this->getDoctrine()
			->getRepository('AppBundle:LevelData');

		$levels = $levelsRepository->findAll();
		$rows = array();

		$data = array(
			"ID",
			"LEVEL_ID",
			"MISSIONS",
			"PORTAL",
			"FIRE",
    			"POLLUTION",
    			"PORTAL_CHARGE",
			"FIRE_CHARGE",
			"POLLUTION_CHARGE",
    			"RESOURCES_CHARGE");

			$rows[] = implode('|', $data);

   		foreach ($levels as $level) {
			$data = array(
				$level->getId(),
				$level->getLevelId(),
    				$level->getMissions(),
				$level->getPortal(),
    				$level->getFire(),
				$level->getPollution(),
				$level->getPortalCharge(),
    				$level->getFireCharge(),
    				$level->getPollutioNCharge(),
    				$level->getResourcesCharge());

			$rows[] = implode('|', $data);
	    	}

		$content = implode("\n", $rows);
		$response = new Response($content);
    		$response->headers->set('Content-Type', 'text/csv');
		$response->headers->set('Content-Disposition', 'attachment; filename="levels_data.csv"');

    		return $response;
		
	}
}

