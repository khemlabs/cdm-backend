<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Sithous\AntiSpamBundle\Entity\SithousAntiSpamType;

class AppFixture extends Fixture
{
	public function load(ObjectManager $manager)
	{
		//Fixed rules for antispam
		$user_protection = new SithousAntiSpamType();
		$user_protection->setId("user_protection");
		$user_protection->setTrackIp(true);
		$user_protection->setTrackUser(false);
		$user_protection->setMaxTime(300);
		$user_protection->setMaxCalls(4);
		$manager->persist($user_protection);

		$dialog_protection = new SithousAntiSpamType();
		$dialog_protection->setId("dialog_protection");
		$dialog_protection->setTrackIp(true);
		$dialog_protection->setTrackUser(false);
		$dialog_protection->setMaxTime(1);
		$dialog_protection->setMaxCalls(3);		
		$manager->persist($dialog_protection);

		$level_protection = new SithousAntiSpamType();
		$level_protection->setId("level_protection");
		$level_protection->setTrackIp(true);
		$level_protection->setTrackUser(false);
		$level_protection->setMaxTime(5);
		$level_protection->setMaxCalls(2);		
		$manager->persist($level_protection);

		$selfie_protection = new SithousAntiSpamType();
		$selfie_protection->setId("selfie_protection");
		$selfie_protection->setTrackIp(true);
		$selfie_protection->setTrackUser(false);
		$selfie_protection->setMaxTime(2);
		$selfie_protection->setMaxCalls(2);		
		$manager->persist($selfie_protection);

		$manager->flush();
	}
}
