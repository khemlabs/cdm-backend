<?php
// src/CustomFos/CustomFosBundle.php

namespace AppBundle\CustomFos\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CustomFosUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
