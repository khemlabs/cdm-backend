<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\CustomFos\UserBundle\Controller;

use FOS\UserBundle\Controller\ResettingController as FosController;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use FOS\UserBundle\Model\UserInterface;
use Gregwar\Captcha\CaptchaBuilder;

/**
 * Controller managing the resetting of the password
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class ResettingController extends FosController
{

    /**
     * Request reset user password: show form
     */
    public function requestAction()
    {
	$builtCaptcha = new CaptchaBuilder();
	$builtCaptcha->build();
	$builtCaptcha->save('captcha.jpg');

	$captcha =crypt($builtCaptcha->getPhrase(),'jp');

        /*return $this->renderLogin(array(
            'last_username' => $lastUsername,
            'error'         => $error,
	    'csrf_token' => $csrfToken,
	    'captcha' => $captcha,
    ));*/
/*	return $this->container->get('templating')->renderResponse(
	'FOSUserBundle:Resetting:request.html.'.$this->getEngine());*/

	    return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(),
		    array('captcha' => $captcha));

    }

    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction()
    {

	 $userCaptcha = substr($this->container->get('request')->request->get('_captcha'), 0, 5);
	 $captcha = $this->container->get('request')->request->get('_k_token');
	    
	 if (!hash_equals($captcha, crypt($userCaptcha, $captcha))) {
		$error = true;
	 	$builtCaptcha = new CaptchaBuilder();
		$builtCaptcha->build();
		$builtCaptcha->save('captcha.jpg');

		$captcha =crypt($builtCaptcha->getPhrase(),'jp');
		return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(),
		    array('error' => $error, 'error_text' => "Captcha invalido", 'captcha' => $captcha));
	 }

        $username = $this->container->get('request')->request->get('username');

        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
		//return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(), array('invalid_username' => $username));
	    $error = true;
	    $builtCaptcha = new CaptchaBuilder();
		$builtCaptcha->build();
		$builtCaptcha->save('captcha.jpg');
		$captcha =crypt($builtCaptcha->getPhrase(),'jp');
		return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.'.$this->getEngine(),
		    array('error' => $error, 'error_text' => "Nombre de usuario inexistente", 'captcha' => $captcha));
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:passwordAlreadyRequested.html.'.$this->getEngine());
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }



        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return new RedirectResponse($this->container->get('router')->generate('fos_user_resetting_check_email'));
    }

   
}

