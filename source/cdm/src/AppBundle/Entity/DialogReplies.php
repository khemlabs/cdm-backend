<?php
// src/AppBundle/Entity/DialogReplies.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dialog_replies")
 */
class DialogReplies
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $computer_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $game_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $character_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $dialog_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $level_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $dialog_index;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $dialog_mood;

    /**
     * @ORM\Column(type="integer")
     */
    private $answer_id;

    /**
     * @ORM\Column(type="float")
     */
    private $dialog_time;

    /** @ORM\Column(type="datetime") */
    private $timestamp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $userId
     * @return DialogReplies
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set computer_id
     *
     * @param string $computerId
     * @return DialogReplies
     */
    public function setComputerId($computerId)
    {
        $this->computer_id = $computerId;

        return $this;
    }

    /**
     * Get computer_id
     *
     * @return string
     */
    public function getComputerId()
    {
        return $this->computer_id;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return DialogReplies
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set dialog_id
     *
     * @param integer $dialog_id
     * @return DialogReplies
     */
    public function setDialogId($dialog_id)
    {
        $this->dialog_id = $dialog_id;

        return $this;
    }

    /**
     * Get dialog_id
     *
     * @return integer
     */
    public function getDialogId()
    {
        return $this->dialog_id;
    }

    
    /**
     * Set game_id
     *
     * @param integer $gameId
     * @return DialogReplies
     */
    public function setGameId($gameId)
    {
        $this->game_id = $gameId;

        return $this;
    }

    public function getGameId(){
        return $this->game_id;
    }

    /**
     * Set character_name
     *
     * @param string $characterName
     * @return DialogReplies
     */
    public function setCharacterName($characterName)
    {
        $this->character_name = $characterName;

        return $this;
    }

    /**
     * Get character_name
     *
     * @return string
     */
    public function getCharacterName()
    {
        return $this->character_name;
    }

    /**
     * Set dialog_index
     *
     * @param integer $dialogIndex
     * @return DialogReplies
     */
    public function setDialogIndex($dialogIndex)
    {
        $this->dialog_index = $dialogIndex;

        return $this;
    }

    /**
     * Get dialog_index
     *
     * @return integer
     */
    public function getDialogIndex()
    {
        return $this->dialog_index;
    }

    /**
     * Set dialog_mood
     *
     * @param string $dialogMood
     * @return DialogReplies
     */
    public function setDialogMood($dialogMood)
    {
        $this->dialog_mood = $dialogMood;

        return $this;
    }

    /**
     * Get dialog_mood
     *
     * @return string
     */
    public function getDialogMood()
    {
        return $this->dialog_mood;
    }

    /**
     * Set answer_id
     *
     * @param integer $answerId
     * @return DialogReplies
     */
    public function setAnswerId($answerId)
    {
        $this->answer_id = $answerId;

        return $this;
    }

    /**
     * Get answer_id
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answer_id;
    }

    /**
     * Set tools_time
     *
     * @param float $toolsTime
     * @return Level
     */
    public function setDialogTime($dialog_time)
    {
        $this->dialog_time = $dialog_time;

        return $this;
    }

    /**
     * Get tools_time
     *
     * @return float
     */
    public function getDialogTime()
    {
        return $this->dialog_time;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return DialogReplies
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function getTimeString()
    {
        return $this->timestamp->format('d-m-Y H:i:s');
    }

}
