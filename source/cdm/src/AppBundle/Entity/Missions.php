<?php
// src/AppBundle/Entity/Missions.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="missions")
 */
class Missions
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    	/**
	* @ORM\Column(type="integer")
	*/
	private $level_id;

	/**
	* @ORM\Column(type="string", length=300)
	*/
	private $missions;


    /** @ORM\Column(type="datetime") */
    private $create_time;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return Missions
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer 
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set missions
     *
     * @param string $missions
     * @return Missions
     */
    public function setMissions($missions)
    {
        $this->missions = $missions;

        return $this;
    }

    /**
     * Get missions
     *
     * @return string 
     */
    public function getMissions()
    {
        return $this->missions;
    }

    /**
     * Set create_time
     *
     * @param \DateTime $createTime
     * @return Missions
     */
    public function setCreateTime($createTime)
    {
        $this->create_time = $createTime;

        return $this;
    }

    /**
     * Get create_time
     *
     * @return \DateTime 
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }
}
