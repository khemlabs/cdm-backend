<?php

// src/AppBundle/Entity/Selfie.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity

 * @ORM\Table(name="selfie")

 */

class Selfie
{

    /**

     * @ORM\Column(type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;

    /**

     * @ORM\Column(type="string", length=100)

     */

    private $user_id;

    /**

     * @ORM\Column(type="string", length=100)

     */

    private $computer_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $level_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $game_id;

    /**

     * @ORM\Column(type="string", length=100)

     */

    private $emoji_name;

    /**

     * @ORM\Column(type="string", length=300)

     */

    private $estado;

    /** @ORM\Column(type="datetime") */

    private $create_time;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $userId
     * @return Selfie
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set computer_id
     *
     * @param string $computerId
     * @return Selfie
     */
    public function setComputerId($computerId)
    {
        $this->computer_id = $computerId;

        return $this;
    }

    /**
     * Get computer_id
     *
     * @return string
     */
    public function getComputerId()
    {
        return $this->computer_id;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return Selfie
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set game_id
     *
     * @param integer $gameId
     * @return Level
     */
    public function setGameId($gameId)
    {
        $this->game_id = $gameId;

        return $this;
    }

    /**
     * Get game_id
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->game_id;
    }

    /**
     * Set emoji_name
     *
     * @param string $emojiName
     * @return Selfie
     */
    public function setEmojiName($emojiName)
    {
        $this->emoji_name = $emojiName;

        return $this;
    }

    /**
     * Get emoji_name
     *
     * @return string
     */
    public function getEmojiName()
    {
        return $this->emoji_name;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Selfie
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set create_time
     *
     * @param \DateTime $createTime
     * @return Selfie
     */
    public function setCreateTime($createTime)
    {
        $this->create_time = $createTime;

        return $this;
    }

    /**
     * Get create_time
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }

    public function getTimeString()
    {
        return $this->create_time->format('d-m-Y H:i:s');
    }
}
