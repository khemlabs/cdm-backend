<?php
// src/AppBundle/Entity/Dialog.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dialogs")
 */
class Dialog
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $character_name;

     /**
	* @ORM\Column(type="integer")
	*/
	private $level_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $dialog_type;

	/**
	* @ORM\Column(type="integer")
	*/
	private $dialog_index;

	/**
     	* @ORM\Column(type="string", length=100)
     	*/
	private $dialog_mood;

	/**
     	* @ORM\Column(type="string", length=300)
     	*/
	private $dialog_prompt;

	/**
	* @ORM\Column(type="integer")
	*/
	private $answer_id;

	/**
	*  @ORM\Column(type="string", length=300)
	*/
	private $answer_text;

	/**
	*  @ORM\Column(type="string", length=300)
	*/
	private $tipo_indicador;

	/**
	*  @ORM\Column(type="string", length=300)
	*/
	private $subtipo_indicador;

	/**
	*  @ORM\Column(type="string", length=300)
	*/
	private $valor_indicador;




    /** @ORM\Column(type="datetime") */
    private $create_time;

  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set character_name
     *
     * @param string $characterName
     * @return Dialogs
     */
    public function setCharacterName($characterName)
    {
        $this->character_name = $characterName;

        return $this;
    }

    /**
     * Get character_name
     *
     * @return string 
     */
    public function getCharacterName()
    {
        return $this->character_name;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return Dialogs
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer 
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set dialog_type
     *
     * @param string $dialogType
     * @return Dialogs
     */
    public function setDialogType($dialogType)
    {
        $this->dialog_type = $dialogType;

        return $this;
    }

    /**
     * Get dialog_type
     *
     * @return string 
     */
    public function getDialogType()
    {
        return $this->dialog_type;
    }

    /**
     * Set dialog_index
     *
     * @param integer $dialogIndex
     * @return Dialogs
     */
    public function setDialogIndex($dialogIndex)
    {
        $this->dialog_index = $dialogIndex;

        return $this;
    }

    /**
     * Get dialog_index
     *
     * @return integer 
     */
    public function getDialogIndex()
    {
        return $this->dialog_index;
    }

    /**
     * Set dialog_mood
     *
     * @param string $dialogMood
     * @return Dialogs
     */
    public function setDialogMood($dialogMood)
    {
        $this->dialog_mood = $dialogMood;

        return $this;
    }

    /**
     * Get dialog_mood
     *
     * @return string 
     */
    public function getDialogMood()
    {
        return $this->dialog_mood;
    }

    /**
     * Set dialog_prompt
     *
     * @param string $dialogPrompt
     * @return Dialogs
     */
    public function setDialogPrompt($dialogPrompt)
    {
        $this->dialog_prompt = $dialogPrompt;

        return $this;
    }

    /**
     * Get dialog_prompt
     *
     * @return string 
     */
    public function getDialogPrompt()
    {
        return $this->dialog_prompt;
    }

    /**
     * Set answer_id
     *
     * @param integer $answerId
     * @return Dialogs
     */
    public function setAnswerId($answerId)
    {
        $this->answer_id = $answerId;

        return $this;
    }

    /**
     * Get answer_id
     *
     * @return integer 
     */
    public function getAnswerId()
    {
        return $this->answer_id;
    }

    /**
     * Set answer_text
     *
     * @param string $answerText
     * @return Dialogs
     */
    public function setAnswerText($answerText)
    {
        $this->answer_text = $answerText;

        return $this;
    }

    /**
     * Get answer_text
     *
     * @return string 
     */
    public function getAnswerText()
    {
        return $this->answer_text;
    }

    /**
     * Set create_time
     *
     * @param \DateTime $createTime
     * @return Dialogs
     */
    public function setCreateTime($createTime)
    {
        $this->create_time = $createTime;

        return $this;
    }

    /**
     * Get create_time
     *
     * @return \DateTime 
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }

    

    /**
     * Set tipo_indicador
     *
     * @param string $tipoIndicador
     * @return Dialog
     */
    public function setTipoIndicador($tipoIndicador)
    {
        $this->tipo_indicador = $tipoIndicador;

        return $this;
    }

    /**
     * Get tipo_indicador
     *
     * @return string 
     */
    public function getTipoIndicador()
    {
        return $this->tipo_indicador;
    }

    /**
     * Set subtipo_indicador
     *
     * @param string $subtipoIndicador
     * @return Dialog
     */
    public function setSubtipoIndicador($subtipoIndicador)
    {
        $this->subtipo_indicador = $subtipoIndicador;

        return $this;
    }

    /**
     * Get subtipo_indicador
     *
     * @return string 
     */
    public function getSubtipoIndicador()
    {
        return $this->subtipo_indicador;
    }

    /**
     * Set valor_indicador
     *
     * @param string $valorIndicador
     * @return Dialog
     */
    public function setValorIndicador($valorIndicador)
    {
        $this->valor_indicador = $valorIndicador;

        return $this;
    }

    /**
     * Get valor_indicador
     *
     * @return string 
     */
    public function getValorIndicador()
    {
        return $this->valor_indicador;
    }
}
