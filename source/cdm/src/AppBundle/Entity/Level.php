<?php
// src/AppBundle/Entity/Level.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="level")
 */

class Level
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $computer_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $level_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $game_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tools_selected;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tools_end_charge;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $missions;

    /**
     * @ORM\Column(type="integer")
     */
    private $portal_done;

    /**
     * @ORM\Column(type="integer")
     */
    private $fire_done;

    /**
     * @ORM\Column(type="integer")
     */
    private $pollution_done;

    /**
     * @ORM\Column(type="integer")
     */
    private $map_checks;

    /**
     * @ORM\Column(type="float")
     */
    private $level_time;

    /**
     * @ORM\Column(type="float")
     */
    private $game_time;

    /**
     * @ORM\Column(type="float")
     */
    private $phone_map_time;

    /**
     * @ORM\Column(type="float")
     */
    private $first_map_time;

    /**
     * @ORM\Column(type="float")
     */
    private $tools_map_time;

    /**
     * @ORM\Column(type="float")
     */
    private $mission_time;

    /**
     * @ORM\Column(type="float")
     */
    private $tools_time;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $map_trail;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $map_dead_ends;

    /**
     * @ORM\Column(type="integer")
     */
    private $rt_begin;

    /**
     * @ORM\Column(type="integer")
     */
    private $rt_after_tools;

    /**
     * @ORM\Column(type="integer")
     */
    private $rt_end;

    /**
     * @ORM\Column(type="integer")
     */
    private $portal_charge;

    /**
     * @ORM\Column(type="integer")
     */
    private $fire_charge;

    /**
     * @ORM\Column(type="integer")
     */
    private $pollution_charge;

    /**
     * @ORM\Column(type="integer")
     */
    private $rt_charge;

    /**
     * @ORM\Column(type="integer")
     */
    private $level_end;

    /** @ORM\Column(type="datetime") */
    private $timestamp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $userId
     * @return Level
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set computer_id
     *
     * @param string $computerId
     * @return Level
     */
    public function setComputerId($computerId)
    {
        $this->computer_id = $computerId;

        return $this;
    }

    /**
     * Get computer_id
     *
     * @return string
     */
    public function getComputerId()
    {
        return $this->computer_id;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return Level
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set game_id
     *
     * @param integer $gameId
     * @return Level
     */
    public function setGameId($gameId)
    {
        $this->game_id = $gameId;

        return $this;
    }

    /**
     * Get game_id
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->game_id;
    }

    /**
     * Set tools_selected
     *
     * @param string $toolsSelected
     * @return Level
     */
    public function setToolsSelected($toolsSelected)
    {
        $this->tools_selected = $toolsSelected;

        return $this;
    }

    /**
     * Get tools_selected
     *
     * @return string
     */
    public function getToolsSelected()
    {
        return $this->tools_selected;
    }

    /**
     * Set missions
     *
     * @param string $missions
     * @return Level
     */
    public function setMissions($missions)
    {
        $this->missions = $missions;

        return $this;
    }

    /**
     * Get missions
     *
     * @return string
     */
    public function getMissions()
    {
        return $this->missions;
    }

    /**
     * Set map_checks
     *
     * @param integer $mapChecks
     * @return Level
     */
    public function setMapChecks($mapChecks)
    {
        $this->map_checks = $mapChecks;

        return $this;
    }

    /**
     * Get map_checks
     *
     * @return integer
     */
    public function getMapChecks()
    {
        return $this->map_checks;
    }

    /**
     * Set level_time
     *
     * @param float $levelTime
     * @return Level
     */
    public function setLevelTime($levelTime)
    {
        $this->level_time = $levelTime;

        return $this;
    }

    /**
     * Get level_time
     *
     * @return float
     */
    public function getLevelTime()
    {
        return $this->level_time;
    }

    /**
     * Set game_time
     *
     * @param float $gameTime
     * @return Level
     */
    public function setGameTime($gameTime)
    {
        $this->game_time = $gameTime;

        return $this;
    }

    /**
     * Get game_time
     *
     * @return float
     */
    public function getGameTime()
    {
        return $this->game_time;
    }

    /**
     * Set first_map_time
     *
     * @param float $firstMapTime
     * @return Level
     */
    public function setPhoneMapTime($phoneMapTime)
    {
        $this->phone_map_time = $phoneMapTime;

        return $this;
    }

    /**
     * Get first_map_time
     *
     * @return float
     */
    public function getPhoneMapTime()
    {
        return $this->phone_map_time;
    }

    /**
     * Set first_map_time
     *
     * @param float $firstMapTime
     * @return Level
     */
    public function setFirstMapTime($firstMapTime)
    {
        $this->first_map_time = $firstMapTime;

        return $this;
    }

    /**
     * Get first_map_time
     *
     * @return float
     */
    public function getFirstMapTime()
    {
        return $this->first_map_time;
    }

    /**
     * Set first_map_time
     *
     * @param float $firstMapTime
     * @return Level
     */
    public function setToolsMapTime($toolsMapTime)
    {
        $this->tools_map_time = $toolsMapTime;

        return $this;
    }

    /**
     * Get first_map_time
     *
     * @return float
     */
    public function getToolsMapTime()
    {
        return $this->tools_map_time;
    }

    /**
     * Set mision_time
     *
     * @param float $misionTime
     * @return Level
     */
    public function setMisionTime($misionTime)
    {
        $this->mision_time = $misionTime;

        return $this;
    }

    /**
     * Get mision_time
     *
     * @return float
     */
    public function getMisionTime()
    {
        return $this->mision_time;
    }

    /**
     * Set tools_time
     *
     * @param float $toolsTime
     * @return Level
     */
    public function setToolsTime($toolsTime)
    {
        $this->tools_time = $toolsTime;

        return $this;
    }

    /**
     * Get tools_time
     *
     * @return float
     */
    public function getToolsTime()
    {
        return $this->tools_time;
    }

    /**
     * Set map_trail
     *
     * @param string $mapTrail
     * @return Level
     */
    public function setMapTrail($mapTrail)
    {
        $this->map_trail = $mapTrail;

        return $this;
    }

    /**
     * Get map_trail
     *
     * @return string
     */
    public function getMapTrail()
    {
        return $this->map_trail;
    }

    /**
     * Set rt_begin
     *
     * @param integer $rtBegin
     * @return Level
     */
    public function setRtBegin($rtBegin)
    {
        $this->rt_begin = $rtBegin;

        return $this;
    }

    /**
     * Get rt_begin
     *
     * @return integer
     */
    public function getRtBegin()
    {
        return $this->rt_begin;
    }

    /**
     * Set rt_end
     *
     * @param integer $rtEnd
     * @return Level
     */
    public function setRtEnd($rtEnd)
    {
        $this->rt_end = $rtEnd;

        return $this;
    }

    /**
     * Get rt_end
     *
     * @return integer
     */
    public function getRtEnd()
    {
        return $this->rt_end;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Level
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set rt_after_tools
     *
     * @param integer $rtAfterTools
     * @return Level
     */
    public function setRtAfterTools($rtAfterTools)
    {
        $this->rt_after_tools = $rtAfterTools;

        return $this;
    }

    /**
     * Get rt_after_tools
     *
     * @return integer
     */
    public function getRtAfterTools()
    {
        return $this->rt_after_tools;
    }

    /**
     * Set mission_time
     *
     * @param float $missionTime
     * @return Level
     */
    public function setMissionTime($missionTime)
    {
        $this->mission_time = $missionTime;

        return $this;
    }

    /**
     * Get mission_time
     *
     * @return float
     */
    public function getMissionTime()
    {
        return $this->mission_time;
    }

    public function getTimeString()
    {
        return $this->timestamp->format('d-m-Y H:i:s');
    }

    /**
     * Set level_end
     *
     * @param integer $levelEnd
     * @return Level
     */
    public function setLevelEnd($levelEnd)
    {
        $this->level_end = $levelEnd;

        return $this;
    }

    /**
     * Get level_end
     *
     * @return integer
     */
    public function getLevelEnd()
    {
        return $this->level_end;
    }

    /**
     * Set portal_done
     *
     * @param integer $portalDone
     * @return Level
     */
    public function setPortalDone($portalDone)
    {
        $this->portal_done = $portalDone;

        return $this;
    }

    /**
     * Get portal_done
     *
     * @return integer
     */
    public function getPortalDone()
    {
        return $this->portal_done;
    }

    /**
     * Set fire_done
     *
     * @param integer $fireDone
     * @return Level
     */
    public function setFireDone($fireDone)
    {
        $this->fire_done = $fireDone;

        return $this;
    }

    /**
     * Get fire_done
     *
     * @return integer
     */
    public function getFireDone()
    {
        return $this->fire_done;
    }

    /**
     * Set pollution_done
     *
     * @param integer $pollutionDone
     * @return Level
     */
    public function setPollutionDone($pollutionDone)
    {
        $this->pollution_done = $pollutionDone;

        return $this;
    }

    /**
     * Get pollution_done
     *
     * @return integer
     */
    public function getPollutionDone()
    {
        return $this->pollution_done;
    }

    /**
     * Set map_dead_ends
     *
     * @param string $mapDeadEnds
     * @return Level
     */
    public function setMapDeadEnds($mapDeadEnds)
    {
        $this->map_dead_ends = $mapDeadEnds;

        return $this;
    }

    /**
     * Get map_dead_ends
     *
     * @return string
     */
    public function getMapDeadEnds()
    {
        return $this->map_dead_ends;
    }

    /**
     * Set tools_end_charge
     *
     * @param string $toolsEndCharge
     * @return Level
     */
    public function setToolsEndCharge($toolsEndCharge)
    {
        $this->tools_end_charge = $toolsEndCharge;

        return $this;
    }

    /**
     * Get tools_end_charge
     *
     * @return string
     */
    public function getToolsEndCharge()
    {
        return $this->tools_end_charge;
    }

    /**
     * Set portal_charge
     *
     * @param integer $portalCharge
     * @return Level
     */
    public function setPortalCharge($portalCharge)
    {
        $this->portal_charge = $portalCharge;

        return $this;
    }

    /**
     * Get portal_charge
     *
     * @return integer
     */
    public function getPortalCharge()
    {
        return $this->portal_charge;
    }

    /**
     * Set fire_charge
     *
     * @param integer $fireCharge
     * @return Level
     */
    public function setFireCharge($fireCharge)
    {
        $this->fire_charge = $fireCharge;

        return $this;
    }

    /**
     * Get fire_charge
     *
     * @return integer
     */
    public function getFireCharge()
    {
        return $this->fire_charge;
    }

    /**
     * Set pollution_charge
     *
     * @param integer $pollutionCharge
     * @return Level
     */
    public function setPollutionCharge($pollutionCharge)
    {
        $this->pollution_charge = $pollutionCharge;

        return $this;
    }

    /**
     * Get pollution_charge
     *
     * @return integer
     */
    public function getPollutionCharge()
    {
        return $this->pollution_charge;
    }

    /**
     * Set rt_charge
     *
     * @param integer $rtCharge
     * @return Level
     */
    public function setRtCharge($rtCharge)
    {
        $this->rt_charge = $rtCharge;

        return $this;
    }

    /**
     * Get rt_charge
     *
     * @return integer
     */
    public function getRtCharge()
    {
        return $this->rt_charge;
    }
}
