<?php
// src/AppBundle/Entity/Users.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class Users
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $computer_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $user_name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cue;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $grado;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $turno;

    /** @ORM\Column(type="datetime") */
    private $create_time;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $userId
     * @return Users
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set computer_id
     *
     * @param string $computerId
     * @return Users
     */
    public function setComputerId($computerId)
    {
        $this->computer_id = $computerId;

        return $this;
    }

    /**
     * Get computer_id
     *
     * @return string
     */
    public function getComputerId()
    {
        return $this->computer_id;
    }

    /**
     * Set create_time
     *
     * @param \DateTime $createTime
     * @return Users
     */
    public function setCreateTime($createTime)
    {
        $this->create_time = $createTime;

        return $this;
    }

    /**
     * Get create_time
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }

    /**
     * Set user_name
     *
     * @param string $userName
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->user_name = $userName;

        return $this;
    }

    /**
     * Get user_name
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set cue
     *
     * @param string $cue
     * @return Users
     */
    public function setCue($cue)
    {
        $this->cue = $cue;

        return $this;
    }

    /**
     * Get cue
     *
     * @return string
     */
    public function getCue()
    {
        return $this->cue;
    }

    /**
     * Set grado
     *
     * @param string $grado
     * @return Users
     */
    public function setGrado($grado)
    {
        $this->grado = $grado;

        return $this;
    }

    /**
     * Get grado
     *
     * @return string
     */
    public function getGrado()
    {
        return $this->grado;
    }

    /**
     * Set division
     *
     * @param string $division
     * @return Users
     */
    public function setDivision($division)
    {
        $this->division = $division;

        return $this;
    }

    /**
     * Get division
     *
     * @return string
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Set turno
     *
     * @param string $turno
     * @return Users
     */
    public function setTurno($turno)
    {
        $this->turno = $turno;

        return $this;
    }

    /**
     * Get turno
     *
     * @return string
     */
    public function getTurno()
    {
        return $this->turno;
    }

    public function getTimeString()
    {
        return $this->create_time->format('d-m-Y H:i:s');
    }
}
