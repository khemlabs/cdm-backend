<?php
// src/AppBundle/Entity/LevelData.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="level_data")
 */
class LevelData
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $level_id;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $missions;

    /**
     * @ORM\Column(type="integer")
     */
    private $portal;

    /**
     * @ORM\Column(type="integer")
     */
    private $fire;

    /**
     * @ORM\Column(type="integer")
     */
    private $pollution;

    /**
     * @ORM\Column(type="integer")
     */
    private $portalCharge;

    /**
     * @ORM\Column(type="integer")
     */
    private $fireCharge;

    /**
     * @ORM\Column(type="integer")
     */
    private $pollutionCharge;

    /**
     * @ORM\Column(type="integer")
     */
    private $resourcesCharge;

    /** @ORM\Column(type="datetime") */
    private $create_time;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level_id
     *
     * @param integer $levelId
     * @return Missions
     */
    public function setLevelId($levelId)
    {
        $this->level_id = $levelId;

        return $this;
    }

    /**
     * Get level_id
     *
     * @return integer
     */
    public function getLevelId()
    {
        return $this->level_id;
    }

    /**
     * Set missions
     *
     * @param string $missions
     * @return Missions
     */
    public function setMissions($missions)
    {
        $this->missions = $missions;

        return $this;
    }

    /**
     * Get missions
     *
     * @return string
     */
    public function getMissions()
    {
        return $this->missions;
    }

    /**
     * Set create_time
     *
     * @param \DateTime $createTime
     * @return Missions
     */
    public function setCreateTime($createTime)
    {
        $this->create_time = $createTime;

        return $this;
    }

    /**
     * Get create_time
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->create_time;
    }

    /**
     * Set portal
     *
     * @param integer $portal
     * @return LevelData
     */
    public function setPortal($portal)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal
     *
     * @return integer
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set fire
     *
     * @param integer $fire
     * @return LevelData
     */
    public function setFire($fire)
    {
        $this->fire = $fire;

        return $this;
    }

    /**
     * Get fire
     *
     * @return integer
     */
    public function getFire()
    {
        return $this->fire;
    }

    /**
     * Set pollution
     *
     * @param integer $pollution
     * @return LevelData
     */
    public function setPollution($pollution)
    {
        $this->pollution = $pollution;

        return $this;
    }

    /**
     * Get pollution
     *
     * @return integer
     */
    public function getPollution()
    {
        return $this->pollution;
    }

    /**
     * Set portalCharge
     *
     * @param integer $portalCharge
     * @return LevelData
     */
    public function setPortalCharge($portalCharge)
    {
        $this->portalCharge = $portalCharge;

        return $this;
    }

    /**
     * Get portalCharge
     *
     * @return integer
     */
    public function getPortalCharge()
    {
        return $this->portalCharge;
    }

    /**
     * Set fireCharge
     *
     * @param integer $fireCharge
     * @return LevelData
     */
    public function setFireCharge($fireCharge)
    {
        $this->fireCharge = $fireCharge;

        return $this;
    }

    /**
     * Get fireCharge
     *
     * @return integer
     */
    public function getFireCharge()
    {
        return $this->fireCharge;
    }

    /**
     * Set pollutionCharge
     *
     * @param integer $pollutionCharge
     * @return LevelData
     */
    public function setPollutionCharge($pollutionCharge)
    {
        $this->pollutionCharge = $pollutionCharge;

        return $this;
    }

    /**
     * Get pollutionCharge
     *
     * @return integer
     */
    public function getPollutionCharge()
    {
        return $this->pollutionCharge;
    }

    /**
     * Set resourcesCharge
     *
     * @param integer $resourcesCharge
     * @return LevelData
     */
    public function setResourcesCharge($resourcesCharge)
    {
        $this->resourcesCharge = $resourcesCharge;

        return $this;
    }

    /**
     * Get resourcesCharge
     *
     * @return integer
     */
    public function getResourcesCharge()
    {
        return $this->resourcesCharge;
    }
}
