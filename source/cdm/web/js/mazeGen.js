var maze = {};
var mazeLoaded = false;
$.getJSON("../data/maze.json", function( data ) {
	maze = data;
	mazeLoaded=true;
});

function createMaze(e){	
	var level = Number($(e).attr('title'));
	var trail = $(e).attr('name');
	var deadEnds = $(e).attr('alt').split(";");
	for(var i=0;i<deadEnds.length;i++){
		if(deadEnds[i]!==""){
			//console.log(deadEnds[i]);
			deadEnds[i]=JSON.parse(deadEnds[i]);
		}
	}
	if(mazeLoaded){		
		var cells = maze[level]["Maze"];
		size = cells[cells.length-1]["id"].split("_");
		var width = Number(size[0])+1;
		var height = Number(size[1])+1;
		//console.log(width+" - "+height);
		var html = "<button class='trail_0'>abrir</button>";
		html += "<table>";	
		for(var i=0;i<height;i++){
			html+="<tr>";
			for(var j=0;j<width;j++){
				var fill = '\xa0';				
				var cell = $.grep(cells, function(e){ return e.id == j+"_"+i; });
				if(trail.match("^"+cell[0].id+";")!=null||trail.match(";"+cell[0].id+";")!=null){
					fill = "X";
					//console.log(cell[0].id+":"+trail);
				}
				var dE = deadEnds.find(x => x.id === cell[0].id);
				//console.log(cell[0].id+":"+dE.id);
				if(dE!=undefined){
					if(dE["times"]>0)
					fill = dE["times"];
				}
	
				html+="<td id="+j+"_"+i+" style='"+getBorders(cell[0])+"'>"+fill+"</td>";
				//html+="<td style=''>&nbsp</td>";
			}
			html+="</tr>";
		}
		html+="</table>";
		$(e).html(html);
		$(e).find("table").hide();

		$('[class^="trail"]').unbind('click').click(function() {
			if(Number($(this).attr("class").split("_")[1])==0){
				$(this).siblings("table").show();
				$(this).text("cerrar");
				$(this).attr("class","trail_1");
			}else{
				$(this).siblings("table").hide();
				$(this).text("abrir");
				$(this).attr("class","trail_0");
			}
		});

	}else{
		setTimeout(function(){createMaze(e);},500);
	}


}

function getBorders(cell){
	var css = "";
	var matchN = cell["north"].match(/INVISIBLE|IN|OUT/);
	var matchS = cell["south"].match(/INVISIBLE|IN|OUT/);
	var matchE = cell["east"].match(/INVISIBLE|IN|OUT/);
	var matchW = cell["west"].match(/INVISIBLE|IN|OUT/);
	
	//console.log(matchN);
	if(matchN !== null && typeof variable !== "object")	
		css+="border-top:none;";
	if(matchS !== null && typeof variable !== "object")	
		css+="border-bottom:none;";
	if(matchW !== null && typeof variable !== "object")	
		css+="border-left:none;";
	if(matchE !== null && typeof variable !== "object")	
		css+="border-right:none;";		
	
	//console.log(css);
	return css;
}

$( ".maze" ).each(function( index ) {
	 createMaze(this);
});

