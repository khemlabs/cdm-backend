############################################################
# Dockerfile to build CDM Server
# Based on wordpress:php5.6-fpm
############################################################
FROM php:5.6-fpm

ARG SYMFONY__SECRET__TOKEN
ARG SYMFONY__MYSQL__HOST
ARG SYMFONY__MYSQL__PORT
ARG SYMFONY__MYSQL__DATABASE
ARG SYMFONY__MYSQL__USER
ARG SYMFONY__MYSQL__PASSWORD
ARG WITH_XDEBUG
ARG XDEBUG_HOST
ARG XDEBUG_PORT
ARG XDEBUG_KEY

ENV SYMFONY__SECRET__TOKEN ${SYMFONY__SECRET__TOKEN}
ENV SYMFONY__MYSQL__HOST ${SYMFONY__MYSQL__HOST}
ENV SYMFONY__MYSQL__PORT ${SYMFONY__MYSQL__PORT}
ENV SYMFONY__MYSQL__DATABASE ${SYMFONY__MYSQL__DATABASE}
ENV SYMFONY__MYSQL__USER ${SYMFONY__MYSQL__USER}
ENV SYMFONY__MYSQL__PASSWORD ${SYMFONY__MYSQL__PASSWORD}
ENV XDEBUG_HOST ${XDEBUG_HOST}
ENV XDEBUG_PORT ${XDEBUG_PORT}
ENV XDEBUG_HOST ${XDEBUG_KEY}

### PHP Dependency Installation
RUN apt-get update && apt-get install -y \
	libfreetype6-dev \
	libjpeg62-turbo-dev \
	libmcrypt-dev \
	libpng-dev \
	mcrypt \
	libpq-dev \
	zlib1g-dev \
	libc-client-dev \ 
	libkrb5-dev \
	libxml2-dev \
	unzip \
	&& docker-php-ext-install iconv \
	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
	&& docker-php-ext-install gd pdo mysql pdo_mysql imap xml zip

### Enable XDEBUG
RUN if [ ${WITH_XDEBUG} = "true" ] ; then \
	pecl install xdebug-2.5.5; \
	docker-php-ext-enable xdebug; \
	echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.default_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_log=/dev/stdout" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.idekey=${XDEBUG_KEY}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_host=${XDEBUG_HOST:-host.docker.internal}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	echo "xdebug.remote_port=${XDEBUG_PORT:-9001}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
	fi ;

### PHP Config Files
COPY config/php.ini /usr/local/etc/php/
COPY config/www.conf /usr/local/etc/php-fpm.d/

### Composer Installation
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp
ENV COMPOSER_VERSION 1.7.2
ENV COMPOSER_INSTALLER_URL https://getcomposer.org/installer
ENV COMPOSER_INSTALLER_HASH 544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061

RUN curl --silent --fail --location --retry 3 --output /tmp/installer.php --url https://raw.githubusercontent.com/composer/getcomposer.org/b107d959a5924af895807021fcef4ffec5a76aa9/web/installer \
	&& php -r " \
	\$signature = '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061'; \
	\$hash = hash('SHA384', file_get_contents('/tmp/installer.php')); \
	if (!hash_equals(\$signature, \$hash)) { \
	unlink('/tmp/installer.php'); \
	echo 'Integrity check failed, installer is either corrupt or worse.' . PHP_EOL; \
	exit(1); \
	}" \
	&& php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
	&& composer --ansi --version --no-interaction \
	&& rm -rf /tmp/* /tmp/.htaccess
RUN chown -R root:www-data /tmp && chmod -R ug+rw /tmp

### CMD App
COPY composer.json /source/composer.json
COPY composer.lock /source/composer.lock

WORKDIR /source

RUN composer install --no-scripts --no-autoloader

COPY ./ /source
RUN set -ex; \
	chown -R www-data:www-data /source; \
	chmod -R 775 /source/web; \
	chmod -R ug+s /source/web; \
	if [-a /source/composer.lock]; then \
	echo "Removing composer.lock" \
	&& rm /source/composer.lock; \
	else \
	echo "composer.lock not found"; \
	fi;

EXPOSE 9000

USER www-data

RUN composer dump-autoload --optimize && \
	composer run-script post-install-cmd

ENTRYPOINT ["/bin/sh", "./docker-entrypoint.sh"]

CMD ["php-fpm"]