CHANGELOG para 1.5
===================

 * Corrección Vulnerabilidad-0001. Agregado de captcha en formulario de recuperación de contraseña.
 * Agregado de captura de tiempo de dialogos en VJ y base de datos.
