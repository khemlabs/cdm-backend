UPDATE para 1.4
===================

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Sobreescribir archivo para arreglar error al evitar que se muestre error 403 en navegador:

	$ cp -f /var/www/html/edu-28-crucedemundos/overwrites/ExceptionController.php /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

+ Limpiar el cache de la aplicación web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm
