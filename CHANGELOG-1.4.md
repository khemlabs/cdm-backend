CHANGELOG para 1.4
===================

 * Corrección incidencia JIRA APPCRUCEMU-23.
 * Corrección incidencia JIRA APPCRUCEMU-31.
 * Corrección incidencia JIRA APPCRUCEMU-32.
