# CHANGELOG para 1.10

- Se agrego vista de metricas para consulta de psicometria
- Se implementaron los fixes para el assestment de seguridad devuelto por la ASI
- Se adjunto la nueva version del videojuego
- Se documentaron los cambios de configuracion pedido por la ASI
