UPDATE para 1.10
===================

+ Clonar o Actualizar el repositorio:

  - Clonar
    $ git clone git@git-asi.buenosaires.gob.ar:usuarioQA/edu-28-crucedemundos.git
  - Actualizar
	$ cd /var/www/html/edu-28-crucedemundos/
    $ git pull

+ Nuevas configuraciones adiccionales de seguridad segun Assetment de la ASI
 - Editar el archivo de virtualhost de apache:
   $ vim /etc/http/conf.d/crucedemundos.conf
 - Agregar la siguiente linea en VirtualHost
   Redirect 404 /.htaccess
 - Editar la linea de directory Options por:
   Options -Indexes FollowSymLinks MultiViews
 - Editar el archivo php.ini y agregar la siguiente linea:
   expose_php=Off
   
+ Borrar la cache del repositorio:
 - Sin Docker:
   $ cd /var/www/html/edu-28-crucedemundos/source/cdm
   $ rm -rf app/cache/*
   $ php app/console cache:clear
   $ php app/console cache:warmup
 - Con Docker:
   $ cd /var/www/html/edu-28-crucedemundos/
   $ make clear-cache
