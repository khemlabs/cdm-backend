CHANGELOG
=========

1.1
-----

 * Correcci�n incidencia JIRA 8.1.1 "Texto  solapado  con  globo  de  di�logo"
 * Correcci�n incidencia JIRA 8.1.3 "Error  ortogr�fico  al  finalizar  nivel  4"
 * Correcci�n incidencia JIRA 8.1.4 "El  audio  contin�a  escuch�ndose  cuando  la aplicaci�n no est� en primer plano"
 * Correcci�n incidencia JIRA 8.1.5 "Nombre  de  personaje  aparece  de  forma incorrecta"
 * Correcci�n incidencia JIRA 8.1.6 "Chequeo  de  spam  en  invocaciones"
 * Correcci�n incidencia JIRA 8.1.7 "Manejo  de  usuarios  /  password"
 * Correcci�n incidencia JIRA 8.1.8 "Captcha  en  Backend"
 * Correcci�n incidencia JIRA 8.1.9 "Falta  p�gina  de  manejo  de  errores"
 * Correcci�n incidencia JIRA 8.1.10 "El usuario debe acceder a la carpeta de instalaci�n para correrla Creada"
 * Correcci�n incidencia JIRA 8.1.11 "Los personajes atraviesan paredes al doblar muy cerrado"
 * Correcci�n incidencia JIRA 8.1.12 "El  sistema  no  permite  salir  del  juego"
 * Agregado del backend para docentes y evaluadores
 * Implementaci�n de la librer�a FOS User Bundle para gestionar los usuarios del backend
 * Implementaci�n de la librer�a GregwarCaptchaBundle para utilizar captcha en formulario
 * Implementaci�n de la librer�a SithousAntiSpam Bundle para evitar spam en invocaciones
 * Agregado de cinem�ticas y final del juego en el videjuego
 * Niveles eliminados en el �rea 1 y en el �rea 3 del videojuego
 * Agregado de grupos en los di�logos
 * Correcci�n de estilo y ortogr�fica final en di�logos en el videojuego
 * Mejoras en pantallas de selecci�n de herramientas en el videojugo
 * Agregado de explicaciones sobre el uso de los recursos tecnol�gicos y herramientas en el videojuego
 * Agregado de interacciones y di�logos en el primer nivel para explicar la mec�nica del videjuego
 * Agregado de nuevos elementos visuales que den variedad a los niveles en el videojuego
 * Agregado de nuevos elementos visuales que den variedad a los niveles en el videojuego
 * Correcci�n de frases de estados en las selfies del videojuego
 * Correcci�n de la falta de la �ltima pregunta sobre Autoconcepto en el videojuego
 * Agregado de opciones femeninas para la customizaci�n del avatar en el videojuego
 * Correcci�n sobre la dificultad para apagar algunos incendios en el juego
 * Mejoras en el sistema de pathfinding de los compa�eros del avatar en el videojuego

 1.2
-----

 * Correcci�n incidencia JIRA APPCRUCEMU-20 "Posibilidad de feedback"
 * Correcci�n incidencia JIRA APPCRUCEMU-14. Limite caracteres en login
 * Correcci�n incidencia JIRA APPCRUCEMU-16. Agregado de Captcha en Login
 * Correcci�n incidencia JIRA APPCRUCEMU-21. Se elimino carga externa de Font Awesome.
 * Correcci�n incidencia JIRA APPCRUCEMU-22. Se agrego en menu bot�n ADMIN para Role Admin
 * Correcci�n Vulnerabilidad-0001. Se agrego timeout de sesion en 10 minutos.
 * Correcci�n Vulnerabilidad-0006. Se agrega un control de ROLE_ADMIN al controlador para agregar cursos.
 * Correcci�n Vulnerabilidad-0007. Se agrega validaci�n de los datos a la carga de cursos.
 * Correcci�n Vulnerabilidad-0004. No se muestra el c�digo de error 403

 1.3
-----

 * Correcci�n incidencia JIRA APPCRUCEMU-23. Se agrego capacidad de inhabilitar y eliminar usuarios
 * Correcci�n incidencia JIRA APPCRUCEMU-26.

  1.4
-----

 * Correcci�n incidencia JIRA APPCRUCEMU-23.
 * Correcci�n incidencia JIRA APPCRUCEMU-31.
 * Correcci�n incidencia JIRA APPCRUCEMU-32.

  1.5
-----

 * Correcci�n Vulnerabilidad-0001. Agregado de captcha en formulario de recuperaci�n de contrase�a.
 * Agregado de captura de tiempo de dialogos en VJ y base de datos.

  1.6
-----

 * Cambio en la validaci�n del campo nombre escuela
 * Cambio para que los informes logren identificar distintos nombres de alumnos que utilicen una misma computadora
 * Animaci�n de latido para bot�n de saltear nivel en nivel 5
 * Evitar que pueda saltearse la ayuda enseguida en el nivel 1
 * Evitar que las opciones de di�logos puedan presionarse inmediatamente para fomentar su lectura
 * Las opciones de respuesta a di�logos aparecen en orden aleatorio cada vez

1.7
-----

* Actualización en versiones de librerías con Sithous y FOS
* Se agrega docker y docker-compose para facilitar el proceso de instalación y configuración
* Se agregaron los pasos manuales en la instalación al proceso automatico de deploy usando las herramientas de Symfony para dichos pasos.
* Se editó el Makefile agregando comandos de build, init, clean, log
* Se editó el manual de instalación para reflejar los cambios en la instalación

1.8
-----

* Actualización de versión de dependencias
* Se hicieron cambios en dockerfile y docker-compose para facilitar el proceso de instalación y configuración
* Se editó el Makefile agregando comandos de build y separando la logica de base de datos
* Se editó el manual de instalación para reflejar los cambios en la instalación y agregar los requerimientos para la instalación con un proxy en nginx.
* Se agrego en el manual un detalle mayor en las tareas y opciones de configuración para facilitar el proceso de homologación.

1.9
-----

* Se agrego un servicio de login
* Se ajusto el Video Juego para utilizar dicho proceso de login
* Se agregaron parametros de relevamiento en el juego
