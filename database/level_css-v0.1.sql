#--
#-- Table structure for table `level_css`
#--
DROP TABLE IF EXISTS `level_css`;

CREATE TABLE `level_css` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `css_id` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#--
#-- Dumping data for table `level_css`
#--

INSERT INTO `level_css` (`id`, `level_id`, `css_id`) VALUES
(2, 1, '11_5'),
(6, 1, '12_1'),
(4, 1, '14_8'),
(3, 1, '15_5'),
(5, 1, '5_0'),
(7, 1, '8_1'),
(1, 1, '9_4'),
(9, 2, '0_9'),
(8, 2, '10_5'),
(27, 3, '0_7'),
(28, 3, '0_9'),
(10, 3, '10_3'),
(17, 3, '12_13'),
(18, 3, '14_13'),
(20, 3, '16_15'),
(30, 3, '20_14'),
(24, 3, '20_18'),
(11, 3, '20_3'),
(16, 3, '21_9'),
(29, 3, '22_13'),
(19, 3, '26_13'),
(21, 3, '26_16'),
(23, 3, '26_17'),
(26, 3, '27_19'),
(13, 3, '27_4'),
(22, 3, '29_16'),
(25, 3, '29_18'),
(14, 3, '4_6'),
(15, 3, '7_7'),
(12, 3, '8_4'),
(31, 4, '0_8'),
(33, 4, '13_5'),
(32, 4, '14_4'),
(34, 4, '3_6'),
(42, 5, '0_13'),
(39, 5, '1_7'),
(40, 5, '11_11'),
(35, 5, '11_2'),
(36, 5, '12_4'),
(41, 5, '16_12'),
(37, 5, '16_4'),
(38, 5, '21_6'),
(43, 5, '5_5'),
(47, 6, '10_9'),
(45, 6, '11_2'),
(44, 6, '5_0'),
(46, 6, '9_4'),
(48, 2, '7_9'),
(49, 2, '8_9'),
(50, 2, '0_6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `level_css`
--
ALTER TABLE `level_css`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_id_cs` (`level_id`,`css_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `level_css`
--
ALTER TABLE `level_css`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;