DROP VIEW IF EXISTS `l4_metrics`;

CREATE VIEW l4_metrics AS
SELECT 
	L.user_id,
	L.level_id,
	(SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 0) AS L4_LEVEL_END_0,
  (SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 1) AS L4_LEVEL_END_1,
	(SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 2) AS L4_LEVEL_END_2,
  (SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 3) AS L4_LEVEL_END_3,
	(SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 4) AS L4_LEVEL_END_4,
  (SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 7) AS L4_LEVEL_END_7,
	(SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 8) AS L4_LEVEL_END_8,
  (SELECT COUNT(*) FROM `level_clean` WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 9) AS L4_LEVEL_END_9,

	(SELECT COUNT(timestamp) 
		FROM `level_clean`
		WHERE user_id = L4P.user_id AND level_id = L4P.level_id 
		AND timestamp < L4P.level_resolve_timestamp) AS L4_MORE_TIMES,
	(SELECT COUNT(timestamp) 
		FROM `level_clean` 
		WHERE user_id = L4P.user_id AND level_id = L4P.level_id 
		AND timestamp < L4P.level_resolve_timestamp AND L4P.level_dra_acepted = 1) AS L4_TIMES_DRA,
	(SELECT COUNT(timestamp) 
		FROM `level_clean` 
		WHERE user_id = L4P.user_id AND level_id = L4P.level_id AND level_end != 0
		AND timestamp < L4P.level_resolve_timestamp AND L4P.level_dra_acepted = 0) AS L4_SKIP_TRIES   

FROM `level_clean` L,
        (SELECT  
            user_id, 
            level_id, 
            SUBSTRING_INDEX(GROUP_CONCAT(id,',',level_end,',',timestamp ORDER BY timestamp DESC SEPARATOR ';'),',', 1) AS level_resolve_id,
            SUBSTRING_INDEX(GROUP_CONCAT(timestamp,',',level_end,',',id ORDER BY timestamp DESC SEPARATOR ';'),',', 1) AS level_resolve_timestamp,
            SUBSTRING_INDEX(GROUP_CONCAT(level_end,',',timestamp,',',id ORDER BY timestamp DESC SEPARATOR ';'),',', 1) AS level_resolve_end,
            CAST(CONCAT('["',REPLACE(SUBSTRING_INDEX(GROUP_CONCAT(map_trail,'|',timestamp ORDER BY timestamp ASC SEPARATOR '\/'),'|',1), ';', '","'),'"]' ) AS JSON) AS level_resolve_trail,
            (SELECT COUNT(*) FROM level_clean WHERE L.user_id = user_id AND L.level_id = level_id AND level_end = 0 AND JSON_LENGTH(CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON)) < 15) AS level_dra_acepted 
        FROM `level_clean` L
        WHERE level_end = 4 OR level_end = 0
        GROUP BY user_id, level_id
        HAVING level_id = 4) L4P
WHERE L.level_id = 4 AND L.user_id = L4P.user_id AND L4P.level_id = L.level_id
GROUP BY L.user_id,L.level_id
ORDER BY L.user_id;


/* SEARCH SUBQUERY */
SELECT 
	user_id, 
    level_id, 
    SUBSTRING_INDEX(GROUP_CONCAT(id,',',level_end,',',timestamp ORDER BY timestamp ASC SEPARATOR ';'),',', 1) AS level_resolve_id,
    SUBSTRING_INDEX(GROUP_CONCAT(timestamp,',',level_end,',',id ORDER BY timestamp ASC SEPARATOR ';'),',', 1) AS level_resolve_timestamp,
    SUBSTRING_INDEX(GROUP_CONCAT(level_end,',',timestamp,',',id ORDER BY timestamp ASC SEPARATOR ';'),',', 1) AS level_resolve_end,
    CAST(CONCAT('["',REPLACE(SUBSTRING_INDEX(GROUP_CONCAT(map_trail,'|',timestamp ORDER BY timestamp ASC SEPARATOR '\/'),'|',1), ';', '","'),'"]' ) AS JSON) AS level_resolve_trail
FROM `level_clean`
WHERE 
	(level_end = 4 AND JSON_LENGTH(CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON)) < 3)
    OR 
    (level_end = 0 AND JSON_LENGTH(CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON)) > 1)
GROUP BY user_id, level_id
HAVING level_id = 4
ORDER by user_id