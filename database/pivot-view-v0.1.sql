DROP VIEW IF EXISTS `level_metrics`;

SET group_concat_max_len = 65536;
SET @sql = NULL;

SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
			#TIMESTAMP
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN game_id END) AS ',
       CONCAT('GAMEID_L',level_id,','),			
			#TIMESTAMP
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_timestamp END) AS ',
       CONCAT('TIMESTAMP_L',level_id,','),
			#RESTAURADOR
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN Restaurador END) AS ',
       CONCAT('RESTAURADOR_L',level_id,','),
			#MATAFUEGOS
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN Matafuegos END) AS ',
       CONCAT('MATAFUEGOS_L',level_id,','),
			#ARMONIZADOR
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN Armonizador END) AS ',
       CONCAT('ARMONIZADOR_L',level_id,','),			 			 
			#MISSIONS
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN missions END) AS ',
       CONCAT('RESULTADO_L',level_id,','),
			#MAP_CHECKS
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN map_checks END) AS ',
       CONCAT('MAP_CHECKS_L',level_id,','),
			#LEVEL_TIME 
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_time END) AS ',
       CONCAT('LEVEL_TIME_L',level_id,','),
			#GAME_TIME
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN game_time END) AS ',
       CONCAT('GAME_TIME_L',level_id,','),
			#TIME_DIFF
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN time_diff END) AS ',
       CONCAT('TIME_DIFF_L',level_id,','),			 
			#FIRST_MAP_TIME
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN first_map_time END) AS ',
       CONCAT('FIRST_MAP_TIME_L',level_id,','),
			#MISSION_TIME
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN mission_time END) AS ',
       CONCAT('MISSION_TIME_L',level_id,','),
			#TOOLS_TIME
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN tools_time END) AS ',
       CONCAT('TOOLS_TIME_L',level_id,','),
			#LEVEL_REPLAY
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_replay END) AS ',
       CONCAT('REPLAY_LEVEL_TIMES_L',level_id,','),
			#LEVEL_PREV
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_prev END) AS ',
       CONCAT('PREVIOUS_LEVEL_TIMES_L',level_id,','),
			#LEVEL_POST
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_post END) AS ',
       CONCAT('POST_LEVEL_TIMES_L',level_id,','),
			#LEVEL_BETTER
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_better END) AS ',
       CONCAT('BETTER_LEVEL_TIMES_L',level_id,','),			 	 			 
			#MAP_TRAIL 
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN map_trail END) AS ',
       CONCAT('MAP_TRAIL_L',level_id,','),
			#RT_BEGIN
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN rt_begin END) AS ',
       CONCAT('RT_BEGIN_L',level_id,','),
			#RT_AFTER_TOOLS
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN rt_after_tools END) AS ',
       CONCAT('RT_AFTER_TOOLS_L',level_id,','),
			#RT_END
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN rt_end END) AS ',
       CONCAT('RT_END_L',level_id,','),
			#LEVEL_END 
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_end END) AS ',
       CONCAT('FIN_NIVEL_L',level_id,','),
			#PORTAL_DONE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN portal_done END) AS ',
       CONCAT('PORTAL_DONE_L',level_id,','),
			#FIRE_DONE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN fire_done END) AS ',
       CONCAT('FIRE_DONE_L',level_id,','),
			#POLLUTION_DONE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN pollution_done END) AS ',
       CONCAT('POLLUTION_DONE_L',level_id,','),
			#PORTAL_CHARGE 
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN portal_charge END) AS ',
       CONCAT('PORTAL_CHARGE_L',level_id,','),
			#FIRE_CHARGE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN fire_charge END) AS ',
       CONCAT('FIRE_CHARGE_L',level_id,','),
			#POLLUTION_CHARGE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN pollution_charge END) AS ',
       CONCAT('POLLUTION_CHARGE_L',level_id,','),
			#RT_CHARGE 
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN rt_charge END) AS ',
       CONCAT('RT_CHARGE_L',level_id)

    ) 
  ) INTO @sql
FROM best_game;

SET @sql = CONCAT(
	'CREATE VIEW level_metrics AS '
	'SELECT user_id AS USER_ID,create_time AS CREATE_TIME, ', @sql, '
   FROM best_game 
   GROUP BY user_id,create_time'
);

PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*
			#TOOLS_END_CHARGE
      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN tools_end_charge END) AS ',
       CONCAT('TOOLS_END_CHARGE_L',level_id,','),