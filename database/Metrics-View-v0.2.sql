#--
#-- Structure for view `best_game`
#--
DROP VIEW IF EXISTS `best_game`;

CREATE VIEW best_game AS 
SELECT 
		LVL.id,
		LVL.user_id AS user_id,
		U.create_time,
    LVL.computer_id,
    LVL.game_id,
    LVL.level_id,
		(SELECT count(*) FROM level WHERE LVL.user_id = user_id AND LVL.level_id = level_id) AS level_replay,
		(SELECT count(*) FROM level WHERE LVL.timestamp > timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id) AS level_prev,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id) AS level_post,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id AND LVL.missions < ROUND ( (LENGTH(missions) - LENGTH( REPLACE ( missions, 'True', '' ) ) ) / LENGTH( 'True' )) ) AS level_better,
		#Agregar los mismos 4 campos para = Game_ID
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_0', '')) ) / LENGTH('Restaurador_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_1', '')) ) / LENGTH('Restaurador_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_2', '')) ) / LENGTH('Restaurador_2'),0) * 100 AS `Restaurador`,
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_0', '')) ) / LENGTH('Matafuegos_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_1', '')) ) / LENGTH('Matafuegos_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_2', '')) ) / LENGTH('Matafuegos_2'),0) * 100 AS `Matafuegos`,
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_0', '')) ) / LENGTH('Armonizador_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_1', '')) ) / LENGTH('Armonizador_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_2', '')) ) / LENGTH('Armonizador_2'),0) * 100 AS `Armonizador`,
		ROUND ( (LENGTH(LVL.missions) - LENGTH( REPLACE ( LVL.missions, 'True', '' ) ) ) / LENGTH( 'True' )) AS missions,
    LVL.map_checks, 
    LVL.level_time, 
    LVL.game_time,
    LVL.first_map_time,
    LVL.mission_time,
    LVL.tools_time,
    CAST(CONCAT('["',REPLACE(LVL.map_trail, ';', '","'),'"]' ) AS JSON) AS map_trail,
		CAST(CONCAT("[",SUBSTRING(REPLACE(map_dead_ends, ";", ","), 1, LENGTH(map_dead_ends) -1),"]") AS JSON) AS map_dead_ends,
    LVL.rt_begin,
    LVL.rt_after_tools,
    LVL.rt_end,
    LVL.timestamp AS level_timestamp,
    LVL.level_end,
    LVL.portal_done,
    LVL.fire_done,
    LVL.pollution_done,
    LVL.portal_charge,
    LVL.fire_charge,
    LVL.pollution_charge,
    LVL.rt_charge
FROM `level` LVL
INNER JOIN
	(	SELECT LWT.user_id, LWT.level_id, GROUP_CONCAT( LWT.id, ' ', LWT.timestamp ORDER BY LWT.timestamp ) time_list
		FROM (SELECT 
				id,
				user_id, 
				level_id,
				CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON) AS trail,
				timestamp,
				level_end
			FROM `level`
			WHERE JSON_LENGTH(CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON)) > 2) AS LWT
		WHERE LWT.level_end = 0 AND LWT.level_id > 0 AND LWT.level_id < 8 
		GROUP BY LWT.user_id, LWT.level_id ) AS L
	ON LVL.user_id = L.user_id 
	AND LVL.id = SUBSTRING_INDEX(SUBSTRING_INDEX(L.time_list, ',', 1), ' ', 1)
INNER JOIN  `users` U ON LVL.user_id = U.user_id
ORDER BY LVL.user_id ASC, LVL.level_id ASC, U.create_time DESC;

#--
#-- Indexes for table `dialog_replies`
#--
ALTER TABLE `dialog_replies`
  ADD KEY `GAME_ID` (`user_id`,`computer_id`,`game_id`,`level_id`,`dialog_id`),
  ADD KEY `USER_ID` (`user_id`),
  ADD KEY `DIALOG_ID` (`level_id`,`character_name`,`dialog_mood`,`dialog_index`,`answer_id`);

#--
#-- Indexes for table `dialogs`
#--
ALTER TABLE `dialogs`
  ADD KEY `DIALOG_ID` (`level_id`,`character_name`,`dialog_type`,`dialog_mood`,`dialog_index`);

#--
#-- Indexes for table `level`
#--
ALTER TABLE `level`
  ADD KEY `GAME_ID` (`user_id`,`computer_id`,`level_id`,`game_id`),
  ADD KEY `USER_ID` (`user_id`),
  ADD KEY `LEVEL_ID` (`user_id`,`level_id`);
ALTER TABLE `level_clean`	
	ADD KEY `LEVEL_END_ID` (`user_id`,`level_id`,`level_end`),
	ADD KEY `LEVEL_MAP_ID` (`user_id`,`level_id`,`map_trail`);

#--
#-- Indexes for table `users`
#--
ALTER TABLE `users`
  ADD KEY `USER_ID` (`user_id`),
  ADD KEY `CREATION` (`user_id`,`create_time`);

#--
#-- Level Best (copy of level table) cleaned to use valid games
#--
DROP TABLE IF EXISTS `level_best`;
INSERT INTO level_best
    (SELECT *
    FROM `level_clean` L
    WHERE (L.user_id, L.level_id, L.id) IN
    (SELECT LWT.user_id, LWT.level_id, SUBSTRING_INDEX(GROUP_CONCAT( LWT.id, ' ', LWT.timestamp ORDER BY LWT.timestamp ), ' ',1) AS level_time_id
    FROM (SELECT
                 id,
                 user_id,
                 level_id,
                 CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON) AS trail,
                 timestamp,
                 level_end
                 FROM `level_clean`
                 WHERE JSON_LENGTH(CAST(CONCAT('["',REPLACE(`map_trail`, ';', '","'),'"]' ) AS JSON)) > 2) AS LWT
    WHERE LWT.level_end = 0 AND LWT.level_id > 0 AND LWT.level_id < 8
    GROUP BY LWT.user_id, LWT.level_id));

#--
#-- Best Games from Level Best
#--
DROP VIEW IF EXISTS `best_game`;
CREATE VIEW best_game AS 
SELECT 
		LVL.id,
		LVL.user_id AS user_id,
		U.create_time,
    LVL.computer_id,
    LVL.game_id,
    LVL.level_id,
		(SELECT count(*) FROM level WHERE LVL.user_id = user_id AND LVL.level_id = level_id) AS level_replay,
		(SELECT count(*) FROM level WHERE LVL.timestamp > timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id) AS level_prev,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id) AS level_post,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.level_id = level_id AND LVL.missions < ROUND ( (LENGTH(missions) - LENGTH( REPLACE ( missions, 'True', '' ) ) ) / LENGTH( 'True' )) ) AS level_better,
		(SELECT count(*) FROM level WHERE LVL.timestamp > timestamp AND LVL.user_id = user_id AND LVL.game_id = game_id AND LVL.level_id = level_id) AS level_samegame_prev,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.game_id = game_id AND LVL.level_id = level_id) AS level_samegame_post,
		(SELECT count(*) FROM level WHERE LVL.timestamp < timestamp AND LVL.user_id = user_id AND LVL.game_id = game_id AND LVL.level_id = level_id AND LVL.missions < ROUND ( (LENGTH(missions) - LENGTH( REPLACE ( missions, 'True', '' ) ) ) / LENGTH( 'True' )) ) AS level_samegame_better,	
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_0', '')) ) / LENGTH('Restaurador_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_1', '')) ) / LENGTH('Restaurador_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Restaurador_2', '')) ) / LENGTH('Restaurador_2'),0) * 100 AS `Restaurador`,
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_0', '')) ) / LENGTH('Matafuegos_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_1', '')) ) / LENGTH('Matafuegos_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Matafuegos_2', '')) ) / LENGTH('Matafuegos_2'),0) * 100 AS `Matafuegos`,
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_0', '')) ) / LENGTH('Armonizador_0'),0) * 50  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_1', '')) ) / LENGTH('Armonizador_1'),0) * 75  +
TRUNCATE((LENGTH(LVL.tools_selected) - LENGTH(REPLACE(LVL.tools_selected, 'Armonizador_2', '')) ) / LENGTH('Armonizador_2'),0) * 100 AS `Armonizador`,
		ROUND ( (LENGTH(LVL.missions) - LENGTH( REPLACE ( LVL.missions, 'True', '' ) ) ) / LENGTH( 'True' )) AS missions,
    LVL.map_checks, 
    LVL.level_time, 
    LVL.game_time,
	( TIME_TO_SEC(TIMEDIFF( (SELECT LVL0.timestamp FROM `level_best` LVL0 WHERE LVL.id = LVL0.id),(SELECT LVL1.timestamp FROM `level_best` LVL1 WHERE LVL1.level_id = (LVL.level_id - 1) AND LVL.user_id = LVL1.user_id) )) - (SELECT (LVL2.tools_time + LVL2.mission_time + LVL2.first_map_time) FROM `level_best` LVL2 WHERE LVL.id = LVL2.id) ) as time_diff,				
    LVL.first_map_time,
    LVL.mission_time,
    LVL.tools_time,
    CAST(CONCAT('["',REPLACE(LVL.map_trail, ';', '","'),'"]' ) AS JSON) AS map_trail,
		CAST(CONCAT("[",SUBSTRING(REPLACE(map_dead_ends, ";", ","), 1, LENGTH(map_dead_ends) -1),"]") AS JSON) AS map_dead_ends,
    LVL.rt_begin,
    LVL.rt_after_tools,
    LVL.rt_end,
    LVL.timestamp AS level_timestamp,
    LVL.level_end,
    LVL.portal_done,
    LVL.fire_done,
    LVL.pollution_done,
    LVL.portal_charge,
    LVL.fire_charge,
    LVL.pollution_charge,
    LVL.rt_charge
FROM `level_best` LVL
INNER JOIN  `users` U ON LVL.user_id = U.user_id
ORDER BY LVL.user_id ASC, LVL.level_id ASC, U.create_time DESC;

#--
#-- Level Clean (copy of level table) cleaned to use valid games
#--
DROP TABLE IF EXISTS `level_clean`;
CREATE TABLE `level_clean` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `computer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
	`game_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tools_selected` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `missions` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `map_checks` int(11) NOT NULL,
  `level_time` double NOT NULL,
  `game_time` double NOT NULL,
	`phone_map_time` double NOT NULL,
  `first_map_time` double NOT NULL,
	`tools_map_time` double NOT NULL,
  `mission_time` double NOT NULL,
  `tools_time` double NOT NULL,
  `map_trail` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `rt_begin` int(11) NOT NULL,
  `rt_after_tools` int(11) NOT NULL,
  `rt_end` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `level_end` int(11) NOT NULL,
  `portal_done` int(11) NOT NULL,
  `fire_done` int(11) NOT NULL,
  `pollution_done` int(11) NOT NULL,
  `map_dead_ends` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `tools_end_charge` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `portal_charge` int(11) NOT NULL,
  `fire_charge` int(11) NOT NULL,
  `pollution_charge` int(11) NOT NULL,
  `rt_charge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#--
#-- Indexes for table `level_clean`
#--
ALTER TABLE `level_clean`
  ADD PRIMARY KEY (`id`);
#--
#-- AUTO_INCREMENT for table `level_clean`
#--
ALTER TABLE `level_clean`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO level_clean
(SELECT DISTINCT LevelA.`id`,LevelA.`user_id`,LevelA.`computer_id`,LevelA.`level_id`,LevelA.`game_id`,LevelA.`tools_selected`, LevelA.`missions`, LevelA.`map_checks`, LevelA.`level_time`, LevelA.`game_time`, LevelA.`phone_map_time`, LevelA.`first_map_time`, LevelA.`tools_map_time`, LevelA.`mission_time`, LevelA.`tools_time`, LevelA.`map_trail`, LevelA.`rt_begin`, LevelA.`rt_after_tools`, LevelA.`rt_end`, LevelA.`timestamp`, LevelA.`level_end`, LevelA.`portal_done`, LevelA.`fire_done`, LevelA.`pollution_done`, LevelA.`map_dead_ends`, LevelA.`tools_end_charge`, LevelA.`portal_charge`, LevelA.`fire_charge`, LevelA.`pollution_charge`,LevelA.`rt_charge`
FROM `level` LevelA
INNER JOIN (
			SELECT `user_id`,`level_id`,GROUP_CONCAT(`id` ORDER BY `timestamp`) AS repeated, COUNT(`timestamp`) AS time_count
			FROM `level`
			GROUP BY
					`user_id`,
					`computer_id`,
					`game_id`,
					`level_id`,
					`level_end`,
					`map_checks`,
					`first_map_time`,
					`tools_map_time`,
					`phone_map_time`,
					`mission_time`) RepeatedLevelA
  ON LevelA.user_id = RepeatedLevelA.user_id 
 	AND LevelA.level_id = RepeatedLevelA.level_id 
WHERE (RepeatedLevelA.time_count = 1 AND FIND_IN_SET(LevelA.`id`,RepeatedLevelA.repeated) > 0)
)
UNION
(SELECT DISTINCT LevelB.`id`,LevelB.`user_id`,LevelB.`computer_id`,LevelB.`level_id`,LevelB.`game_id`,LevelB.`tools_selected`,LevelB.`missions`,LevelB.`map_checks`,LevelB.`level_time`,LevelB.`game_time`,LevelB.`phone_map_time`,LevelB.`first_map_time`,LevelB.`tools_map_time`,LevelB.`mission_time`,LevelB.`tools_time`,LevelB.`map_trail`,LevelB.`rt_begin`,LevelB.`rt_after_tools`,LevelB.`rt_end`,LevelB.`timestamp`,LevelB.`level_end`,LevelB.`portal_done`,LevelB.`fire_done`,LevelB.`pollution_done`,LevelB.`map_dead_ends`,LevelB.`tools_end_charge`,LevelB.`portal_charge`,LevelB.`fire_charge`,LevelB.`pollution_charge`,LevelB.`rt_charge`
FROM `level` LevelB
INNER JOIN (
        SELECT `user_id`,`level_id`,SUBSTRING_INDEX(GROUP_CONCAT(`id` ORDER BY `timestamp`), ",", 1) AS first_id
        FROM `level`
        GROUP BY
            `user_id`,
            `computer_id`,
            `game_id`,
            `level_id`,
            `level_end`,
            `map_checks`,
            `first_map_time`,
            `tools_map_time`,
            `phone_map_time`,
            `mission_time`
        HAVING COUNT(`timestamp`) > 1) RepeatedLevelB
ON LevelB.`id` = RepeatedLevelB.first_id)


/*
SELECT `user_id`,`level_id`,GROUP_CONCAT(`id`,"|",`timestamp` ORDER BY `timestamp`) AS clean_id,COUNT(id)
FROM `level`
GROUP BY 
	`user_id`,
	`computer_id`,
	`game_id`,
	`level_id`,
	`level_end`,
	`map_checks`,
	`first_map_time`,
	`tools_map_time`,
	`phone_map_time`,
	`mission_time`
HAVING COUNT(`timestamp`) >= 2
ORDER BY `user_id`
*/