##--DIALOGS QUERY GENERATION
DROP VIEW IF EXISTS `dialogs_metrics`;

SET group_concat_max_len = 32768;
SET @sql = NULL;
SELECT GROUP_CONCAT( 
	DISTINCT CONCAT(
		'MAX(CASE WHEN DC.dialog_columns = ''', DC0.dialog_columns, ''' THEN DR.dialog_id END) AS ', 
		REPLACE( CONCAT('L_',level_id,'_',character_name,'_',dialog_mood,'_',dialog_index,'_ID,'), ' ', ''),		
		'MAX(CASE WHEN DC.dialog_columns = ''', DC0.dialog_columns, ''' THEN DR.answer_id END) AS ', 
		REPLACE( CONCAT('L_',level_id,'_',character_name,'_',dialog_mood,'_',dialog_index,'_ANSWER_ID,'), ' ', ''),
		'MAX(CASE WHEN DC.dialog_columns = ''', DC0.dialog_columns, ''' THEN DR.dialog_time END) AS ', 
		REPLACE( CONCAT('L_',level_id,'_',character_name,'_',dialog_mood,'_',dialog_index,'_TIME,'), ' ', ''),
		'MAX(CASE WHEN DC.dialog_columns = ''', DC0.dialog_columns, ''' THEN DR.timestamp END) AS ', 
		REPLACE( CONCAT('L_',level_id,'_',character_name,'_',dialog_mood,'_',dialog_index,'_TIMESTAMP'), ' ', '')		
	)) INTO @sql
FROM dialogs
INNER JOIN (
		SELECT DISTINCT
			CONCAT('L_',D0.`level_id`,D0.`character_name`,D0.`dialog_type`,D0.`dialog_mood`,D0.`dialog_index`) AS 'dialog_columns'
			FROM `dialogs` D0
			WHERE valor_indicador <> '' OR character_name = 'Dra Grimberg' OR (character_name = 'Mark' AND level_id = 1)
			ORDER BY dialog_columns) DC0
	ON CONCAT('L_',`level_id`,`character_name`,`dialog_type`,`dialog_mood`,`dialog_index`) = DC0.dialog_columns    
ORDER BY level_id ASC,character_name ASC;

SET @sql = CONCAT(
		'CREATE VIEW dialogs_metrics AS ',
		'SELECT DR.user_id,', @sql, '
		FROM `dialog_replies` DR
		INNER JOIN `dialogs` D ON D.id = DR.dialog_id
		INNER JOIN (
				SELECT	
					DR0.user_id,
					DR0.level_id,
					DR0.dialog_id,
					SUBSTRING_INDEX(GROUP_CONCAT(DR0.id, " ", DR0.game_id, DR0.timestamp ORDER BY DR0.game_id,DR0.timestamp), " " , 1) AS first_awnser
				FROM dialog_replies DR0
				GROUP BY user_id, level_id, dialog_id) FD
			ON FD.first_awnser = DR.id
		INNER JOIN (
				SELECT DISTINCT
					CONCAT("L_",D1.level_id,D1.character_name,D1.dialog_type,D1.dialog_mood,D1.dialog_index) AS dialog_columns
				FROM `dialogs` D1
				WHERE D1.valor_indicador <> "" OR D1.character_name = "Dra Grimberg" OR (D1.character_name = "Mark" AND D1.level_id = 1) ) DC
			ON CONCAT("L_",D.level_id,D.character_name,D.dialog_type,D.dialog_mood,D.dialog_index) = DC.dialog_columns	
		GROUP BY DR.user_id
		ORDER BY DR.user_id;
	 '
);
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*
Opcion join con best_game
		INNER JOIN `best_game` BG 
			ON DR.user_id = BG.user_id 
			AND DR.level_id = BG.level_id 
			AND DR.game_id = BG.game_id 
			AND DR.timestamp <= BG.level_timestamp
Opcion menor timestamp
SELECT	user_id,
		level_id,
        dialog_id,
        SUBSTRING_INDEX(GROUP_CONCAT(id, ' ', timestamp ORDER BY timestamp), ' ', 1) AS first_awnser
FROM dialog_replies
GROUP BY user_id, level_id, dialog_id
Opcion menor id
SELECT	user_id,
		level_id,
        dialog_id,
        MIN(id)
FROM dialog_replies
GROUP BY user_id, level_id, dialog_id