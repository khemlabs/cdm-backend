/****** TEST QUERYS ***********

select * from cdm.users;
select * from cdm.level;
select * from cdm.dialogs;
select * from cdm.dialog_replies;

select 
	*,
	D.id
from cdm.dialog_replies DR
left join cdm.dialogs on DR.character_name = D.character_name 
		AND DR.level_id = D.level_id 
        AND DR.dialog_index = D.dialog_index 
        AND DR.dialog_mood = D.dialog_mood
        AND DR.answer_id = D.answer_id ;
--	(
--     select id 
--     from cdm.dialogs D 
--     where
--			DR.character_name = D.character_name 
--		AND DR.level_id = D.level_id 
--        AND DR.dialog_index = D.dialog_index 
--        AND DR.dialog_mood = D.dialog_mood
--        AND DR.answer_id = D.answer_id
--	) as dialog_id 


     select * 
     from 
		cdm.dialogs D, 
        cdm.dialog_replies DR
     where 
			DR.character_name = D.character_name 
		AND DR.level_id = D.level_id 
        AND DR.dialog_index = D.dialog_index;    
        
select 
	cdm.level.user_id,
    cdm.users.user_name,
    cdm.level.game_id, 
    cdm.users.create_time, 
    cdm.level.level_id, 
    cdm.level.tools_selected, 
    cdm.level.missions, 
    cdm.level.map_checks, 
    cdm.level.level_time, 
    cdm.level.game_time,
    cdm.level.first_map_time,
    cdm.level.mission_time,
    cdm.level.tools_time,
    cdm.level.map_trail,
    cdm.level.rt_begin,
    cdm.level.rt_after_tools,
    cdm.level.rt_end,
    cdm.level.timestamp,
    cdm.level.level_end,
    cdm.level.portal_done,
    cdm.level.fire_done,
    cdm.level.pollution_done,
    cdm.level.map_dead_ends,
    cdm.level.tools_end_charge,
    cdm.level.portal_charge,
    cdm.level.fire_charge,
    cdm.level.pollution_charge,
    cdm.level.rt_charge,
    cdm.dialog_replies.timestamp as DialogTimeStamp,
    cdm.dialog_replies.answer_id,
    cdm.dialog_replies.dialog_time
from  
	cdm.level, 
    cdm.users,
    cdm.dialog_replies
where
	cdm.level.user_id = cdm.users.user_id
	and
    cdm.level.computer_id = cdm.users.computer_id
    and
    cdm.level.game_id = cdm.dialog_replies.game_id
    and
    cdm.users.computer_id = cdm.dialog_replies.computer_id
    and
    cdm.level.level_id = cdm.dialog_replies.level_id;
-- Consulta de Respuestas de dialogos
select
	U.user_id, U.computer_id, 
    
    DR.game_id,
    DR.level_id,
    
	DR.timestamp as DialogTimeStamp,
    DR.answer_id,
    DR.dialog_time
from 
	cdm.dialog_replies DR
left join cdm.users U on DR.user_id = U.user_id AND DR.computer_id = U.computer_id
order by DR.user_id, DR.computer_id, DR.game_id, DR.level_id, DR.timestamp ASC;


--- Consulta de niveles jugados
select
	U.user_id, U.computer_id, 
    L.game_id,
    L.level_id,
 
    L.tools_selected, 
    L.missions, 
    L.map_checks, 
    L.level_time, 
    L.game_time,
    L.first_map_time,
    L.mission_time,
    L.tools_time,
    L.map_trail,
    L.rt_begin,
    L.rt_after_tools,
    L.rt_end,
    L.timestamp,
    L.level_end,
    L.portal_done,
    L.fire_done,
    L.pollution_done,
    L.map_dead_ends,
    L.tools_end_charge,
    L.portal_charge,
    L.fire_charge,
    L.pollution_charge,
    L.rt_charge
    
from 
	cdm.level L
left join cdm.users U on L.user_id = U.user_id AND L.computer_id = U.computer_id
order by L.user_id, L.computer_id, L.game_id, L.level_id, L.timestamp ASC;



select
	U.user_id, U.computer_id, 
    
    L.game_id,
    L.level_id,
    
    L.tools_selected, 
    L.missions, 
    L.map_checks, 
    L.level_time, 
    L.game_time,
    L.first_map_time,
    L.mission_time,
    L.tools_time,
    L.map_trail,
    L.rt_begin,
    L.rt_after_tools,
    L.rt_end,
    L.timestamp,
    L.level_end,
    L.portal_done,
    L.fire_done,
    L.pollution_done,
    L.map_dead_ends,
    L.tools_end_charge,
    L.portal_charge,
    L.fire_charge,
    L.pollution_charge,
    L.rt_charge,
   
	DR.timestamp as DialogTimeStamp,
    DR.answer_id,
    DR.dialog_time,
    (
     select id 
     from cdm.dialogs D 
     where DR.character_name = D.character_name 
		AND DR.level_id = D.level_id 
        AND DR.dialog_index = D.dialog_index 
        AND DR.dialog_mood = D.dialog_mood
        AND DR.answer_id = D.answer_id
	) as dialog_id      
    
from 
	cdm.level L
left join cdm.users U on L.user_id = U.user_id AND L.computer_id = U.computer_id
left join cdm.dialog_replies DR on L.user_id = DR.user_id AND L.computer_id = DR.computer_id AND L.game_id = DR.game_id
order by L.user_id, L.computer_id, L.game_id, L.level_id asc;

select 
	*,
	DR.timestamp as dialog_timestamp,
    DR.answer_id,
    DR.dialog_time,
    (
     select id 
     from cdm.dialogs D 
     where DR.character_name = D.character_name 
		AND DR.level_id = D.level_id 
        AND DR.dialog_index = D.dialog_index 
        AND DR.dialog_mood = D.dialog_mood
        AND DR.answer_id = D.answer_id
	) as dialog_id    
from
(select 
	L.user_id,
    L.computer_id,
    L.game_id,
    L.level_id,
    (select count(distinct game_id) from cdm.level P where P.level_id = L.level_id group by level_id) as game_parts,
    (select count(*) from ccd /dm.level C where C.level_id = L.level_id group by level_id) as level_times,
    L.tools_selected, 
    L.missions, 
    L.map_checks, 
    L.level_time, 
    L.game_time,
    L.first_map_time,
    L.mission_time,
    L.tools_time,
    L.map_trail,
    L.rt_begin,
    L.rt_after_tools,
    L.rt_end,
    L.timestamp as level_timesamp,
    L.level_end,
    L.portal_done,
    L.fire_done,
    L.pollution_done,
    L.map_dead_ends,
    L.tools_end_charge,
    L.portal_charge,
    L.fire_charge,
    L.pollution_charge,
    L.rt_charge
from cdm.level L
where 
	(L.level_id, L.timestamp) in (select level_id, min(timestamp) from cdm.level group by level_id) ) LC
left join cdm.dialog_replies DR on LC.user_id = DR.user_id AND LC.computer_id = DR.computer_id AND LC.game_id = DR.game_id;

select *
from 
	(select level_id, min(timestamp), count(*) as times from cdm.level group by level_id) C
join cdm.level L on C.level_id = L.level_id;


XCIBKZ

create view metrics as 
select UUID() as id,
		LC.user_id,
		U.create_time,
    LC.computer_id,
    LC.game_id,
    LC.level_id,
    LC.game_parts,
    LC.level_times,
    LC.tools_selected, 
    LC.missions, 
    LC.map_checks, 
    LC.level_time, 
    LC.game_time,
    LC.first_map_time,
    LC.mission_time,
    LC.tools_time,
    LC.map_trail,
    LC.rt_begin,
    LC.rt_after_tools,
    LC.rt_end,
    LC.LevelTimesamp as level_timestamp,
    LC.level_end,
    LC.portal_done,
    LC.fire_done,
    LC.pollution_done,
    LC.map_dead_ends,
    LC.tools_end_charge,
    LC.portal_charge,
    LC.fire_charge,
    LC.pollution_charge,
    LC.rt_charge,
    DR.dialog_id,
    DR.dialog_index,
		DR.timestamp as dialog_timestamp,
    DR.answer_id,
    DR.dialog_time
from
(select 
	L.user_id,
    L.computer_id,
    L.game_id,
    L.level_id,
    (select count(distinct game_id) from cdm.level P where P.user_id = L.user_id) as game_parts,
    (select count(*) from cdm.level C where C.level_id = L.level_id AND C.user_id = L.user_id group by C.level_id) as level_times,
    L.tools_selected, 
    L.missions, 
    L.map_checks, 
    L.level_time, 
    L.game_time,
    L.first_map_time,
    L.mission_time,
    L.tools_time,
    L.map_trail,
    L.rt_begin,
    L.rt_after_tools,
    L.rt_end,
    L.timestamp as LevelTimesamp,
    L.level_end,
    L.portal_done,
    L.fire_done,
    L.pollution_done,
    L.map_dead_ends,
    L.tools_end_charge,
    L.portal_charge,
    L.fire_charge,
    L.pollution_charge,
    L.rt_charge
from cdm.level L
where 
	(L.level_id, L.timestamp) in (select level_id, min(timestamp) from cdm.level group by user_id, level_id)
) LC, cdm.dialog_replies DR, cdm.users U
where LC.user_id = DR.user_id AND LC.computer_id = DR.computer_id AND LC.game_id = DR.game_id AND LC.level_id = DR.level_id AND LC.user_id = U.user_id;


SELECT UUID() AS id,
		LC.user_id,
		U.create_time,
    LC.computer_id,
    LC.game_id,
    LC.level_id,
    LC.games_played,
    LC.level_times,
    LC.tools_selected, 
    LC.missions, 
    LC.map_checks, 
    LC.level_time, 
    LC.game_time,
    LC.first_map_time,
    LC.mission_time,
    LC.tools_time,
    LC.map_trail,
    LC.rt_begin,
    LC.rt_after_tools,
    LC.rt_end,
    LC.timestamp AS level_timestamp,
    LC.level_end,
    LC.portal_done,
    LC.fire_done,
    LC.pollution_done,
    LC.map_dead_ends,
    LC.tools_end_charge,
    LC.portal_charge,
    LC.fire_charge,
    LC.pollution_charge,
    LC.rt_charge,
    DR.dialog_id,
		DR.character_name,
		DR.dialog_mood,
		DR.dialog_type,
    DR.dialog_index,
		DR.timestamp AS dialog_timestamp,
    DR.answer_id,
    DR.dialog_time
FROM
	(SELECT *,
		(SELECT count(DISTINCT game_id) FROM cdm.level P WHERE P.user_id = L.user_id) AS games_played,
		(SELECT count(*) FROM cdm.level C WHERE C.level_id = L.level_id AND C.user_id = L.user_id GROUP BY C.level_id) AS level_times
	 FROM cdm.level L
	 WHERE	(L.user_id, L.level_id, L.timestamp) IN 
		(SELECT LVL.user_id,LVL.level_id, min(LVL.timestamp) 
		 FROM 
			(SELECT user_id,level_id,timestamp,level_end 
			FROM cdm.level 
			WHERE 
				level_end = 0 
				AND level_id > 1 AND level_id < 7 AND level_id != 4 
				AND LENGTH(map_trail)>6
			AS LVL
		 GROUP BY LVL.user_id, LVL.level_id)
	) LC, (SELECT 
					Diags.id as dialog_id,
					Diags.dialog_type,
					Diags.character_name,
					Diags.dialog_mood,
					DiaRep.dialog_index, 
					DiaRep.user_id,
					DiaRep.computer_id,
					DiaRep.game_id,
					DiaRep.level_id,
					DiaRep.timestamp,
					DiaRep.answer_id,
					DiaRep.dialog_time
				FROM cdm.dialog_replies DiaRep 
				JOIN cdm.dialogs Diags 
					ON DiaRep.character_name = Diags.character_name 
					AND DiaRep.dialog_index = Diags.dialog_index 
					AND DiaRep.dialog_mood = Diags.dialog_mood 
					AND DiaRep.answer_id = Diags.answer_id 
					AND DiaRep.level_id = Diags.level_id ) DR, cdm.users U
WHERE LC.user_id = DR.user_id AND LC.computer_id = DR.computer_id AND LC.game_id = DR.game_id AND LC.level_id = DR.level_id AND LC.user_id = U.user_id;



SELECT UUID() AS id,
		LC.user_id,
		U.create_time,
    LC.computer_id,
    LC.game_id,
    LC.level_id,
    LC.games_played,
    LC.level_times,
    LC.tools_selected, 
    LC.missions, 
    LC.map_checks, 
    LC.level_time, 
    LC.game_time,
    LC.first_map_time,
    LC.mission_time,
    LC.tools_time,
    LC.map_trail,
    LC.rt_begin,
    LC.rt_after_tools,
    LC.rt_end,
    LC.timestamp AS level_timestamp,
    LC.level_end,
    LC.portal_done,
    LC.fire_done,
    LC.pollution_done,
    LC.map_dead_ends,
    LC.tools_end_charge,
    LC.portal_charge,
    LC.fire_charge,
    LC.pollution_charge,
    LC.rt_charge
FROM
	(SELECT *,
		(SELECT count(DISTINCT game_id) FROM cdm.level P WHERE P.user_id = L.user_id) AS games_played,
		(SELECT count(*) FROM cdm.level C WHERE C.level_id = L.level_id AND C.user_id = L.user_id GROUP BY C.level_id) AS level_times
	 FROM cdm.level L
	 WHERE	(L.user_id, L.level_id, L.timestamp) IN 
		(SELECT LVL.user_id,LVL.level_id, min(LVL.timestamp) 
		 FROM 
			(SELECT user_id,level_id,timestamp,level_end 
			 FROM cdm.level 
			 WHERE level_end = 0
			 AND level_id > 0 AND level_id < 7 AND level_id != 4 
			 AND LENGTH(map_trail)>6
			) AS LVL 
		 GROUP BY LVL.user_id, LVL.level_id)
	) AS LC, 
	cdm.users U
WHERE LC.user_id = U.user_id 
AND U.create_time > '2019-06-10 08:00:00' AND U.create_time < '2019-07-22 08:00:00'
ORDER BY LC.user_id ASC, LC.level_id ASC, U.create_time DESC
LIMIT 100;


SELECT GAME.usr_id,
 MAX(CASE GAME.n )
FROM
(SELECT 
		LVL.id,
    LVL.user_id,
    LVL.level_id,
    MIN(LVL.timestamp)
FROM
    (
    SELECT
        user_id,
        level_id,
        TIMESTAMP,
        level_end
    FROM
        cdm.level
    WHERE
        level_end = 0 AND level_id > 0 AND level_id < 7 AND level_id != 4 AND LENGTH(map_trail) > 6
) AS LVL
GROUP BY
    LVL.user_id,
    LVL.level_id) GAME



SELECT *
FROM level LVL
INNER JOIN (
    SELECT user_id, level_id, GROUP_CONCAT( id, ' ', timestamp ORDER BY timestamp ) pair
    FROM cdm.level
    GROUP BY user_id, level_id ) L ON LVL.user_id = L.user_id AND LVL.id = SUBSTRING_INDEX(SUBSTRING_INDEX(L.pair, ',', 1), ' ', 1)



		SELECT COUNT(*)
FROM level LVL
INNER JOIN (
    SELECT user_id, level_id, GROUP_CONCAT( id, ' ', timestamp ORDER BY timestamp ) pair
    FROM cdm.level
    WHERE level_end = 0 AND level_id > 0 AND level_id < 7 AND level_id != 4 AND LENGTH(map_trail) > 6
    GROUP BY user_id, level_id ) L ON LVL.user_id = L.user_id AND LVL.id = SUBSTRING_INDEX(SUBSTRING_INDEX(L.pair, ',', 1), ' ', 1)
WHERE tools_selected = ''



		(SELECT count(DISTINCT game_id) FROM cdm.level P WHERE P.user_id = LVL.user_id) AS games_played,
		(SELECT count(*) FROM cdm.level C WHERE C.level_id = LVL.level_id AND C.user_id = LVL.user_id GROUP BY C.level_id) AS level_times

SELECT level_id,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero22COLLABNEUTRAL1' THEN D.answer_id END) AS Bombero2_2_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero22COLLABNEUTRAL5' THEN D.answer_id END) AS Bombero2_2_5,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero31NARRATIVENEUTRAL1' THEN D.answer_id END) AS Bombero3_1_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero31NARRATIVENEUTRAL3' THEN D.answer_id END) AS Bombero3_1_3,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero31NARRATIVENEUTRAL4' THEN D.answer_id END) AS Bombero3_1_4,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero36COLLABNEUTRAL1' THEN D.answer_id END) AS Bombero3_6_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero36COLLABNEUTRAL2' THEN D.answer_id END) AS Bombero3_6_2,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero36COLLABPOSITIVE2' THEN D.answer_id END) AS Bombero3_6_2,
	COUNT(CASE WHEN DC.dialog_columns = 'Bombero4COLLABNEUTRAL0' THEN D.answer_id END) AS Bombero_4_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mark3ETNEUTRAL0' THEN D.answer_id END) AS Mark_3_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mark3ETNEUTRAL3' THEN D.answer_id END) AS Mark_3_3,
	COUNT(CASE WHEN DC.dialog_columns = 'Merk2NARRATIVENEUTRAL0' THEN D.answer_id END) AS Merk_2_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Merk2NARRATIVENEUTRAL1' THEN D.answer_id END) AS Merk_2_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Merk5COLLABNEUTRAL0' THEN D.answer_id END) AS Merk_5_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mindy5NARRATIVENEUTRAL0' THEN D.answer_id END) AS Mindy_5_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mindy5NARRATIVENEUTRAL1' THEN D.answer_id END) AS Mindy_5_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Mindy5NARRATIVENEUTRAL2' THEN D.answer_id END) AS Mindy_5_2,
	COUNT(CASE WHEN DC.dialog_columns = 'Mindy5NARRATIVENEUTRAL3' THEN D.answer_id END) AS Mindy_5_3,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk1ETNEUTRAL0' THEN D.answer_id END) AS Mirk_1_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk1ETNEUTRAL1' THEN D.answer_id END) AS Mirk_1_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk1ETPOSITIVE1' THEN D.answer_id END) AS Mirk_1_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk1ETPOSITIVE2' THEN D.answer_id END) AS Mirk_1_2,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk2ETNEUTRAL0' THEN D.answer_id END) AS Mirk_2_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk2ETNEUTRAL2' THEN D.answer_id END) AS Mirk_2_2,
	COUNT(CASE WHEN DC.dialog_columns = 'Mirk6ETNEUTRAL1' THEN D.answer_id END) AS Mirk_6_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Mork3ETNEUTRAL0' THEN D.answer_id END) AS Mork_3_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mork5COLLABNEUTRAL0' THEN D.answer_id END) AS Mork_5_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mork5COLLABNEUTRAL1' THEN D.answer_id END) AS Mork_5_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Mork6COLLABNEUTRAL0' THEN D.answer_id END) AS Mork_6_0,
	COUNT(CASE WHEN DC.dialog_columns = 'Mork6COLLABNEUTRAL1' THEN D.answer_id END) AS Mork_6_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Murk3NARRATIVENEUTRAL1' THEN D.answer_id END) AS Murk_3_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Murk5COLLABNEUTRAL1' THEN D.answer_id END) AS Murk_5_1,
	COUNT(CASE WHEN DC.dialog_columns = 'Patovalien5AUTOEVALNEUTRAL1' THEN D.answer_id END) AS Patovalien_5_1
FROM
 (SELECT DISTINCT CONCAT(`character_name`,`level_id`,`dialog_type`,`dialog_mood`,`dialog_index`) as 'dialog_columns'
 FROM `dialogs`
 WHERE valor_indicador <> '') DC
LEFT JOIN `dialogs` D ON CONCAT(D.`character_name`,D.`level_id`,D.`dialog_type`,D.`dialog_mood`,D.`dialog_index`) = DC.dialog_columns
GROUP BY D.`level_id`
ORDER BY D.`level_id`;


SELECT user_id,GROUP_CONCAT(level_id,' ',level_timestamp ORDER BY level_id) FROM `best_game` GROUP BY user_id

*/
SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(

      'MAX(CASE WHEN level_id = ''',
      level_id,
      ''' THEN level_timestamp END) AS ',
       CONCAT('lvl_',level_id,'_timestamp')

			 
    ) 
  ) AS 'columns_lvl'
FROM best_game



MAX(CASE WHEN level_id = '1' THEN level_timestamp END) AS lvl_1_timestamp,
MAX(CASE WHEN level_id = '2' THEN level_timestamp END) AS lvl_2_timestamp,
MAX(CASE WHEN level_id = '3' THEN level_timestamp END) AS lvl_3_timestamp,
MAX(CASE WHEN level_id = '5' THEN level_timestamp END) AS lvl_5_timestamp,
MAX(CASE WHEN level_id = '6' THEN level_timestamp END) AS lvl_6_timestamp


#SELECT SUBSTR(tools_selected,LOCATE(";",tools_selected)+1,LOCATE(";",tools_selected,LOCATE(";",tools_selected)+1) ) FROM `best_game`

SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(tools_selected,';',1),':',-1) FROM best_game

CREATE TABLE best_game_view AS
SELECT id,user_id,create_time,computer_id,game_id,level_id,level_replay,level_prev,level_post,level_better,Restaurador,Matafuegos,Armonizador,missions,map_checks,level_time,game_time,first_map_time,mission_time,tools_time,map_trail,map_dead_ends,rt_begin,rt_after_tools,rt_end,level_timestamp,level_end,portal_done,fire_done,pollution_done,portal_charge,fire_charge,pollution_charge,rt_charge
FROM best_game



/*
SELECT user_id,level_id,MIN(timestamp)
FROM dialog_replies
GROUP BY user_id, level_id

Obtener el game_time del level 7
SELECT 
	( TIME_TO_SEC(TIMEDIFF( (SELECT level_timestamp FROM `best_game` WHERE level_id = 7 AND user_id = B.user_id),(SELECT level_timestamp FROM `best_game` WHERE level_id = 6 AND user_id = B.user_id) )) - (SELECT (tools_time + mission_time + first_map_time) FROM `best_game` WHERE level_id = 7 AND user_id = B.user_id) ) as game_time
from best_game B