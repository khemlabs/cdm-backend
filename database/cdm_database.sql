-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2017 at 04:29 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--
DROP TABLE IF EXISTS `cursos`;

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `cue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `grado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `turno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_user`
--

DROP TABLE IF EXISTS `data_user`;

CREATE TABLE `data_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dialogs`
--

DROP TABLE IF EXISTS `dialogs`;

CREATE TABLE `dialogs` (
  `id` int(11) NOT NULL,
  `character_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
  `dialog_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dialog_index` int(11) NOT NULL,
  `dialog_mood` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dialog_prompt` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `answer_id` int(11) NOT NULL,
  `answer_text` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `tipo_indicador` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `subtipo_indicador` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `valor_indicador` varchar(300) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dialogs`
--

INSERT INTO `dialogs` (`id`, `character_name`, `level_id`, `dialog_type`, `dialog_index`, `dialog_mood`, `dialog_prompt`, `answer_id`, `answer_text`, `create_time`, `tipo_indicador`, `subtipo_indicador`, `valor_indicador`) VALUES
(235,'Agustina',2,'NARRATIVE',0,'NEUTRAL','¿Qué hacen acá? ¡La ciudad está terrible!',0,'¡Por eso te necesitamos! En el camino te explico, ¿estás lista?','2019-04-17 03:11:46','','',''),
(1,'Agustina',2,'NARRATIVE',1,'NEUTRAL','¿Yo? ¡Siempre!',0,'¡Grande, Agus!','2019-04-17 03:11:46','','',''),
(2,'Agustina',7,'NARRATIVE',0,'NEUTRAL','Yo me ocupo de esta antena. ¡Vamos, nosotros podemos!',0,'¡Vamos, sí!','2019-04-17 03:11:46','','',''),
(3,'Agustina',7,'NARRATIVE',1,'NEUTRAL','¿Yo? ¡Siempre!',0,'¡Grande, Agus!','2019-04-17 03:11:46','','',''),
(4,'Bombero2',2,'COLLAB',0,'NEUTRAL','Ustedes son los ayudantes de la doctora, ¿no?',0,'Sí, vamos al puerto. ¿De dónde podemos sacar combustible para una lancha?','2019-04-17 03:11:46','','',''),
(5,'Bombero2',2,'COLLAB',1,'NEUTRAL','Les buscaríamos un bidón pero estamos ocupados con los incendios. ¿Capaz podemos trabajar juntos?',0,'Sí, nosotros podemos ocuparnos de un incendio y ustedes del bidón.','2019-04-17 03:11:46','COLABORATIVO','TODOS','sí'),
(6,'Bombero2',2,'COLLAB',1,'NEUTRAL','Les buscaríamos un bidón pero estamos ocupados con los incendios. ¿Capaz podemos trabajar juntos?',1,'No, trabajar juntos nos demoraría.','2019-04-17 03:11:46','COLABORATIVO','TODOS','no'),
(7,'Bombero2',2,'COLLAB',1,'NEUTRAL','Les buscaríamos un bidón pero estamos ocupados con los incendios. ¿Capaz podemos trabajar juntos?',2,'No creo que sea buena esa idea...','2019-04-17 03:11:46','COLABORATIVO','TODOS','no - moderado'),
(8,'Bombero2',2,'COLLAB',2,'NEUTRAL','Avisen si se arrepienten.',0,'¡Quedamos así!','2019-04-17 03:11:46','','',''),
(9,'Bombero2',2,'COLLAB',2,'POSITIVE','¡Perfecto!',0,'¡Quedamos así!','2019-04-17 03:11:46','','',''),
(10,'Bombero2',2,'COLLAB',3,'NEUTRAL','¿Y? ¿Qué hay con ese fuego?',0,'Ah, sí','2019-04-17 03:11:46','','',''),
(11,'Bombero2',2,'COLLAB',4,'NEUTRAL','¡Lo hicieron! Acá tienen la nafta.',0,'¡Esa!','2019-04-17 03:11:46','','',''),
(12,'Bombero2',2,'COLLAB',5,'NEUTRAL','Ahora sigan, y hasta que lleguen al puerto despreocúpense de los incendios que nosotros los iremos apagando.',0,'Qué bien, un tema menos. ¡Saludos a toda la banda!','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','sí'),
(13,'Bombero2',2,'COLLAB',5,'NEUTRAL','Ahora sigan, y hasta que lleguen al puerto despreocúpense de los incendios que nosotros los iremos apagando.',1,'No confío en que puedan solos. Si encuentro uno, lo apago.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','no'),
(14,'Bombero2',2,'COLLAB',5,'NEUTRAL','Ahora sigan, y hasta que lleguen al puerto despreocúpense de los incendios que nosotros los iremos apagando.',2,'Mmm no sé si confiar en que puedan solos... Veo si apago alguno.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','no - moderado'),
(15,'Bombero2',2,'COLLAB',6,'NEUTRAL','Ahora sigan, y hasta que lleguen al puerto despreocúpense de los incendios que yo los iré apagando.',0,'Okey','2019-04-17 03:11:46','','',''),
(16,'Bombero3',1,'NARRATIVE',0,'NEUTRAL','Hola, ¿qué hacen por acá? Hay un incendio, no se puede pasar.',0,'Estamos ayudando a la doctora Grimberg a cerrar los portales interdimensionales.','2019-04-17 03:11:46','','',''),
(17,'Bombero3',1,'NARRATIVE',1,'NEUTRAL','¡Ah, ella nos habló de ustedes! En la próxima cuadra hay uno de esos portales, ¿pueden ayudar a cerrarlo?',0,'¡Claro!','2019-04-17 03:11:46','COLABORATIVO','TODOS','sí'),
(18,'Bombero3',1,'NARRATIVE',1,'NEUTRAL','¡Ah, ella nos habló de ustedes! En la próxima cuadra hay uno de esos portales, ¿pueden ayudar a cerrarlo?',1,'Mmm, ¿justo ahora?','2019-04-17 03:11:46','COLABORATIVO','TODOS','moderado'),
(19,'Bombero3',1,'NARRATIVE',1,'NEUTRAL','¡Ah, ella nos habló de ustedes! En la próxima cuadra hay uno de esos portales, ¿pueden ayudar a cerrarlo?',2,'No','2019-04-17 03:11:46','COLABORATIVO','TODOS','no'),
(20,'Bombero3',1,'NARRATIVE',2,'NEUTRAL','No lo pediríamos si no fuera necesario.',0,'Está bien.','2019-04-17 03:11:46','','',''),
(21,'Bombero3',1,'NARRATIVE',3,'NEUTRAL','El único tema es que en el camino encontrarán incendios...',0,'¿Entonces nos darían un matafuegos, por favor?','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - buen modo'),
(22,'Bombero3',1,'NARRATIVE',3,'NEUTRAL','El único tema es que en el camino encontrarán incendios...',1,'Dame ya un matafuegos que estamos apurados, ¡¿no ven?!','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - agresivo'),
(23,'Bombero3',1,'NARRATIVE',3,'NEUTRAL','El único tema es que en el camino encontrarán incendios...',2,'¿Incendios? Uff, qué mala suerte','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','no pedir'),
(24,'Bombero3',1,'NARRATIVE',4,'NEGATIVE','Acá tienen uno.',0,'Listo','2019-04-17 03:11:46','','',''),
(25,'Bombero3',1,'NARRATIVE',4,'NEUTRAL','¿Quieren un matafuegos?',0,'¡Aceptamos, gracias!','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','sí'),
(26,'Bombero3',1,'NARRATIVE',4,'NEUTRAL','¿Quieren un matafuegos?',1,'No, gracias.','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','no'),
(27,'Bombero3',1,'NARRATIVE',4,'POSITIVE','Seguro, lo van a necesitar.',0,'Gracias','2019-04-17 03:11:46','','',''),
(28,'Bombero3',1,'NARRATIVE',5,'NEUTRAL','LLévenlo igual, lo van a necesitar.',0,'...','2019-04-17 03:11:46','','',''),
(29,'Bombero3',1,'NARRATIVE',6,'NEUTRAL','Eso es todo. Cuídense mucho.',0,'Ustedes también.','2019-04-17 03:11:46','','',''),
(30,'Bombero3',6,'COLLAB',0,'NEUTRAL','Chicos, no se puede cruzar el puente por acá, hay peligro de derrumbe.',0,'Deberemos encontrar otro camino, entonces.','2019-04-17 03:11:46','','',''),
(31,'Bombero3',6,'COLLAB',1,'NEUTRAL','Sí, pero les pedimos un favor. Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',0,'OK, si nos cruzamos con uno, lo apagamos.','2019-04-17 03:11:46','COLABORATIVO','TODOS','sí'),
(32,'Bombero3',6,'COLLAB',1,'NEUTRAL','Sí, pero les pedimos un favor. Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',1,'Mmmh, lo vamos viendo, no creo que podamos parar para eso.','2019-04-17 03:11:46','COLABORATIVO','TODOS','no - moderado'),
(33,'Bombero3',6,'COLLAB',1,'NEUTRAL','Sí, pero les pedimos un favor. Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',2,'Ese es su trabajo, nosotros tenemos otro.','2019-04-17 03:11:46','COLABORATIVO','TODOS','no - agresivo'),
(34,'Bombero3',6,'COLLAB',2,'NEUTRAL','Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',0,'Dale, si nos cruzamos con uno, lo apagamos.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(35,'Bombero3',6,'COLLAB',2,'NEUTRAL','Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',1,'Mmmh, lo vamos viendo, no creo que podamos parar para eso.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(36,'Bombero3',6,'COLLAB',2,'NEUTRAL','Si nos ayudan a apagar al menos un incendio, evitaremos entre todos que haya más derrumbes.',2,'Ese es tu trabajo, viejo, nosotros tenemos otro.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(37,'Bombero3',6,'COLLAB',2,'POSITIVE','Entonces, ¿aceptarías que te demos una carga extra de matafuegos?',0,'No sé si da.','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','no'),
(38,'Bombero3',6,'COLLAB',2,'POSITIVE','Entonces, ¿aceptarías que te demos una carga extra de matafuegos?',1,'¡Seguro!','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','sí'),
(39,'Bombero3',6,'COLLAB',2,'POSITIVE','Entonces, ¿aceptarías que te demos una carga extra de matafuegos?',2,'No va a hacer falta.','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','no'),
(40,'Bombero3',6,'COLLAB',3,'NEUTRAL','Esperamos que encuentren cómo cruzar el puente.',0,'Nosotros también.','2019-04-17 03:11:46','','',''),
(41,'Bombero3',6,'COLLAB',4,'NEUTRAL','¡Gracias por ayudar con los incendios!',0,'No Hay problema.','2019-04-17 03:11:46','','',''),
(42,'Bombero',4,'COLLAB',0,'NEUTRAL','Esta zona es rarísima. Las calles cambian de lugar a cada rato.',0,'¿Podrías decirnos cómo salir?','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - buen modo'),
(43,'Bombero',4,'COLLAB',0,'NEUTRAL','Esta zona es rarísima. Las calles cambian de lugar a cada rato.',1,'¿Y a mí qué me importa? Decínos algo útil, cómo salir, por ejemplo.','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - agresivo'),
(44,'Bombero',4,'COLLAB',0,'NEUTRAL','Esta zona es rarísima. Las calles cambian de lugar a cada rato.',2,'Sí, es rara la zona...','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','no pedir'),
(45,'Bombero',4,'COLLAB',1,'POSITIVE','Me encantaría pero estoy perdido. No puedo orientarlos.',0,'Uh, bueno, nos vemos, entonces.','2019-04-17 03:11:46','','',''),
(46,'Bombero',4,'COLLAB',1,'NEGATIVE','Les estoy diciendo que las calles cambian solas. ¡No sé cómo salir!',0,'Entonces, chau.','2019-04-17 03:11:46','','',''),
(47,'Bombero',4,'COLLAB',2,'NEUTRAL','¿Ustedes, de nuevo? Ya les dije que estoy perdido.',0,'Cierto.','2019-04-17 03:11:46','','',''),
(48,'Dra Grimberg',1,'AUTOEVAL',0,'NEUTRAL','Antes de que se vayan,¿creés que llegarás al lado opuesto de esta parte de la ciudad?',0,'¡Seguro, doctora!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(49,'Dra Grimberg',1,'AUTOEVAL',0,'NEUTRAL','Antes de que se vayan,¿creés que llegarás al lado opuesto de esta parte de la ciudad?',1,'Haré lo posible','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(50,'Dra Grimberg',1,'AUTOEVAL',0,'NEUTRAL','Antes de que se vayan,¿creés que llegarás al lado opuesto de esta parte de la ciudad?',2,'No creo que pueda','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(51,'Dra Grimberg',2,'AUTOEVAL',0,'NEUTRAL','Vienen muy bien. ¿Necesitan algo?',0,'Ahora que lo dice... Creo que estamos cerca de la casa de Agustina...','2019-04-17 03:11:46','','',''),
(52,'Dra Grimberg',2,'AUTOEVAL',1,'NEUTRAL','Recuerden esto: van a necesitar nafta para la lancha. Consíganla antes de llegar al puerto.',0,'Afirmativo, doc.','2019-04-17 03:11:46','','',''),
(53,'Dra Grimberg',3,'AUTOEVAL',0,'NEUTRAL','¡Ya están más cerca! Sólo falta embarcarse hasta la isla. ¿Cómo lo ves, #Manu?',0,'No lo veo.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(54,'Dra Grimberg',3,'AUTOEVAL',0,'NEUTRAL','¡Ya están más cerca! Sólo falta embarcarse hasta la isla. ¿Cómo lo ves, #Manu?',1,'¡Súper!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(55,'Dra Grimberg',3,'AUTOEVAL',0,'NEUTRAL','¡Ya están más cerca! Sólo falta embarcarse hasta la isla. ¿Cómo lo ves, #Manu?',2,'Mmmm, complicado.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(56,'Dra Grimberg',3,'AUTOEVAL',1,'NEUTRAL','Equipo, están saliendo de la ciudad. #Manu, ¿qué tal te sentís con lo hecho hasta ahora?',0,'¡Nací para esto!','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(57,'Dra Grimberg',3,'AUTOEVAL',1,'NEUTRAL','Equipo, están saliendo de la ciudad. #Manu, ¿qué tal te sentís con lo hecho hasta ahora?',1,'Creo que lo hice bastante bien!','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(58,'Dra Grimberg',3,'AUTOEVAL',1,'NEUTRAL','Equipo, están saliendo de la ciudad. #Manu, ¿qué tal te sentís con lo hecho hasta ahora?',2,'Podría haberlo hecho mucho mejor.','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(59,'Dra Grimberg',4,'AUTOEVAL',0,'NEUTRAL','La superposición de dimensiones generó una zona laberíntica. ¿Te parece que podrás salir de ella?',0,'Claro que sí!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(60,'Dra Grimberg',4,'AUTOEVAL',0,'NEUTRAL','La superposición de dimensiones generó una zona laberíntica. ¿Te parece que podrás salir de ella?',1,'¿La verdad? ¡No sé!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(61,'Dra Grimberg',4,'AUTOEVAL',0,'NEUTRAL','La superposición de dimensiones generó una zona laberíntica. ¿Te parece que podrás salir de ella?',2,'Lo veo dificilísimo','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(62,'Dra Grimberg',4,'AUTOEVAL',1,'NEUTRAL','Chicos veo que se les complica pasar esta zona, les abri un portal para que lleguen al otro lado! ¿Lo quieren usar?',0,'¡Esta bien!','2019-04-17 03:11:46','','',''),
(63,'Dra Grimberg',4,'AUTOEVAL',1,'NEUTRAL','Chicos veo que se les complica pasar esta zona, les abri un portal para que lleguen al otro lado! ¿Lo quieren usar?',1,'¡No, nosotros podemos pasarlo sin problemas!','2019-04-17 03:11:46','','',''),
(64,'Dra Grimberg',5,'AUTOEVAL',0,'NEUTRAL','Bueno, ¿qué decís, #Manu? ¿Te sentís capaz de salir del nivel en menos de 2 minutos?',0,'De una!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(65,'Dra Grimberg',5,'AUTOEVAL',0,'NEUTRAL','Bueno, ¿qué decís, #Manu? ¿Te sentís capaz de salir del nivel en menos de 2 minutos?',1,'Más o menos.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(66,'Dra Grimberg',5,'AUTOEVAL',0,'NEUTRAL','Bueno, ¿qué decís, #Manu? ¿Te sentís capaz de salir del nivel en menos de 2 minutos?',2,'Ni ahí','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(67,'Dra Grimberg',6,'AUTOEVAL',0,'NEUTRAL','Al cruzar el puente llegarás a la universidad. Allí se dará la misión final. ¿Cómo te ves cruzando el puente?',0,'¡Súper!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(68,'Dra Grimberg',6,'AUTOEVAL',0,'NEUTRAL','Al cruzar el puente llegarás a la universidad. Allí se dará la misión final. ¿Cómo te ves cruzando el puente?',1,'Mmmm, complicado.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(69,'Dra Grimberg',6,'AUTOEVAL',0,'NEUTRAL','Al cruzar el puente llegarás a la universidad. Allí se dará la misión final. ¿Cómo te ves cruzando el puente?',2,'No lo veo.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(70,'Dra Grimberg',6,'AUTOEVAL',1,'NEUTRAL','Estás dejando atrás la isla, ¿qué tal te sentís con lo hecho hasta ahora?',0,'¡Nací para esto!','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(71,'Dra Grimberg',6,'AUTOEVAL',1,'NEUTRAL','Estás dejando atrás la isla, ¿qué tal te sentís con lo hecho hasta ahora?',1,'¡Creo que lo hice bastante bien!','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(72,'Dra Grimberg',6,'AUTOEVAL',1,'NEUTRAL','Estás dejando atrás la isla, ¿qué tal te sentís con lo hecho hasta ahora?',2,'Podría haberlo hecho mucho mejor.','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(73,'Dra Grimberg',7,'AUTOEVAL',0,'NEUTRAL','Deben desconectar las tres antenas a la vez, y para eso, cada uno de ustedes debe ocuparse de una. #Manu, ¿cómo te ves cerrando el vórtice?',0,'¡Súper!','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(74,'Dra Grimberg',7,'AUTOEVAL',0,'NEUTRAL','Deben desconectar las tres antenas a la vez, y para eso, cada uno de ustedes debe ocuparse de una. #Manu, ¿cómo te ves cerrando el vórtice?',1,'Mmmm, complicado.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(75,'Dra Grimberg',7,'AUTOEVAL',0,'NEUTRAL','Deben desconectar las tres antenas a la vez, y para eso, cada uno de ustedes debe ocuparse de una. #Manu, ¿cómo te ves cerrando el vórtice?',2,'No lo veo.','2019-04-17 03:11:46','AUTOEFICACIA','JUICIO',''),
(76,'Dra Grimberg',7,'AUTOEVAL',1,'NEUTRAL','¡Lo lograron! #Manu, ¿qué tal te sentís con lo que hiciste?',0,'¡Me siento genial! Salió todo perfecto.','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(77,'Dra Grimberg',7,'AUTOEVAL',1,'NEUTRAL','¡Lo lograron! #Manu, ¿qué tal te sentís con lo que hiciste?',1,'Creo que lo hice bastante bien!','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(78,'Dra Grimberg',7,'AUTOEVAL',1,'NEUTRAL','¡Lo lograron! #Manu, ¿qué tal te sentís con lo que hiciste?',2,'Podría haberlo hecho mucho mejor.','2019-04-17 03:11:46','AUTOEFICACIA','AUTOPERCEPCIÓN',''),
(79,'Emmanuel',2,'NARRATIVE',0,'NEUTRAL','¡Sí! Vamos a buscarla, ¡la necesitamos!',0,'Con ella en el equipo no nos para nadie.','2019-04-17 03:11:46','','',''),
(80,'Emmanuel',2,'NARRATIVE',1,'NEUTRAL','Pero gente, ¿no será peligroso eso?',0,'Dale, Emma, si apagamos incendios, ¡más vale que podemos rastrear un bidón!','2019-04-17 03:11:46','','',''),
(81,'Emmanuel',7,'NARRATIVE',0,'NEUTRAL','Esta es mi antena. Me quedo acá.',0,'¡Ánimo, Emma!','2019-04-17 03:11:46','','',''),
(82,'Mark',1,'ET',0,'NEUTRAL','Aunque ahora mismo esto sea un desastre, nos encanta saber que hay seres de otras dimensiones.',0,'¡A mí también!','2019-04-17 03:11:46','NARRATIVO','NARRATIVO',''),
(83,'Mark',1,'ET',0,'NEUTRAL','Aunque ahora mismo esto sea un desastre, nos encanta saber que hay seres de otras dimensiones.',1,'Estaría más encantado si el mundo no estuviera patas arriba.','2019-04-17 03:11:46','NARRATIVO','NARRATIVO',''),
(84,'Mark',1,'ET',0,'NEUTRAL','Aunque ahora mismo esto sea un desastre, nos encanta saber que hay seres de otras dimensiones.',2,'No veo nada bueno en eso.','2019-04-17 03:11:46','NARRATIVO','NARRATIVO',''),
(85,'Mark',1,'ET',1,'NEUTRAL','No tengo nada más para decir por ahora.',0,'Salir','2019-04-17 03:11:46','NARRATIVO','NARRATIVO',''),
(86,'Mark',3,'ET',0,'NEUTRAL','¡¿Vieron la cantidad de incendios que hay?!',0,'El bombero dijo que ellos se ocuparán por ahora y le creemos.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','sí'),
(87,'Mark',3,'ET',0,'NEUTRAL','¡¿Vieron la cantidad de incendios que hay?!',1,'No sabemos si el bombero dijo la verdad pero nuestra prioridad son los portales','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','moderado'),
(88,'Mark',3,'ET',0,'NEUTRAL','¡¿Vieron la cantidad de incendios que hay?!',2,'Seguro que los bomberos no los apagan, pero ya fue...','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','no'),
(89,'Mark',3,'ET',1,'NEGATIVE','Opino que igual ustedes deberían apagar al menos tres incendios.',0,'Repito, no podemos hacer nada.','2019-04-17 03:11:46','','',''),
(90,'Mark',3,'ET',1,'POSITIVE','Opino que igual deberían apagar al menos tres incendios.',0,'Ellos mismos nos dijeron que nos despreocupemos.','2019-04-17 03:11:46','','',''),
(91,'Mark',3,'ET',1,'NEUTRAL','Opino que igual deberían apagar al menos tres incendios.',0,'Bueno, capaz que tenés razón.','2019-04-17 03:11:46','','',''),
(92,'Mark',3,'ET',2,'NEUTRAL','Insisto, deberían apagar al menos tres incendios, pero ustedes hagan lo que les parezca.',0,'Eso haremos.','2019-04-17 03:11:46','','',''),
(93,'Mark',3,'ET',3,'NEUTRAL','Ahora que extinguieron los fuegos, cantaré alabanzas para ustedes llevará su tiempo pero son realmente hermosas.',0,'Necesitamos seguir con lo nuestro, ¡hasta luego!','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí'),
(94,'Mark',3,'ET',3,'NEUTRAL','Ahora que extinguieron los fuegos, cantaré alabanzas para ustedes llevará su tiempo pero son realmente hermosas.',1,'¡Nos hicieron perder un montón de tiempo, pesados! ¡Chau!','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí - agresivo'),
(95,'Mark',3,'ET',3,'NEUTRAL','Ahora que extinguieron los fuegos, cantaré alabanzas para ustedes llevará su tiempo pero son realmente hermosas.',2,'Somos todo oídos.','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','no'),
(96,'Mark',3,'ET',4,'NEUTRAL','Looooo, roliloooooooo...',0,'Conmovedor...','2019-04-17 03:11:46','','',''),
(97,'Mark',3,'ET',4,'NEUTRAL','Looooo, roliloooooooo...',1,'Salir','2019-04-17 03:11:46','','',''),
(98,'Mark',3,'ET',5,'NEUTRAL','Liiiiiii, ralaliiiiiiiiiiii...',0,'Bien ahí...','2019-04-17 03:11:46','','',''),
(99,'Mark',3,'ET',5,'NEUTRAL','Liiiiiii, ralaliiiiiiiiiiii...',1,'Salir','2019-04-17 03:11:46','','',''),
(100,'Mark',3,'ET',6,'NEUTRAL','Po, poropopóóóóóoóó...',0,'Esa melodía me suena de algún lado.','2019-04-17 03:11:46','','',''),
(101,'Mark',3,'ET',6,'NEUTRAL','Po, poropopóóóóóoóó...',1,'Salir','2019-04-17 03:11:46','','',''),
(102,'Mark',3,'ET',7,'NEUTRAL','Cuaaaaa, lalán truliiiiip...',0,'¿Una que sepamos todos?','2019-04-17 03:11:46','','',''),
(103,'Mark',3,'ET',7,'NEUTRAL','Cuaaaaa, lalán truliiiiip...',1,'Salir','2019-04-17 03:11:46','','',''),
(104,'Mark',3,'ET',8,'NEUTRAL','Oh, singarinlááááááá...',0,'¡Qué voz! ¡Y qué insistencia!','2019-04-17 03:11:46','','',''),
(105,'Mark',3,'ET',8,'NEUTRAL','Oh, singarinlááááááá...',1,'Salir','2019-04-17 03:11:46','','',''),
(106,'Mark',3,'ET',9,'NEUTRAL','Puloyarún, quetelacuáááá...',0,'Creo que ya podemos darnos por alabados, ¡gracias!','2019-04-17 03:11:46','','',''),
(107,'Mark',3,'ET',10,'NEUTRAL','Ha sido un placer conocerlos, heroicos seres.',0,'Igualmente, ¡hasta luego!','2019-04-17 03:11:46','','',''),
(108,'Merk',2,'NARRATIVE',0,'NEUTRAL','...',0,'Hola, ¿podemos ayudarte en algo?','2019-04-17 03:11:46','ASERTIVIDAD','PROACTIVIDAD','sí'),
(109,'Merk',2,'NARRATIVE',0,'NEUTRAL','...',1,'(En realidad no es un buen momento para parar.)','2019-04-17 03:11:46','ASERTIVIDAD','PROACTIVIDAD','no - moderado'),
(110,'Merk',2,'NARRATIVE',0,'NEUTRAL','...',2,'Corréte, tonto, que queremos seguir.','2019-04-17 03:11:46','ASERTIVIDAD','PROACTIVIDAD','no - agresivo'),
(111,'Merk',2,'NARRATIVE',1,'NEUTRAL','Debo llevar a los míos un solo recurso tecnológico y no sé cómo conseguirlo.',0,'Nosotros tenemos RT. Podemos darte un poco.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(112,'Merk',2,'NARRATIVE',1,'NEUTRAL','Debo llevar a los míos un solo recurso tecnológico y no sé cómo conseguirlo.',1,'Nosotros tenemos RT pero no podemos darte.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(113,'Merk',2,'NARRATIVE',1,'NEUTRAL','Debo llevar a los míos un solo recurso tecnológico y no sé cómo conseguirlo.',2,'Corréte de nuestro camino que queremos seguir.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(114,'Merk',2,'NARRATIVE',2,'POSITIVE','¡Eso sería un gesto maravilloso, acepto!',0,'Tomá RT, ¡y suerte!','2019-04-17 03:11:46','','',''),
(115,'Merk',2,'NARRATIVE',2,'NEUTRAL','Oh, qué lástima. Adiós entonces.',0,'Adiós.','2019-04-17 03:11:46','','',''),
(116,'Merk',2,'NARRATIVE',3,'NEUTRAL','Ya tengo recurso tecnológico. Gracias y buen viaje.',0,'¡Bueno!','2019-04-17 03:11:46','','',''),
(117,'Merk',5,'COLLAB',0,'NEUTRAL','Los ayudaríamos a limpiar la zona pero necesitaríamos algún recurso tecnológico para nuestro antiplasma. ¿Podrán darnos uno de los suyos?',0,'Mmmm... No estoy Seguro...','2019-04-17 03:11:46','COLABORATIVO','COMPARTIR','no'),
(118,'Merk',5,'COLLAB',0,'NEUTRAL','Los ayudaríamos a limpiar la zona pero necesitaríamos algún recurso tecnológico para nuestro antiplasma. ¿Podrán darnos uno de los suyos?',1,'Sí. Tomen un poco.','2019-04-17 03:11:46','COLABORATIVO','COMPARTIR','sí'),
(119,'Merk',5,'COLLAB',0,'NEUTRAL','Los ayudaríamos a limpiar la zona pero necesitaríamos algún recurso tecnológico para nuestro antiplasma. ¿Podrán darnos uno de los suyos?',2,'Lo lamento pero no. Puede que los necesitemos más adelante.','2019-04-17 03:11:46','COLABORATIVO','COMPARTIR','no'),
(120,'Merk',5,'COLLAB',1,'NEUTRAL','Les agradezco. Ya mismo nos ocuparemos de eso.',0,'Perfecto. ¡Suerte!','2019-04-17 03:11:46','','',''),
(121,'Mindy',5,'NARRATIVE',0,'NEUTRAL','...',0,'¿Hola? Te noto preocupada','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','preocupado (preocupado)'),
(122,'Mindy',5,'NARRATIVE',0,'NEUTRAL','...',1,'¿Hola? Te noto irritada','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','irritado (preocupado)'),
(123,'Mindy',5,'NARRATIVE',0,'NEUTRAL','...',2,'¿Hola? Te noto confundida','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','confundida (preocupado)'),
(124,'Mindy',5,'NARRATIVE',1,'NEUTRAL','Estoy preocupadísima.',0,'¿Puedo preguntarte por qué?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(125,'Mindy',5,'NARRATIVE',1,'NEUTRAL','Estoy preocupadísima.',1,'(No puedo ocuparme de todo, yo sigo.)','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(126,'Mindy',5,'NARRATIVE',1,'NEUTRAL','Estoy preocupadísima.',2,'No parece... Nosotros estamos ocupadísimos, queremos seguir.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(127,'Mindy',5,'NARRATIVE',2,'NEUTRAL','No encuentro a mi hijo. Tuvimos un accidente con la nave, y nos separamos. ¿Me pueden ayudar?',0,'Nosotros somos tres, ya mismo salimos a buscarlo.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(128,'Mindy',5,'NARRATIVE',2,'NEUTRAL','No encuentro a mi hijo. Tuvimos un accidente con la nave, y nos separamos. ¿Me pueden ayudar?',1,'Uh, ahora tenemos otra misión entre manos. Suerte con eso.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(129,'Mindy',5,'NARRATIVE',2,'NEUTRAL','No encuentro a mi hijo. Tuvimos un accidente con la nave, y nos separamos. ¿Me pueden ayudar?',2,'Lo hubieras cuidado mejor. Nosotros no estamos para eso. Chau','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(130,'Mindy',5,'NARRATIVE',3,'POSITIVE','Mi hijo todavía no aparece.',0,'Lo buscaremos.','2019-04-17 03:11:46','','',''),
(131,'Mindy',5,'NARRATIVE',3,'NEUTRAL','Mi hijo todavía no aparece.',0,'Nosotros somos tres, ya mismo salimos a buscarlo.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(132,'Mindy',5,'NARRATIVE',3,'NEUTRAL','Mi hijo todavía no aparece.',1,'Uh, ahora tenemos otra misión entre manos. Suerte con eso.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(133,'Mindy',5,'NARRATIVE',3,'NEUTRAL','Mi hijo todavía no aparece.',2,'Lo hubieras cuidado mejor. Nosotros no estamos para eso. Chau','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(134,'Mindy',5,'NARRATIVE',4,'POSITIVE','Muchas gracias por encontrar a mi amado hijo, Rúculo.',0,'No hay de qué. ¡Cuídense!','2019-04-17 03:11:46','','',''),
(135,'Mindy',5,'NARRATIVE',4,'NEUTRAL','Muchas gracias por encontrar a mi amado hijo, Rúculo.',0,'No hay de qué. ¡Cuídense!','2019-04-17 03:11:46','','',''),
(136,'Mirk',1,'ET',0,'NEUTRAL','Hola',0,'Hola, ¿qué te pasa? ¿Estás triste?','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','triste (triste)'),
(137,'Mirk',1,'ET',0,'NEUTRAL','Hola',1,'Hola, ¿qué te pasa? ¿Te sentís avergonzado?','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','avergonzado (triste)'),
(138,'Mirk',1,'ET',0,'NEUTRAL','Hola',2,'Hola, ¿qué te pasa? ¿Tenés miedo?','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','miedo (triste)'),
(139,'Mirk',1,'ET',1,'NEUTRAL','No, estoy triste como un swampapón perdido en el espacio.',0,'¿Triste? ¡Andáááá, salame!','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no agresivo'),
(140,'Mirk',1,'ET',1,'NEUTRAL','No, estoy triste como un swampapón perdido en el espacio.',1,'(...No me interesa, mejor sigo)','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no moderado'),
(141,'Mirk',1,'ET',1,'NEUTRAL','No, estoy triste como un swampapón perdido en el espacio.',2,'¿Por qué estás triste?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(142,'Mirk',1,'ET',1,'POSITIVE','¡Sí! Me siento muy triste',0,'¿Triste? ¡Andáááá, salame!','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no'),
(143,'Mirk',1,'ET',1,'POSITIVE','¡Sí! Me siento muy triste',1,'(...No me interesa, mejor sigo)','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','moderado'),
(144,'Mirk',1,'ET',1,'POSITIVE','¡Sí! Me siento muy triste',2,'¿Por qué estás triste?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(145,'Mirk',1,'ET',2,'POSITIVE','Porque no sé cuándo volveré a casa y extraño a los míos.',0,'Uy, claro, pero va a estar todo bien, de verdad.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(146,'Mirk',1,'ET',2,'POSITIVE','Porque no sé cuándo volveré a casa y extraño a los míos.',1,'Mmmh, ¿qué decir?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','moderado'),
(147,'Mirk',1,'ET',2,'POSITIVE','Porque no sé cuándo volveré a casa y extraño a los míos.',2,'¿Extrañás? Qué pavada, pensé que te pasaba algo importante.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no'),
(148,'Mirk',1,'ET',3,'NEUTRAL','Buff.',0,'Buff, sí. Adiós','2019-04-17 03:11:46','','',''),
(149,'Mirk',1,'ET',3,'POSITIVE','¿En serio? Me alegraron el día. En agradecimiento les daré unos recursos tecnológicos.',0,'¡Gracias, nos vemos!','2019-04-17 03:11:46','','',''),
(150,'Mirk',1,'ET',4,'POSITIVE','Creo en ustedes, no los olvidaré. ¡Adiós!',0,'SALIR','2019-04-17 03:11:46','','',''),
(151,'Mirk',2,'ET',0,'NEUTRAL','...',0,'Parece que en medio de esta catástrofe estás alegre','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','alegre (alegre)'),
(152,'Mirk',2,'ET',0,'NEUTRAL','...',1,'Parece que en medio de esta catástrofe estás aliviado','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','aliviado (alegre)'),
(153,'Mirk',2,'ET',0,'NEUTRAL','...',2,'Parece que en medio de esta catástrofe estás relajado','2019-04-17 03:11:46','EMPATÍA','RECONOCIMIENTO','relajado (alegre)'),
(154,'Mirk',2,'ET',1,'NEUTRAL','Estoy alegre. Soy un optimista incurable y un fanático de los deportes.',0,'Buena combinación.','2019-04-17 03:11:46','','',''),
(155,'Mirk',2,'ET',2,'NEUTRAL','Mientras esperamos que todo se solucione, ¿les cuento algunas anécdotas deportivas?',0,'Nada nos gustaría más pero tenemos dos dimensiones que salvar','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí'),
(156,'Mirk',2,'ET',2,'NEUTRAL','Mientras esperamos que todo se solucione, ¿les cuento algunas anécdotas deportivas?',1,'¡¿Anécdotas, ahora?! Nos vamos, vos sos más tonto que optimista.','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí - agresivo'),
(157,'Mirk',2,'ET',2,'NEUTRAL','Mientras esperamos que todo se solucione, ¿les cuento algunas anécdotas deportivas?',2,'Contános, dale.','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','no'),
(158,'Mirk',2,'ET',3,'NEUTRAL','En los juegos del 2012, se descalificó al maratonista X-L2 por usar teletransportación para ganar la carrera.',0,'¡Qué vago!','2019-04-17 03:11:46','','',''),
(159,'Mirk',2,'ET',3,'NEUTRAL','En los juegos del 2012, se descalificó al maratonista X-L2 por usar teletransportación para ganar la carrera.',1,'Salir','2019-04-17 03:11:46','','',''),
(160,'Mirk',2,'ET',4,'NEUTRAL','Isadorisc, del cuadrante 8, fue el primer ser transmente en ganar una medalla de cuarzo.',0,'¡Bien por ella! O por él. Bueno, por lo que sea.','2019-04-17 03:11:46','','',''),
(161,'Mirk',2,'ET',4,'NEUTRAL','Isadorisc, del cuadrante 8, fue el primer ser transmente en ganar una medalla de cuarzo.',1,'Salir','2019-04-17 03:11:46','','',''),
(162,'Mirk',2,'ET',5,'NEUTRAL','En la competencia de ´Tiro a la tetera´, aún nadie ha superado a la gran Novena Ocho.',0,'Un deporte de alto riesgo para la vajilla.','2019-04-17 03:11:46','','',''),
(163,'Mirk',2,'ET',5,'NEUTRAL','En la competencia de ´Tiro a la tetera´, aún nadie ha superado a la gran Novena Ocho.',1,'Salir','2019-04-17 03:11:46','','',''),
(164,'Mirk',2,'ET',6,'NEUTRAL','La final de extremidad - pelota se suspendió este año por una invasión de topos radioactivos que arruinaron la cancha.',0,'Un problema realmente topográfico. ¡Gracias por desasnarnos!','2019-04-17 03:11:46','','',''),
(165,'Mirk',2,'ET',7,'NEUTRAL','Ya verán cómo todo se arregla. A menos que muramos todos, claro.',0,'...Okey. No esperaba eso último, pero bueno.','2019-04-17 03:11:46','','',''),
(166,'Mirk',6,'ET',0,'NEUTRAL','Ah, unos humanos. ¿Tuvieron algo que ver con este caos? No hace falta que respondan, solo quiero culpar a alguien por esto.',0,'¿Te va a llevar mucho?','2019-04-17 03:11:46','','',''),
(167,'Mirk',6,'ET',1,'NEUTRAL','No, apenas unos 40 minutos de culpabilizar a su especie. ¿Están listos?',0,'Estamos listos. Decinos','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','no'),
(168,'Mirk',6,'ET',1,'NEUTRAL','No, apenas unos 40 minutos de culpabilizar a su especie. ¿Están listos?',1,'¿Pero vos qué te pensás? Salí de acá que estamos apurados.','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí - agresivo'),
(169,'Mirk',6,'ET',1,'NEUTRAL','No, apenas unos 40 minutos de culpabilizar a su especie. ¿Están listos?',2,'Nos encantaría pero no va a poder ser. ¡Justamente estamos solucionándolo!','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí'),
(170,'Mirk',6,'ET',2,'NEUTRAL','En primer lugar, se sabe que los humanos son de hacer lío con la ciencia.',0,'Bueno, quién dice lío, dice progreso. Son formas de verlo.','2019-04-17 03:11:46','','',''),
(171,'Mirk',6,'ET',2,'NEUTRAL','En primer lugar, se sabe que los humanos son de hacer lío con la ciencia.',1,'Salir','2019-04-17 03:11:46','','',''),
(172,'Mirk',6,'ET',3,'NEUTRAL','¿Qué necesidad había de jugar con las coordenadas de tiempo y espacio? ¿Eh?',0,'No lo sé. Apenas soy el ayudante de la científica.','2019-04-17 03:11:46','','',''),
(173,'Mirk',6,'ET',3,'NEUTRAL','¿Qué necesidad había de jugar con las coordenadas de tiempo y espacio? ¿Eh?',1,'Salir','2019-04-17 03:11:46','','',''),
(174,'Mirk',6,'ET',4,'NEUTRAL','Los ayudantes son los peores. Tiran la mano y esconden la piedra. ¿Se dice así?',0,'Algo así. Pero creemos que fue un accidente.','2019-04-17 03:11:46','','',''),
(175,'Mirk',6,'ET',4,'NEUTRAL','Los ayudantes son los peores. Tiran la mano y esconden la piedra. ¿Se dice así?',1,'Salir','2019-04-17 03:11:46','','',''),
(176,'Mirk',6,'ET',5,'NEUTRAL','¡Claaaro! Un accidente, ajá. ¿Jugamos nosotros con portales y saltos de tiempo?',0,'No sé, ¿lo hacen?','2019-04-17 03:11:46','','',''),
(177,'Mirk',6,'ET',5,'NEUTRAL','¡Claaaro! Un accidente, ajá. ¿Jugamos nosotros con portales y saltos de tiempo?',1,'Salir','2019-04-17 03:11:46','','',''),
(178,'Mirk',6,'ET',6,'NEUTRAL','¡Por supuesto que no! Tenemos una res-pon-sa-bi-li-dad frente al universo.',0,'Entiendo a qué vas con esto.','2019-04-17 03:11:46','','',''),
(179,'Mirk',6,'ET',6,'NEUTRAL','¡Por supuesto que no! Tenemos una res-pon-sa-bi-li-dad frente al universo.',1,'Salir','2019-04-17 03:11:46','','',''),
(180,'Mirk',6,'ET',7,'NEUTRAL','¿Ah, sí? ¿Y qué opinás? ¿Tengo razón o no?',0,'Sí, pero así como hacemos lío, tratamos de arreglarlo. ¡Y es lo que pienso hacer ahora!','2019-04-17 03:11:46','','',''),
(181,'Mirk',6,'ET',8,'NEUTRAL','No pierdan más tiempo. Vayan y arreglen esto.',0,'Ahí vamos.','2019-04-17 03:11:46','','',''),
(182,'Morkid',5,'NARRATIVE',0,'NEUTRAL','Estoy perdido.',0,'¡Te encontramos!','2019-04-17 03:11:46','','',''),
(183,'Mork',3,'ET',0,'NEUTRAL','¡Ayuda! ¡Estos portales son peligrosos! ¿Pueden ayudarme a cerrarlos?',0,'Lástima, nos agarrás en mal momento.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(184,'Mork',3,'ET',0,'NEUTRAL','¡Ayuda! ¡Estos portales son peligrosos! ¿Pueden ayudarme a cerrarlos?',1,'¡Ni locos! ¿Qué te pensás?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - agresivo'),
(185,'Mork',3,'ET',0,'NEUTRAL','¡Ayuda! ¡Estos portales son peligrosos! ¿Pueden ayudarme a cerrarlos?',2,'Podemos, ahí vamos.','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(186,'Mork',3,'ET',1,'NEUTRAL','Les daré recursos tecnológicos si lo consiguen',0,'Nos vendrían más que bien','2019-04-17 03:11:46','','',''),
(187,'Mork',3,'ET',2,'NEUTRAL','Sigue habiendo portales abiertos',0,'Ah, sí','2019-04-17 03:11:46','','',''),
(188,'Mork',3,'ET',3,'NEUTRAL','¡Son unos cracks, lo lograron! Tomen sus recursos.',0,'¡Buena!','2019-04-17 03:11:46','','',''),
(189,'Mork',3,'ET',4,'NEUTRAL','Aquí está todo en orden... Por ahora.',0,'SALIR','2019-04-17 03:11:46','','',''),
(190,'Mork',5,'COLLAB',0,'NEUTRAL','No sé qué hacer.',0,'(Parece estar en problemas pero mejor sigo de largo.)','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no - moderado'),
(191,'Mork',5,'COLLAB',0,'NEUTRAL','No sé qué hacer.',1,'Parece que estás en problemas. ¿Podemos ayudarte?','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','sí'),
(192,'Mork',5,'COLLAB',0,'NEUTRAL','No sé qué hacer.',2,'No sé qué te pasa, pero mejor seguimos','2019-04-17 03:11:46','EMPATÍA','ACCIÓN','no'),
(193,'Mork',5,'COLLAB',1,'NEUTRAL','Ojalá. Mi nave está en llamas y si explota puede abrir un vórtice muy peligroso. ¿Apagarían conmigo el incendio?',0,'Sí, ya mismo.','2019-04-17 03:11:46','COLABORATIVO','TODOS','sí'),
(194,'Mork',5,'COLLAB',1,'NEUTRAL','Ojalá. Mi nave está en llamas y si explota puede abrir un vórtice muy peligroso. ¿Apagarían conmigo el incendio?',1,'No podemos en este momento. Lo lamento de verdad.','2019-04-17 03:11:46','COLABORATIVO','TODOS','no - moderado'),
(195,'Mork',5,'COLLAB',1,'NEUTRAL','Ojalá. Mi nave está en llamas y si explota puede abrir un vórtice muy peligroso. ¿Apagarían conmigo el incendio?',2,'¿Qué te crees que somos? ¿Una sociedad de beneficencia? ¡Arregláte!','2019-04-17 03:11:46','COLABORATIVO','TODOS','no - agresivo'),
(196,'Mork',5,'COLLAB',2,'NEUTRAL','¿Apagamos juntos el incendio de la nave? ¡Si no, puede que se abra un vórtice peligroso!',0,'Sí, ya mismo.','2019-04-17 03:11:46','','',''),
(197,'Mork',5,'COLLAB',3,'NEUTRAL','Gracias por salvar mi nave y evitar mayores desgracias.',0,'Todo bien.','2019-04-17 03:11:46','','',''),
(198,'Mork',6,'COLLAB',0,'NEUTRAL','Hola. Si colaboran conmigo limpiando la contaminación de este sector, será mejor para todos.',0,'No podemos ahora.','2019-04-17 03:11:46','COLABORATIVO','TODOS','no'),
(199,'Mork',6,'COLLAB',0,'NEUTRAL','Hola. Si colaboran conmigo limpiando la contaminación de este sector, será mejor para todos.',1,'De acuerdo.','2019-04-17 03:11:46','COLABORATIVO','TODOS','sí'),
(200,'Mork',6,'COLLAB',0,'NEUTRAL','Hola. Si colaboran conmigo limpiando la contaminación de este sector, será mejor para todos.',2,'Mmmm No estoy seguro','2019-04-17 03:11:46','COLABORATIVO','TODOS','no'),
(201,'Mork',6,'COLLAB',1,'NEUTRAL','Si limpian cuatro contaminaciones, confíen en que me ocuparé del resto, ¿sí?',0,'No estoy muy de acuerdo.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','no'),
(202,'Mork',6,'COLLAB',1,'NEUTRAL','Si limpian cuatro contaminaciones, confíen en que me ocuparé del resto, ¿sí?',1,'¡Perfecto! Limpiaremos solo cuatro.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','sí'),
(203,'Mork',6,'COLLAB',1,'NEUTRAL','Si limpian cuatro contaminaciones, confíen en que me ocuparé del resto, ¿sí?',2,'No confío en que puedas hacerlo.','2019-04-17 03:11:46','COLABORATIVO','CONFIANZA','no'),
(204,'Mork',6,'COLLAB',2,'NEUTRAL','Es sorprendente lo que se consigue uniendo fuerzas.',0,'Si vos lo decís...','2019-04-17 03:11:46','','',''),
(205,'Mork',6,'COLLAB',2,'POSITIVE','Es sorprendente lo que se consigue uniendo fuerzas.',0,'¡Sí, adiós!','2019-04-17 03:11:46','','',''),
(206,'Mork',6,'COLLAB',3,'NEUTRAL','Es sorprendente lo que se consigue uniendo fuerzas.',0,'¡Sí, adiós!','2019-04-17 03:11:46','','',''),
(207,'Murk',3,'NARRATIVE',0,'NEUTRAL','Hola, nunca habíamos visto seres con ruedas incorporadas. ¡Qué práctico!',0,'En realidad tenemos piernas, pero bueno, estamos un poco ocupados como para explicarles anatomía humana.','2019-04-17 03:11:46','','',''),
(208,'Murk',3,'NARRATIVE',1,'NEUTRAL','Ya vemos. Nosotros también estamos ocupados limpiando la contaminación con esta herramienta. Tenemos otra de repuesto...',0,'Ah, nos vendría muy bien si les sobra, ¿nos la darían?','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - buen modo'),
(209,'Murk',3,'NARRATIVE',1,'NEUTRAL','Ya vemos. Nosotros también estamos ocupados limpiando la contaminación con esta herramienta. Tenemos otra de repuesto...',1,'No creo que la necesitemos.','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','no pedir'),
(210,'Murk',3,'NARRATIVE',1,'NEUTRAL','Ya vemos. Nosotros también estamos ocupados limpiando la contaminación con esta herramienta. Tenemos otra de repuesto...',2,'¡Déjense de perder el tiempo y dennos una!','2019-04-17 03:11:46','ASERTIVIDAD','PEDIDO','sí - agresivo'),
(211,'Murk',3,'NARRATIVE',2,'POSITIVE','Seguro, la van a necesitar si quieren atravesar la isla.',0,'Gracias.','2019-04-17 03:11:46','','',''),
(212,'Murk',3,'NARRATIVE',2,'NEUTRAL','Llévenla. La van a necesitar si quieren atravesar la isla',0,'Gracias.','2019-04-17 03:11:46','','',''),
(213,'Murk',3,'NARRATIVE',2,'NEGATIVE','Bueno, ¡qué carácter! Aquí la tienen.',0,'Bien. Adiós.','2019-04-17 03:11:46','','',''),
(214,'Murk',3,'NARRATIVE',3,'NEUTRAL','Adiós y que lleguen bien. ¡Ojalá este desbarajuste se arregle pronto!',0,'Lo mismo digo, ¡adiós!','2019-04-17 03:11:46','','',''),
(215,'Murk',5,'COLLAB',0,'NEUTRAL','Hola, humanos en desarrollo. En mi dimensión soy comediante y aprendí unos cuantos chistes estando aquí.',0,'Ahá, interesante, ¿entonces?','2019-04-17 03:11:46','','',''),
(216,'Murk',5,'COLLAB',1,'NEUTRAL','¿Quieren oír algunos de mi extenso repertorio?',0,'Disculpá, pero no tenemos mucho tiempo. Otro día, ¿sí?','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí'),
(217,'Murk',5,'COLLAB',1,'NEUTRAL','¿Quieren oír algunos de mi extenso repertorio?',1,'No vamos a parar para oir tus pavadas. ¡El chiste sos vos!','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','sí - agresivo'),
(218,'Murk',5,'COLLAB',1,'NEUTRAL','¿Quieren oír algunos de mi extenso repertorio?',2,'Te escuchamos.','2019-04-17 03:11:46','ASERTIVIDAD','NEGARSE','no'),
(219,'Murk',5,'COLLAB',2,'NEUTRAL','Que le dijo una impresora a otra? ¿Ese papel es tuyo o es impresión mía? Jiu, jiu.',0,'No está nada mal.','2019-04-17 03:11:46','','',''),
(220,'Murk',5,'COLLAB',2,'NEUTRAL','Que le dijo una impresora a otra? ¿Ese papel es tuyo o es impresión mía? Jiu, jiu.',1,'Salir','2019-04-17 03:11:46','','',''),
(221,'Murk',5,'COLLAB',3,'NEUTRAL','¿Cómo declara su amor una chinche a la otra? Te amo chincheramente. Jiu, jiu.',0,'Simpático, sí.','2019-04-17 03:11:46','','',''),
(222,'Murk',5,'COLLAB',3,'NEUTRAL','¿Cómo declara su amor una chinche a la otra? Te amo chincheramente. Jiu, jiu.',1,'Salir','2019-04-17 03:11:46','','',''),
(223,'Murk',5,'COLLAB',4,'NEUTRAL','¿Que le dice un perro arrepentido a otro? Perroname. Jiu, jiu.',0,'Je, je, ¡está bien!','2019-04-17 03:11:46','','',''),
(224,'Murk',5,'COLLAB',4,'NEUTRAL','¿Que le dice un perro arrepentido a otro? Perroname. Jiu, jiu.',1,'Salir','2019-04-17 03:11:46','','',''),
(225,'Murk',5,'COLLAB',5,'NEUTRAL','Uno de mi dimensión: ¿Qué le dijo un swampampón constipado a otro? Driblitz inodoris quiront. Jiu, jiu.',0,'Ah, ese no lo cacé del todo.','2019-04-17 03:11:46','','',''),
(226,'Murk',5,'COLLAB',5,'NEUTRAL','Uno de mi dimensión: ¿Qué le dijo un swampampón constipado a otro? Driblitz inodoris quiront. Jiu, jiu.',1,'Salir','2019-04-17 03:11:46','','',''),
(227,'Murk',5,'COLLAB',6,'NEUTRAL','Una cuchara llama a la otra: ¡Cuchara, cuchara! Oh, parece que no es - cuchara. Jiu, Jiu',0,'Jiu, jiu. Ay, ya me contagió su risa.','2019-04-17 03:11:46','','',''),
(228,'Murk',5,'COLLAB',6,'NEUTRAL','Una cuchara llama a la otra: ¡Cuchara, cuchara! Oh, parece que no es - cuchara. Jiu, Jiu',1,'Salir','2019-04-17 03:11:46','','',''),
(229,'Murk',5,'COLLAB',7,'NEUTRAL','¿Cómo insulta una pila a otra? ¡Estúpila!',0,'Estuvo muy bueno. Pagaría por verte en un show de stand up.','2019-04-17 03:11:46','','',''),
(230,'Murk',5,'COLLAB',8,'NEUTRAL','Han sido un público maravilloso. ¡Vuelvan cuando quieran!',0,'¡Hasta luego!','2019-04-17 03:11:46','','',''),
(231,'Patovalien',5,'AUTOEVAL',0,'NEUTRAL','Solo diré que aún te quedan cosas por hacer aquí.',0,'No puedo sacarle una palabra más. Mejor averiguo qué me queda por hacer.','2019-04-17 03:11:46','','',''),
(232,'Patovalien',5,'AUTOEVAL',1,'NEUTRAL','Ayudaste a mi amigo en desgracia y ahora yo colaboraré contigo dándote un recurso tecnológico. ¡Acéptalo!',0,'¡Acepto, gracias!','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','sí'),
(233,'Patovalien',5,'AUTOEVAL',1,'NEUTRAL','Ayudaste a mi amigo en desgracia y ahora yo colaboraré contigo dándote un recurso tecnológico. ¡Acéptalo!',1,'No, tengo suficiente.','2019-04-17 03:11:46','COLABORATIVO','ACEPTAR','no'),
(234,'Patovalien',5,'AUTOEVAL',2,'NEUTRAL','Que lleguen bien a su destino.',0,'Y vos al tuyo.','2019-04-17 03:11:46','','','');


-- --------------------------------------------------------

--
-- Table structure for table `dialog_replies`
--
DROP TABLE IF EXISTS `dialog_replies`;

CREATE TABLE `dialog_replies` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `computer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
	`game_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `character_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
  `dialog_index` int(11) NOT NULL,
  `dialog_mood` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `answer_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--
DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `computer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
	`game_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tools_selected` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `missions` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `map_checks` int(11) NOT NULL,
  `level_time` double NOT NULL,
  `game_time` double NOT NULL,
	`phone_map_time` double NOT NULL,
  `first_map_time` double NOT NULL,
	`tools_map_time` double NOT NULL,
  `mission_time` double NOT NULL,
  `tools_time` double NOT NULL,
  `map_trail` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `rt_begin` int(11) NOT NULL,
  `rt_after_tools` int(11) NOT NULL,
  `rt_end` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `level_end` int(11) NOT NULL,
  `portal_done` int(11) NOT NULL,
  `fire_done` int(11) NOT NULL,
  `pollution_done` int(11) NOT NULL,
  `map_dead_ends` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `tools_end_charge` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `portal_charge` int(11) NOT NULL,
  `fire_charge` int(11) NOT NULL,
  `pollution_charge` int(11) NOT NULL,
  `rt_charge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `level_data`
--
DROP TABLE IF EXISTS `level_data`;

CREATE TABLE `level_data` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `missions` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `portal` int(11) NOT NULL,
  `fire` int(11) NOT NULL,
  `pollution` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `portal_charge` int(11) NOT NULL,
  `fire_charge` int(11) NOT NULL,
  `pollution_charge` int(11) NOT NULL,
  `resources_charge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `level_data`
--

INSERT INTO `level_data` (`id`, `level_id`, `missions`, `portal`, `fire`, `pollution`, `create_time`, `portal_charge`, `fire_charge`, `pollution_charge`, `resources_charge`) VALUES
(1, 1,'Completar el nivel en menos de 60 segundos;Eliminar todos los portales;Apagar el incendio',2,1,0,'2019-05-10 12:00:00',1,0,0,1),
(2, 2,'Salir del nivel en 60 segundos;Apagar todos los incendios;',2,3,0,'2019-05-10 12:00:00',1,2,0,5)
(3, 3,'Llegar al bote sanos y a salvo;Cerrar todos los portales del nivel;Llegar al bote con carga máxima de matafuegos',3,6,1,'2019-05-10 12:00:00',2,1,0,2)
(4, 4,'Pasar de nivel;Terminar el nivel con las herramientas cargadas al máximo;',4,5,5,'2019-05-10 12:00:00',1,1,1,2)
(5, 5,'Apagar el incendio de la nave;Encontrar al niño alien;Completar el nivel en 60 segundos',4,4,4,'2019-05-10 12:00:00',0,2,1,3)
(6, 6,'Cruzar el puente ;Limpiar toda la contaminación;Cerrar todos los portales',4,4,6,'2019-05-10 12:00:00',0,0,0,0)
(7, 7,'Encontrarse con la Dra Grimberg;Cerrar el vórtice principal;',5,0,0,'2019-05-10 12:00:00',0,0,0,0);



-- --------------------------------------------------------

--
-- Table structure for table `missions`
--
DROP TABLE IF EXISTS `missions`;

CREATE TABLE `missions` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `missions` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `selfie`
--
DROP TABLE IF EXISTS `selfie`;

CREATE TABLE `selfie` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `computer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
	`game_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `emoji_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `computer_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `cue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `grado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `turno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_user`
--
ALTER TABLE `data_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_36DC1DAB92FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_36DC1DABA0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_36DC1DABC05FB297` (`confirmation_token`);

--
-- Indexes for table `dialogs`
--
ALTER TABLE `dialogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dialog_replies`
--
ALTER TABLE `dialog_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_data`
--
ALTER TABLE `level_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `missions`
--
ALTER TABLE `missions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selfie`
--
ALTER TABLE `selfie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_user`
--
ALTER TABLE `data_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dialogs`
--
ALTER TABLE `dialogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT for table `dialog_replies`
--
ALTER TABLE `dialog_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `level_data`
--
ALTER TABLE `level_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `missions`
--
ALTER TABLE `missions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selfie`
--
ALTER TABLE `selfie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
