CREATE VIEW metrics AS 
SELECT * 
FROM level_metrics LM
INNER JOIN dialogs_metrics DM
	ON LM.user_id = DM.user_id;