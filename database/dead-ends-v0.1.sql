##--DIALOGS QUERY GENERATION
DROP VIEW IF EXISTS `css_metrics`;

SET group_concat_max_len = 32768;
SET @sql = NULL;

SELECT GROUP_CONCAT( 
	DISTINCT CONCAT(
		'MAX(CASE WHEN CSSC.css_column = ''', CONCAT('L',LC.level_id,'_CSS_',LC.css_id), ''' THEN CSSC.css_times END) AS ', 
		CONCAT('L_',level_id,'_CSS_',css_id)		
	)) INTO @sql
FROM level_css LC;

SET @sql = CONCAT(
'CREATE VIEW css_metrics AS ',	
	'
SELECT CSSC.user_id,' ,@sql , '
FROM level_css LC
INNER JOIN (
    SELECT 	user_id,
			CONCAT("L",DE.level_id,"_CSS_",LCSS.css_id) AS css_column,
			CAST(JSON_EXTRACT(map_dead_ends, CONCAT(SUBSTR( JSON_SEARCH(map_dead_ends, "all", LCSS.css_id) ,2, LOCATE(".id", JSON_SEARCH(map_dead_ends, "all", LCSS.css_id))-2 ),".times") ) AS UNSIGNED)  AS css_times
	FROM (
        SELECT 	user_id,
				level_id,
      	map_dead_ends
       	FROM best_game) AS DE
	INNER JOIN level_css LCSS ON DE.level_id = LCSS.level_id) CSSC
ON CSSC.css_column = CONCAT("L",LC.level_id,"_CSS_",LC.css_id)
GROUP BY user_id
	'
);

PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;




/*WHERE JSON_SEARCH(dead_ends,'all',"5_0") <> NULL


SELECT dead_ends->>"$[1].times"
     FROM (SELECT user_id,CAST(CONCAT('[',SUBSTRING(REPLACE(map_dead_ends, ';', ','), 1, LENGTH(map_dead_ends) -1),']') AS JSON) as 'dead_ends'
          FROM level) AS DE
WHERE dead_ends->>"$[1].times" > 0


SELECT CAST(CONCAT('[',SUBSTRING(REPLACE(map_dead_ends, ';', ','), 1, LENGTH(map_dead_ends) -1),']') AS JSON)
FROM level

SELECT  JSON_EXTRACT(dead_ends, JSON_SEARCH(dead_ends,'one',"5_0"))
FROM (SELECT CAST(CONCAT('[',SUBSTRING(REPLACE(map_dead_ends, ';', ','), 1, LENGTH(map_dead_ends) -1),']') AS JSON) as 'dead_ends'
      FROM level) AS DE