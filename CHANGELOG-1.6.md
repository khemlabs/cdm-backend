CHANGELOG para 1.6
===================

 * Cambio en la validaci�n del campo nombre escuela
 * Cambio para que los informes logren identificar distintos nombres de alumnos que utilicen una misma computadora
 * Animaci�n de latido para bot�n de saltear nivel en nivel 5
 * Evitar que pueda saltearse la ayuda enseguida en el nivel 1
 * Evitar que las opciones de di�logos puedan presionarse inmediatamente para fomentar su lectura
 * Las opciones de respuesta a di�logos aparecen en orden aleatorio cada vez
