UPDATE para 1.2
===================

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Actualizar librer�as con composer
	$ php composer.phar update

+ Copiar el dump de la base de datos
	$ cd /var/www/html/edu-28-crucedemundos/database/
	$ mysql -u root -p cdm < cdm_database.sql

+ Actualizar las tablas de la base de datos para la librer�a SithousAntiSpam
	$ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --dump-sql
	$ php app/console doctrine:schema:update --force

+ Agregar las reglas del filtro anti spam para la creaci�n de usuarios
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: user_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 86400
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de di�logos
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: dialog_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 1
	Max Calls that can happin in MaxTime: 3

+ Agregar las reglas del filtro anti spam para la persistencia de niveles
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: level_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 5
	Max Calls that can happin in MaxTime: 1

+ Agregar las reglas del filtro anti spam para la persistencia de selfies
	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: selfie_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 2
	Max Calls that can happin in MaxTime: 1

+ Modificar security_listeners.xml para habilitar Captcha en Login:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/config/security_listeners.xml

all� cambiar esta l�nea:
<parameter key="security.authentication.listener.form.class">Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener</parameter>

por esta:
<parameter key="security.authentication.listener.form.class">AppBundle\Listener\UserFormListener</parameter>

+ Evitar que se muestre error 403 en navegador:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

all� modificar las l�neas 66 a 75:

        return new Response($this->twig->render(
            (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException),
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        
cambiar por :

	if($code==403){
		$code=404;		
	}

	$response=new Response($this->twig->render(
            (string) $this->findTemplate($request, $request->getRequestFormat(), $code, $showException),
            array(
                'status_code' => $code,
                'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'exception' => $exception,
                'logger' => $logger,
                'currentContent' => $currentContent,
            )
        ), 200, array('Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html'));

	$response->setStatusCode(code);

        return 	$response;

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm
