UPDATE para 1.5
===================

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Actualizar las tablas de la base de datos para medir tiempo de diálogos
	$ cd /var/www/html/edu-28-crucedemundos/source/cdm
	$ php app/console doctrine:schema:update --force

+ Editar el filtro antispam "user_protection"
	$ php app/console sithous:antispam:delete
	Enter the SithousAntiSpamType ID to delete: user_protection
	Successfully removed SithousAntiSpamType "user_protection"

	$ php app/console sithous:antispam:generate
	Please enter the ID for this type: user_protection
	Track IP [Y/N]? Y
	Track User [Y/N]? N
	Max Time to track action (seconds): 30
	Max Calls that can happin in MaxTime: 1

+ Limpiar el cache de la aplicación web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm

