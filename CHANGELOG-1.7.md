# CHANGELOG para 1.7

- Actualización en versiones de librerías con Sithous y FOS
- Se agrega docker y docker-compose para facilitar el proceso de instalación y configuración
- Se agregaron los pasos manuales en la instalación al proceso automatico de deploy usando las herramientas de Symfony para dichos casos.
- Se editó el Makefile agregando comandos de build, init, clean, log
- Se editó el manual de instalación para reflejar los cambios en la instalación
