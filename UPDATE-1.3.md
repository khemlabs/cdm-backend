UPDATE para 1.3
===================

+ Actualizar del repositorio:
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R desarrollo:desarrollo cdm
	$ cd cdm
	$ git pull

+ Arreglar error al evitar que se muestre error 403 en navegador:

$vi /var/www/html/edu-28-crucedemundos/source/cdm/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php

modificar la l�nea 
	$response->setStatusCode(code);
por la l�nea
	$response->setStatusCode($code);

+ Limpiar el cache de la aplicaci�n web
	$ php app/console cache:clear --no-warmup -e prod

+ Volver a asignar a apache como propietario de la carpeta cdm 
	$ cd /var/www/html/edu-28-crucedemundos/source/
	$ sudo chown -R apache:apache cdm
