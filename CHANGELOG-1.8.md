# CHANGELOG para 1.8

- Actualización de versión de dependencias
- Se hicieron cambios en dockerfile y docker-compose para facilitar el proceso de instalación y configuración
- Se editó el Makefile agregando comandos de build y separando la logica de base de datos
- Se editó el manual de instalación para reflejar los cambios en la instalación y agregar los requerimientos para la instalación con un proxy en nginx.
- Se agrego en el manual un detalle mayor en las tareas y opciones de configuración para facilitar el proceso de homologación.
