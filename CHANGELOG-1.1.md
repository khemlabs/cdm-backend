CHANGELOG para 1.1
===================

 * Correcci�n incidencia JIRA 8.1.1 "Texto  solapado  con  globo  de  di�logo"
 * Correcci�n incidencia JIRA 8.1.3 "Error  ortogr�fico  al  finalizar  nivel  4"
 * Correcci�n incidencia JIRA 8.1.4 "El  audio  contin�a  escuch�ndose  cuando  la aplicaci�n no est� en primer plano"
 * Correcci�n incidencia JIRA 8.1.5 "Nombre  de  personaje  aparece  de  forma incorrecta"
 * Correcci�n incidencia JIRA 8.1.6 "Chequeo  de  spam  en  invocaciones"
 * Correcci�n incidencia JIRA 8.1.7 "Manejo  de  usuarios  /  password"
 * Correcci�n incidencia JIRA 8.1.8 "Captcha  en  Backend"
 * Correcci�n incidencia JIRA 8.1.9 "Falta  p�gina  de  manejo  de  errores"
 * Correcci�n incidencia JIRA 8.1.10 "El usuario debe acceder a la carpeta de instalaci�n para correrla Creada"
 * Correcci�n incidencia JIRA 8.1.11 "Los personajes atraviesan paredes al doblar muy cerrado"
 * Correcci�n incidencia JIRA 8.1.12 "El  sistema  no  permite  salir  del  juego"
 * Agregado del backend para docentes y evaluadores
 * Implementaci�n de la librer�a FOS User Bundle para gestionar los usuarios del backend
 * Implementaci�n de la librer�a GregwarCaptchaBundle para utilizar captcha en formulario
 * Implementaci�n de la librer�a SithousAntiSpam Bundle para evitar spam en invocaciones
 * Agregado de cinem�ticas y final del juego en el videjuego
 * Niveles eliminados en el �rea 1 y en el �rea 3 del videojuego
 * Agregado de grupos en los di�logos
 * Correcci�n de estilo y ortogr�fica final en di�logos en el videojuego
 * Mejoras en pantallas de selecci�n de herramientas en el videojugo
 * Agregado de explicaciones sobre el uso de los recursos tecnol�gicos y herramientas en el videojuego
 * Agregado de interacciones y di�logos en el primer nivel para explicar la mec�nica del videjuego
 * Agregado de nuevos elementos visuales que den variedad a los niveles en el videojuego
 * Agregado de nuevos elementos visuales que den variedad a los niveles en el videojuego
 * Correcci�n de frases de estados en las selfies del videojuego
 * Correcci�n de la falta de la �ltima pregunta sobre Autoconcepto en el videojuego
 * Agregado de opciones femeninas para la customizaci�n del avatar en el videojuego
 * Correcci�n sobre la dificultad para apagar algunos incendios en el juego
 * Mejoras en el sistema de pathfinding de los compa�eros del avatar en el videojuego
